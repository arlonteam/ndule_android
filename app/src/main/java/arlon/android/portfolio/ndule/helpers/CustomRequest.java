package arlon.android.portfolio.ndule.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import arlon.android.portfolio.ndule.interfaces.ErrorListener;
import arlon.android.portfolio.ndule.interfaces.Listener;
import arlon.android.portfolio.ndule.interfaces.ResultHandler;

/**
 * Created by Arlon Mukeba on 2/6/2018.
 */

public class CustomRequest extends JsonObjectRequest {

    public static final String OBJECT_PARAM = "object";
    public static final String ACTION_PARAM = "action";

    public static final String USER = "user";
    public static final String PICTURE = "picture";
    public static final String MOVIE = "movie";
    public static final String USERS = "users";

    private Context mContext = null;
    private ResultHandler mResultHandler = null;
    private JSONObject mParams = null;

    public CustomRequest(Context context, int/*Request.Method*/ methodType, String url,
                         JSONObject params, ResultHandler resultHandler) {

        super(methodType, url, params, new Listener(context, resultHandler), new ErrorListener(
                context, resultHandler));

        if (params != null) {
            Log.d("PARAMS", params.toString());
        }

        mContext = context;
        mResultHandler = resultHandler;
        mParams = params;

        try {
            Log.d("REQUESTBODY", new JSONObject(getHeaders()).toString());
        } catch (Exception e) {
            Log.d("REQUESTBODY", "EXCEPTION: " + e.getMessage());
        }

        setShouldCache(false);
        setRetryPolicy();
    }

    private void setRetryPolicy() {
        int socketTimeout = 30000;//30 seconds

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        setRetryPolicy(policy);
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject = jsonParse(response);
        } catch (Exception e) {
            Log.d("VOLLEYEXCEPTION", e.getMessage());
            return Response.error(new VolleyError(e.getMessage()));
        }

        return Response.success(jsonObject, HttpHeaderParser.parseCacheHeaders(response));
    }

    private JSONObject jsonParse(NetworkResponse response) throws UnsupportedEncodingException,
            JSONException {

        Log.d("DATA!", new String(response.data));
        String jsonString = new String(response.data, "UTF-8");
        Log.d("DATA!", jsonString);
        return new JSONObject(jsonString);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        return headers;
    }
}