package arlon.android.portfolio.ndule.helpers;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Arlon Mukeba on 2/6/2018.
 */

public class NetworkHelper {

    private static NetworkHelper mNetworkHelper = null;
    private Context mContext = null;
    private RequestQueue mRequestQueue = null;

    public NetworkHelper(Context context) {
        mContext = context;

        setRequestQueue();
    }

    public static NetworkHelper getNetworkManager(Context context) {
        if (mNetworkHelper == null) {
            mNetworkHelper = new NetworkHelper(context);
        }

        return mNetworkHelper;
    }

    private void setRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
    }

    public void makeRequest(/*JsonObject*/Request request) {
        mRequestQueue.add(request);
    }



    public static boolean googleServicesAvailable() {
        /*int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());

        return (isAvailable == ConnectionResult.SUCCESS);*/

        return true;
    }
}