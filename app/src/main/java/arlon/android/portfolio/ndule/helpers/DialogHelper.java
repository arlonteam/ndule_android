package arlon.android.portfolio.ndule.helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;
import com.afollestad.materialdialogs.MaterialDialog;
import arlon.android.portfolio.ndule.R;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class DialogHelper {

    private static String getString(Context context, int resId) {
        return context.getString(resId);
    }

    private static MaterialDialog.Builder oneButtonDialog(Context context, int titleResId,
                                                          int contentResId, int positiveButtonResId,
                                                          Listener listener) {

        return oneButtonDialog(context, getString(context, titleResId), getString(context,
                contentResId), getString(context, positiveButtonResId), listener);
    }

    public static void showMessage(Context context, String title, String content) {
        showMessage(context, title, content, null);
    }

    public static void showMessage(Context context, int titleResId, int contentResId) {
        showMessage(context, titleResId, contentResId, null);
    }

    public static void showMessage(Context context, int titleResId, int contentResId,
                                   Listener listener) {

        String title = context.getString(titleResId);
        String content = context.getString(contentResId);

        showMessage(context, title, content, listener);
    }

    public static void showMessage(Context context, String title, String content,
                                   Listener listener) {

        oneButtonDialog(context, title, content, "Okay", listener).show();
    }

    private static MaterialDialog.Builder oneButtonDialog(Context context, String title,
                                                          String content, String positiveButton,
                                                          Listener listener) {

        int colorRes = R.color.blue;

        return getBase(context).backgroundColorRes(R.color.black).title(title).content(content)
            .contentColorRes(R.color.white).positiveText(positiveButton)
                .positiveColorRes(colorRes).negativeColorRes(R.color.white)
                    .titleColorRes(colorRes).maxIconSizeRes(R.dimen.dialog_icon_max_size)
                        .iconRes(R.drawable.ic_treble_key).callback(listener);
    }

    private static MaterialDialog.Builder getBase(Context context) {
        return new MaterialDialog.Builder(context).cancelable(false);
    }

    public static MaterialDialog.Builder twoButtonDialog(Context context, int titleResId,
                                                         int contentResId, int positiveButtonResId,
                                                         int negativeButtonResId, Listener listener) {

        return twoButtonDialog(context, getString(context, titleResId), getString(context,
            contentResId), getString(context, positiveButtonResId), getString(context,
                negativeButtonResId), listener);
    }

    public static MaterialDialog.Builder twoButtonDialog(Context context, String title,
                                                         String content, String positiveButton,
                                                         String negativeButton, Listener listener) {

        return getBase(context).backgroundColorRes(R.color.black).iconRes(R.drawable.ic_treble_key)
            .maxIconSizeRes(R.dimen.dialog_icon_max_size).title(title).titleColorRes(R.color.blue).content(content)
                .positiveText(positiveButton).positiveColorRes(R.color.blue)
                    .contentColorRes(R.color.white).negativeText(negativeButton)
                        .negativeColorRes(R.color.red).callback(listener);
    }

    /*public static Dialog loading(Context context, int contentResId) {
        return loadingDialog(context, context.getString(R.string.dialog_progress_msg),
                getString(context, contentResId))).show();
    }

    public static Dialog loading(Context context) {
        return loading(context, R.string.dialog_loading_default);
    }*/

    public static MaterialDialog.Builder loadingDialog(Context context, int contentResId) {
        return loadingDialog(context, getString(context, contentResId));
    }

    public static MaterialDialog.Builder loadingDialog(Context context, String content) {
        return getBase(context).backgroundColorRes(R.color.black).iconRes(R.drawable.ic_treble_key)
            .maxIconSizeRes(R.dimen.dialog_icon_max_size).title(R.string.loading).titleColorRes(R.color.blue)
                .content(content).widgetColorRes(R.color.blue).progress(true, 0).callback(null);
    }

    /*public static MaterialDialog.Builder buildDateTimeDialog(Context context,
                                                             MaterialDialog.SingleButtonCallback positiveBtnClicked,
                                                             MaterialDialog.SingleButtonCallback negativeBtnClicked) {

        return getBase(context).customView(R.layout.date_time_picker_layout, false)
                .title(R.string.date_time_dialog_header).titleColorRes(R.color.ocean_boat_blue).positiveText("Set Date")
                .positiveColorRes(R.color.ocean_boat_blue).onPositive(positiveBtnClicked)
                .negativeColorRes(R.color.candy_pink).onNegative(negativeBtnClicked)
                .autoDismiss(false);
    }*/

    public static MaterialDialog.Builder buildChoiceListDialog(Context context, int titleResId,
                                                               int positiveTxtResId,
                                                               RecyclerView.Adapter<?> adapter,
                                                               MaterialDialog.SingleButtonCallback
                                                                       singleButtonCallback) {

        return getBase(context).title(titleResId).titleColorRes(R.color.black)
            .positiveText(positiveTxtResId).positiveColorRes(R.color.black)
                .adapter(adapter, null).itemsCallbackSingleChoice(0, null)
                    .onPositive(singleButtonCallback);

    }

    public static MaterialDialog.Builder buildImageCropDialog(Context context,
                                                              RelativeLayout layout,
                                                              MaterialDialog.SingleButtonCallback callback) {

        int titleResId = R.string.pf_image_crop_title;
        int yesBtnResId = R.string.btn_proceed;
        int noBtnResId = R.string.btn_cancel;

        return buildCustomDialog(context, layout, titleResId, yesBtnResId, noBtnResId, callback);
    }

    /*public static MaterialDialog.Builder buildDOBDialog(Context context,
                                                        RelativeLayout layout,
                                                        MaterialDialog.SingleButtonCallback callback) {

        int titleResId = R.string.dialog_dob_title;
        int yesBtnResId = R.string.dialog_okay;
        int noBtnResId = R.string.dialog_cancel;

        return buildCustomDialog(context, layout, titleResId, yesBtnResId, noBtnResId, callback)
                .autoDismiss(false);
    }

    public static MaterialDialog.Builder buildGenderDialog(Context context,
                                                        RelativeLayout layout,
                                                        MaterialDialog.SingleButtonCallback callback) {

        int titleResId = R.string.dialog_option_title;
        int yesBtnResId = R.string.dialog_okay;
        int noBtnResId = R.string.dialog_cancel;

        return buildCustomDialog(context, layout, titleResId, yesBtnResId, noBtnResId, callback)
                .autoDismiss(false);
    }*/

    public static MaterialDialog.Builder buildCustomDialog(Context context,
                                                           RelativeLayout layout, int titleResId,
                                                           int yesBtnResId, int noBtnResId,
                                                           MaterialDialog.SingleButtonCallback callback) {

        return getBase(context).title(titleResId)
            .backgroundColor(context.getResources().getColor(R.color.white))
                .titleColorRes(R.color.blue).positiveText(yesBtnResId)
                    .positiveColorRes(R.color.blue).negativeText(noBtnResId)
                        .negativeColorRes(R.color.red).customView(layout, false).onAny(callback);
    }

    /*public static MaterialDialog.Builder buildDateTimeDialog(Context context,
                                                             MaterialDialog.SingleButtonCallback positiveBtnClicked,
                                                             MaterialDialog.SingleButtonCallback negativeBtnClicked) {

        return getBase(context).customView(R.layout.date_time_picker_layout, false)
                .title(R.string.date_time_dialog_header).titleColorRes(R.color.ocean_boat_blue).positiveText("Set Date")
                .positiveColorRes(R.color.ocean_boat_blue).onPositive(positiveBtnClicked)
                .negativeColorRes(R.color.candy_pink).onNegative(negativeBtnClicked)
                .autoDismiss(false);
    }

    public static MaterialDialog.Builder buildChoiceListDialog(Context context, int titleResId,
                                                               int positiveTxtResId,
                                                               WaterSourceDropDownAdapter waterSourceAdapter,
                                                               MaterialDialog.SingleButtonCallback
                                                                       singleButtonCallback) {

        return getBase(context).title(titleResId).titleColorRes(R.color.oceanBlue)
                .positiveText(positiveTxtResId).positiveColorRes(R.color.oceanBlue)
                .adapter(waterSourceAdapter, null)
                .onPositive(singleButtonCallback);

    }*/

    public static abstract class Listener extends MaterialDialog.ButtonCallback {

    }
}