package arlon.android.portfolio.ndule.helpers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import arlon.android.portfolio.ndule.objects.User;

/**
 * Created by Arlon Mukeba on 1/20/2018.
 */

public class JsonHelper {

    public static int getInt(JSONObject jsonObject, String key) {
        int value = 0;

        if (jsonObject.has(key)) {
            try {
                value = jsonObject.getInt(key);
            } catch (Exception e) {

            }
        }

        return value;
    }

    public static String getString(JSONObject jsonObject, String key) {
        String value = "";

        if (jsonObject.has(key)) {
            try {
                value = jsonObject.getString(key);
            } catch (Exception e) {
                Log.d("JSONEXCEPTION", "KEY: " + key);
            }
        }

        return value;
    }

    public static JSONObject getObject(JSONObject jsonObject, String key) {
        JSONObject object = new JSONObject();

        if (jsonObject.has(key)) {
            try {
                object = jsonObject.getJSONObject(key);
            } catch (Exception e) {

            }
        }

        return object;
    }

    public static JSONArray getArray(JSONObject jsonObject, String key) {
        JSONArray array = new JSONArray();

        if (jsonObject.has(key)) {
            try {
                array = jsonObject.getJSONArray(key);
            } catch (Exception e) {
                Log.d("ARRAYEXCEPTION", "Exception: " + e.getMessage());
            }
        }

        return array;
    }

    public static JSONArray listToJsonArray(ArrayList<User> users) {

        JSONArray userArray = new JSONArray();

        for (User user : users) {
            userArray.put(user.getEmail());
        }

        return userArray;
    }
}