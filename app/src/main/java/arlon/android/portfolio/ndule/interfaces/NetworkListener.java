package arlon.android.portfolio.ndule.interfaces;

/**
 * Created by Arlon Mukeba on 2/6/2018.
 */

public interface NetworkListener {

    public void success(Object data);
    public void error(String errorMessage);
}