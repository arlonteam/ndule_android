package arlon.android.portfolio.ndule.interfaces;

import java.util.Hashtable;

/**
 * Created by Arlon Mukeba on 1/22/2018.
 */

public interface ProfileFragmentSwitcher {

    void profileEdited(boolean changed, boolean saved);
}