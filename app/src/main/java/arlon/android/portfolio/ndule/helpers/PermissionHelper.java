package arlon.android.portfolio.ndule.helpers;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import java.util.ArrayList;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class PermissionHelper {

    public static String[] checkPermissions(Context context, ArrayList<String> perms) {
        Log.d("PERMISSIONREQUESTED", "CHECKCAMPERM: " + (Build.VERSION.SDK_INT < Build.VERSION_CODES.M));

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return new String[] {};

        ArrayList<String> permissions = new ArrayList<String>();

        for (String permission : perms) {
            if (!isPermissionGranted(context, permission)) {
                permissions.add(permission);
            }
        }

        return permissions.toArray(new String[permissions.size()]);
    }

    @TargetApi(23)
    private static boolean isPermissionGranted(Context context, String permission) {
        return (context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean isPermGranted(int[] grantedPerms) {
        boolean permGranted = true;

        for (int i = 0; i < grantedPerms.length; i++) {
            if (grantedPerms[i] == PackageManager.PERMISSION_DENIED) {
                permGranted = false;
                break;
            }
        }

        return permGranted;
    }
}