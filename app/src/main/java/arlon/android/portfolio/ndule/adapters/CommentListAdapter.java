package arlon.android.portfolio.ndule.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;

import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.objects.Comment;
import arlon.android.portfolio.ndule.objects.CommentViewHolder;

/**
 * Created by Arlon Mukeba on 2/21/2018.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentViewHolder> {

    private Context mContext = null;
    private int mThemeColor = -1;
    private ArrayList<Comment> mComments = new ArrayList<Comment>();

    public CommentListAdapter(Context context) {
        mContext = context;
    }

    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout feedItemLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
            .inflate(R.layout.layout_comment, parent, false);

        return new CommentViewHolder(feedItemLayout);
    }

    public void onBindViewHolder(final CommentViewHolder commentViewHolder, int position) {
        final Comment comment = mComments.get(position);

        commentViewHolder.mTvAuthor.setText(comment.getAuthorPseudo());
        commentViewHolder.mTvDate.setText(comment.getTimeStamp());
        commentViewHolder.mTvContent.setText(comment.getContent());

        String authorThumbnail = comment.getAuthorThumbnail();

        Glide.with(mContext).load(authorThumbnail).asBitmap().override(1000, 1000)
            .error(R.drawable.ic_treble_key)
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                    .into(new SimpleTarget<Bitmap>(1000, 1000) {
                              @Override
                              public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                  commentViewHolder.mCivImage.setImageBitmap(bitmap);
                              }
                          }
                    );

        commentViewHolder.mTvLike.setText(comment.isLiked() ? "Unlike" : "Like");

        commentViewHolder.mTvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.isLiked()) {
                    unlike(comment);
                } else {
                    like(comment);
                }
            }
        });

        commentViewHolder.mIvLike.setImageResource(comment.isLiked() ? R.drawable.ic_action_like :
                R.drawable.ic_action_like_gray);

        commentViewHolder.mIvLike.setOnClickListener(new LikeBtnClicked(comment));
        commentViewHolder.mTvReplies.setVisibility(comment.hasParent() ? View.GONE : View.VISIBLE);

        commentViewHolder.mTvReplies.setOnClickListener(new ToggleReplyBtnClicked(
            commentViewHolder.mRvReplies));

        commentViewHolder.mVLastDot.setVisibility(comment.hasParent() ? View.GONE : View.VISIBLE);

        commentViewHolder.mIvReply.setVisibility(comment.hasParent() ? View.GONE : View.VISIBLE);
        commentViewHolder.mIvReply.setOnClickListener(new ReplyBtnClicked(comment));
        commentViewHolder.mRvReplies.setVisibility(View.GONE);

        if (!comment.hasParent()) {
            setReplies(commentViewHolder.mRvReplies, commentViewHolder.mTvReplies,
                    comment.getCommentId());
        }
    }

    public void setComments(ArrayList<Comment> comments) {
        mComments = comments;
    }

    private void setReplies(RecyclerView rvReplies, TextView tvReplies, String commentId) {
        CommentListAdapter commentListAdapter = new CommentListAdapter(mContext);
        rvReplies.setAdapter(commentListAdapter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rvReplies.setLayoutManager(mLayoutManager);
        rvReplies.setItemAnimator(new DefaultItemAnimator());

        ArrayList<Comment> comments = (ArrayList) Select.from(Comment.class).where(
            Condition.prop(Comment.COLUMN_PARENT_ID).eq(commentId)).list();

        Log.d("REPLIES", "COMMENT_COUNT: " + comments.size());

        tvReplies.setText(comments.size() + " repl" + ((comments.size() != 1) ? "ies" : "y"));

        commentListAdapter.setComments(comments);
        commentListAdapter.notifyDataSetChanged();

        if (comments.isEmpty()) {
            rvReplies.setVisibility(View.GONE);
        } else {
            rvReplies.setVisibility(View.VISIBLE);
        }
    }

    private class LikeBtnClicked implements View.OnClickListener {

        private Comment mComment = null;

        public LikeBtnClicked(Comment comment) {
            mComment = comment;
        }

        @Override
        public void onClick(View view) {
            if (mComment.isLiked()) {
                unlike(mComment);
            } else {
                like(mComment);
            }
        }
    }

    private class ReplyBtnClicked implements View.OnClickListener {

        private Comment mComment = null;

        public ReplyBtnClicked(Comment comment) {
            mComment = comment;
        }

        @Override
        public void onClick(View view) {
            reply(mComment);
        }
    }

    private class ToggleReplyBtnClicked implements View.OnClickListener {

        private RecyclerView mRvReplies = null;

        public ToggleReplyBtnClicked(RecyclerView rvReplies) {
            mRvReplies = rvReplies;
        }

        @Override
        public void onClick(View view) {
            mRvReplies.setVisibility((mRvReplies.getVisibility() == View.VISIBLE) ? View.GONE :
                View.VISIBLE);
        }
    }

    public int getItemCount() {
        return (mComments == null) ? 0 : mComments.size();
    }

    public void setThemeColor(int themeColor) {
        mThemeColor = themeColor;
    }

    private void like(Comment comment) {
        comment.like();
        comment.save();
        notifyDataSetChanged();
    }

    private void unlike(Comment comment) {
        comment.unlike();
        comment.save();
        notifyDataSetChanged();
    }

    protected void reply(Comment comment) {

    }
}