package arlon.android.portfolio.ndule.interfaces;

import android.util.Log;

/**
 * Created by Arlon Mukeba on 2/6/2018.
 */

public class ResultHandler {

    protected NetworkListener mListener = null;

    public ResultHandler(NetworkListener listener) {
        mListener = listener;
    }

    public void error(String message) {
        Log.d("NETWORKRESPONSE", "ERROR: " + message);

        if (mListener != null) {
            mListener.error(message);
        }
    }

    public void success(Object data) {
        Log.d("NETWORKRESPONSE", "SUCCESS: " + data.toString());

        if (mListener != null) {
            mListener.success(data);
        }
    }
}