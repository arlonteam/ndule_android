package arlon.android.portfolio.ndule.fragments;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.adapters.ItemListAdapter;
import arlon.android.portfolio.ndule.adapters.SongTabAdapter;
import arlon.android.portfolio.ndule.interfaces.SongTabFragmentSwitcher;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Song;
import arlon.android.portfolio.ndule.objects.User;
import arlon.android.portfolio.ndule.screens.HomeScreen;

/**
 * Created by Arlon Mukeba on 1/27/2018.
 */

public class SongTabFragment extends BaseFragment implements SongTabFragmentSwitcher {

    public static final String TAG = "SONG_TAB_FRAGMENT";
    public static final String TAB_PARAM = "TAB_PARAM";
    /*private static final String TYPE_ALL = "TYPE_ALL";
    private static final String TYPE_FAVS = "TYPE_FAVS";
    private static final String TYPE_PLAYLIST = "TYPE_PLAYLIST";*/

    private ArrayList<Song> mSongs = new ArrayList<Song>();

    private TabLayout mTabSlider = null;
    private ViewPager mVpSlider = null;
    private int mSelectTabIndex = 0;
    private TabChangeListener mTabChangeListener = new TabChangeListener();

    public SongTabFragment() {

    }

    public static SongTabFragment newInstance(Hashtable<String, Object> params) {
        SongTabFragment songTabFragment = new SongTabFragment();
        Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_PARAMS, params);
        songTabFragment.setArguments(args);
        return songTabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("SELECTEDTABINDEX", (mParams == null) ? "YES" : "NO");

        if (mParams != null) {
            Log.d("SELECTEDTABINDEX", "NOT NULL");
            if (mParams.containsKey(TAB_PARAM)) {
                Log.d("SELECTEDTABINDEX", "HERE");
                mSelectTabIndex = Integer.parseInt((String) mParams.get(TAB_PARAM));
                Log.d("SELECTEDTABINDEX", "Index: " + mSelectTabIndex);
            }
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_song_tab;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
//        populateViews();
        startSongSelection();
    }

    protected void handlePermResult(boolean permGranted) {
        if (permGranted) {
            super.initFragment(null);
        } else {
            populateViews();
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        populateViews();
    }

    protected DataFetcher getLoader() {
        return new LocalSongFetcher(getContext());
    }

    private static class LocalSongFetcher extends DataFetcher {

        public LocalSongFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            deleteLocalSongs();
            retrieveLocalSongs();
            return null;
        }

        private static void deleteLocalSongs() {
            ArrayList<Song> songs = new ArrayList<Song>(Select.from(Song.class).where(Condition.prop(
                Song.COLUMN_TYPE).eq(Song.SONG_TYPE_LOCAL)).list());

            Song.deleteInTx(songs);
        }

        private void retrieveLocalSongs() {
            ContentResolver contentResolver = getContext().getContentResolver();
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
            String order = MediaStore.Audio.Media.TITLE + " ASC";
            Cursor cursor = contentResolver.query(uri, null, selection, null, order);

            ArrayList<Song> songs = new ArrayList<Song>(Select.from(Song.class).list());
            ArrayList<Artist> artists = new ArrayList<Artist>(Select.from(Artist.class).list());

            Log.d("SONG_LIST", songs.toString());
            Log.d("ARTIST_LIST", artists.toString());

            Artist artist = null;
            Song song = null;

            if ((contentResolver != null) && (cursor.getCount() != 0)) {
                while (cursor.moveToNext()) {
                    Log.d("COLUMN_NAMES", (new ArrayList<String>(Arrays.asList(cursor.getColumnNames())).toString()));
                    Log.d("FILE_DETAILS", "DISPLAY_NAME: " + getValue(cursor, cursor.getColumnIndex("_display_name")));
                    Log.d("FILE_DETAILS", "TITLE: " + getValue(cursor, cursor.getColumnIndex("title")));
                    Log.d("FILE_DETAILS", "ALBUM: " + getValue(cursor, cursor.getColumnIndex("album")));
                    Log.d("FILE_DETAILS", "ALBUM_KEY: " + getValue(cursor, cursor.getColumnIndex("album_key")));
                    Log.d("FILE_DETAILS", "SIZE: " + getValue(cursor, cursor.getColumnIndex("_size")));
                    Log.d("FILE_DETAILS", "ARTIST: " + getValue(cursor, cursor.getColumnIndex("artist")));
                    Log.d("FILE_DETAILS", "DATE_MODIFIED: " + getValue(cursor, cursor.getColumnIndex("date_modified")));
                    Log.d("FILE_DETAILS", "DATA: " + getValue(cursor, cursor.getColumnIndex("_data")));

                    song = new Song();
                    song.setTitle(getValue(cursor, cursor.getColumnIndex("title")));

                    int index = songs.indexOf(song);

                    /*if (index == (-1)) {

                    }*/

                    song.setOriginalUrl(getValue(cursor, cursor.getColumnIndex("_data")));
                    song.setAlbumTitle(getValue(cursor, cursor.getColumnIndex("album")));

                    artist = new Artist();
                    artist.setName(getValue(cursor, cursor.getColumnIndex("artist")));

                    index = artists.indexOf(artist);

                    if (index == (-1)) {
                        artist.save();
                    } else {
                        artist = artists.get(index);
                    }

                    song.setType(Song.SONG_TYPE_LOCAL);
                    song.setArtist(artist);
                    song.save();

                    Log.d("SONG_DETAILS", song.toString());
                }
            }
        }

        private String getValue(Cursor cursor, int index) {
            String value = "";

            switch (cursor.getType(index)) {
                case Cursor.FIELD_TYPE_INTEGER:
                    value = "" + cursor.getInt(index);
                    break;

                case Cursor.FIELD_TYPE_FLOAT:
                    value = "" + cursor.getFloat(index);
                    break;

                case Cursor.FIELD_TYPE_STRING:
                    value = "" + cursor.getString(index);
                    break;

                case Cursor.FIELD_TYPE_BLOB:
                    value = "" + cursor.getBlob(index);
                    break;
            }

            return value;
        }
    }

    @Override
    public void onDetach() {
        mVpSlider.removeOnPageChangeListener(mTabChangeListener);
        super.onDetach();
    }

    private void setupViews() {
        mTabSlider = (TabLayout) getView().findViewById(R.id.tabFragmentSlider);
        mVpSlider = (ViewPager) getView().findViewById(R.id.vpFragmentSlider);
        mVpSlider.addOnPageChangeListener(mTabChangeListener);
    }

    protected void populateViews() {
        populateFragmentPager();
        populateTabLayout();
    }

    private void populateFragmentPager() {
        SongTabAdapter songTabAdapter = new SongTabAdapter(getChildFragmentManager());

        ArrayList<Fragment> fragmentList = new ArrayList<Fragment>() {
            {
                SongListFragment songListFragment = SongListFragment.newInstance(
                    new Hashtable<String, Object>() {
                        {
                            put(SongListFragment.SONG_LIST_TYPE_PARAM, ItemListAdapter.TYPE.ALL);
                        }
                    }
                );

                songListFragment.setTabSwitcher(SongTabFragment.this);

                add(songListFragment);

                add(
                    ArtistListFragment.newInstance(
                        new Hashtable<String, Object>() {
                            {
                                put(ArtistListFragment.SONG_LIST_TYPE_PARAM,
                                    ItemListAdapter.TYPE.ARTIST);
                            }
                        }
                    )
                );

                User user = Ndule.getUser();

                if ((user != null) && (user.isLoggedIn())) {
                    songListFragment = SongListFragment.newInstance(
                            new Hashtable<String, Object>() {
                                {
                                    put(SongListFragment.SONG_LIST_TYPE_PARAM, ItemListAdapter.TYPE.FAVS);
                                }
                            }
                    );

                    songListFragment.setTabSwitcher(SongTabFragment.this);

                    add(songListFragment);

                    songListFragment = SongListFragment.newInstance(
                            new Hashtable<String, Object>() {
                                {
                                    put(SongListFragment.SONG_LIST_TYPE_PARAM,
                                            ItemListAdapter.TYPE.PLAYLIST);
                                }
                            }
                    );

                    songListFragment.setTabSwitcher(SongTabFragment.this);

                    add(songListFragment);
                }
            }
        };

        songTabAdapter.setFragmentList(fragmentList);

        mVpSlider.setAdapter(songTabAdapter);
    }

    private void populateTabLayout() {
        mTabSlider.setupWithViewPager(mVpSlider);

        mTabSlider.getTabAt(0).setText("All");
        mTabSlider.getTabAt(0).setIcon(R.drawable.ic_action_list);
        mTabSlider.getTabAt(1).setText("Artists");
        mTabSlider.getTabAt(1).setIcon(R.drawable.ic_action_artist);

        User user = Ndule.getUser();

        if ((user != null) && (user.isLoggedIn())) {
            mTabSlider.getTabAt(2).setText("Favorite");
            mTabSlider.getTabAt(2).setIcon(R.drawable.ic_action_favorite);
            mTabSlider.getTabAt(3).setText("Playlist");
            mTabSlider.getTabAt(3).setIcon(R.drawable.ic_action_playlist);
        }

        Log.d("SELECTEDTABINDEX", mSelectTabIndex + "");

        mVpSlider.setCurrentItem(mSelectTabIndex);
    }

    public int getFabIconResId() {
        return -1;
    }

    protected int getMenuResId() {
        return R.menu.menu_song_tab_fragment;
    }

    public void handleLoginRequest() {
        mFragmentSwitcher.login();
    }

    private class TabChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            mSelectTabIndex = position;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("LOGINREQUEST", "FROM FRAGMENT: REQUEST: " + requestCode);

        if ((requestCode == HomeScreen.LOGIN_CODE) && (resultCode == Activity.RESULT_OK)) {
            initFragment(null);
        }
    }
}