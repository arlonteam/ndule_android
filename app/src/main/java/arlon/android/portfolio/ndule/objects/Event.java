package arlon.android.portfolio.ndule.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

/**
 * Created by Arlon Mukeba on 3/3/2018.
 */

public class Event extends SugarRecord implements Parcelable {

    public final static String EVENT = "event";

    public final static String COLUMN_ID = "event_id";
    public final static String COLUMN_TIMESTAMP = "timestamp";
    public final static String COLUMN_THUMBNAIL = "thumbnail";

    @Column(name = COLUMN_ID)
    private String mEventId = "";

    @Column(name = COLUMN_TIMESTAMP)
    private String mTimeStamp = "";

    @Column(name = COLUMN_THUMBNAIL)
    private String mThumbnail = "";

    public Event() {
    }

    public Event(Parcel in) {
        setId(in.readLong());
        setEventId(in.readString());
        setThumbnail(in.readString());
    }

    public void setEventId(String eventId) {
        mEventId = eventId;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public String getEventId() {
        return mEventId;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {

        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getEventId());
        dest.writeString(getThumbnail());
    }

    public long save() {
        if (mTimeStamp.isEmpty()) {
            mTimeStamp = "" + System.currentTimeMillis();
        }

        if (mEventId.isEmpty()) {
            mEventId = "Event_" + mTimeStamp;
        }

        return super.save();
    }

    public String toString () {
        return new StringBuffer("EVENT: ").append("\n\n")
            .append("Id: ").append(mEventId).append("\n")
                .append("Thumbnail: ").append(mThumbnail).append("\n\n").toString();
    }
}