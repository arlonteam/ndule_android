package arlon.android.portfolio.ndule.helpers;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.google.zxing.common.BitMatrix;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class PixHelper  {

//    private static String URL = CineKin.SCRIPT_URL + "server.php";
    public static final String PROFILES_IMAGE_URL = "PROFILES_IMAGE_URL";
    public static final String ADS_IMAGE_URL = "ADS_IMAGE_URL";

    public static Bitmap getBitmapFromMemory(String path) {
        return BitmapFactory.decodeFile(path);
    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static String getRealPathFromURI(Context context, Bitmap bitmap) {
        Cursor cursor = context.getContentResolver().query(getImageUri(context, bitmap), null, null,
                null, null);

        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String path = cursor.getString(idx);
        cursor.close();

        return path;
    }

    private static Uri getImageUri(Context context, Bitmap bitmap) {
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap,
                "Title", null);

        return Uri.parse(path);
    }

    public static void saveImageToMemory(Bitmap bitmap, String path) {
        File file = new File(path);

        if (file.exists()) {
            file.delete();
        }

        try {
            FileOutputStream fileOutStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutStream);
            fileOutStream.flush();
            fileOutStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap BitMatrixToBitmap(BitMatrix bitMatrix) {
        int height = bitMatrix.getHeight();
        int width = bitMatrix.getWidth();

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++){
                bitmap.setPixel(x, y, bitMatrix.get(x,y) ? Color.BLACK : Color.WHITE);
            }
        }

        return bitmap;
    }

    /*public static void getAllPictures(Context context, NetworkListener networkListener) {
        String url = URL;
        url += "?" + CustomRequest.OBJECT_PARAM + "=" + CustomRequest.PICTURE;

        Log.d("PICTURE_REQUEST_URL", url);

        CustomRequest customRequest = new CustomRequest(context, Request.Method.GET, url,
                null, new PixListResultHandler(context, PixListResultHandler.PIX_OPTIONS.ALL_PICTURES,
                networkListener));

        NetworkHelper networkHelper = NetworkHelper.getNetworkManager(context);
        networkHelper.makeRequest(customRequest);
    }*/
}