package arlon.android.portfolio.ndule.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import com.orm.query.Select;
import java.util.ArrayList;
import java.util.Hashtable;
import arlon.android.portfolio.ndule.*;
import arlon.android.portfolio.ndule.adapters.EventListAdapter;
import arlon.android.portfolio.ndule.helpers.DialogHelper;
import arlon.android.portfolio.ndule.objects.Event;

/**
 * Created by Arlon Mukeba on 11/16/2017.
 */

public class EventFragment extends BaseFragment {

    public static final String TAG = "EVENT_FRAGMENT";

    private RecyclerView mRvEvents = null;
    private EventListAdapter mEventListAdapter = null;

    private Context mContext = null;

    public EventFragment() {
    }

    public static EventFragment newInstance(Hashtable<String, Object> params) {
        EventFragment fragment = new EventFragment();
        /*Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_PARAMS, params);
        fragment.setArguments(args);*/
        return fragment;
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_events;
    }

    /*protected int getMenuResId() {
        return R.menu.menu_feed_fragment;
    }*/

    public int getFabIconResId() {
        return -1; //R.drawable.ic_action_refresh;
    }

    public void handleFabClick(View view) {
        DialogHelper.showMessage(getContext(), "Tagging a Friend",
                "This will allow any user to tag their friend on a picture", null);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        mContext = getContext();
        setupViews();
        super.initFragment(savedInstanceState);
    }

    private void setupViews() {
        mEventListAdapter = new EventListAdapter(getContext());
        mRvEvents = (RecyclerView) getView().findViewById(R.id.rvEvents);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());

        mRvEvents.setLayoutManager(mLayoutManager);
        mRvEvents.setItemAnimator(new DefaultItemAnimator());
        mRvEvents.setAdapter(mEventListAdapter);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        loadEvents(data);
    }

    private void loadEvents(Object data) {
        ArrayList<Event> eventItems = (ArrayList<Event>) data;
        Log.d("EVENTITEMS", eventItems.size() + "");
        mEventListAdapter.setEventItems(eventItems);
        mEventListAdapter.notifyDataSetChanged();
    }

    protected DataFetcher getLoader() {
        return new HomeDataFetcher(getContext());
    }

    private static class HomeDataFetcher extends DataFetcher {

        public HomeDataFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            return Select.from(Event.class).list();
        }
    }
}