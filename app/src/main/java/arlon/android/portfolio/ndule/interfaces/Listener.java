package arlon.android.portfolio.ndule.interfaces;

import android.content.Context;
import android.util.Log;
import com.android.volley.Response;
import org.json.JSONObject;
import arlon.android.portfolio.ndule.helpers.JsonHelper;

/**
 * Created by Arlon Mukeba on 2/6/2018.
 */

public class Listener implements Response.Listener<JSONObject> {

    private Context mContext = null;
    private ResultHandler mResultHandler = null;

    public Listener(Context context, ResultHandler resultHandler) {
        mContext = context;
        mResultHandler = resultHandler;
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.d("OBJECTRETURNED", "DATA: " + response.toString());

        JSONObject data = response;

        Log.d("OBJECTRETURNED", "DATA2: " + response.toString());

        if (response.has("data")) {
            data = JsonHelper.getObject(response, "data");
        }

        Log.d("OBJECTRETURNED", "DATA3: " + data.toString());

        mResultHandler.success(data);
    }
}