package arlon.android.portfolio.ndule.screens;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.fragments.BaseFragment;
import arlon.android.portfolio.ndule.fragments.ContactUsFragment;
import arlon.android.portfolio.ndule.fragments.CouponScannerFragment;
import arlon.android.portfolio.ndule.fragments.EventFragment;
import arlon.android.portfolio.ndule.fragments.HomeFragment;
import arlon.android.portfolio.ndule.fragments.ProfileFragment;
import arlon.android.portfolio.ndule.fragments.SignUpFragment;
import arlon.android.portfolio.ndule.fragments.SongSearchFragment;
import arlon.android.portfolio.ndule.fragments.SongTabFragment;
import arlon.android.portfolio.ndule.helpers.PixHelper;
import arlon.android.portfolio.ndule.interfaces.FragmentSwitcher;
import arlon.android.portfolio.ndule.objects.User;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentSwitcher/*, Observer*/ {

    public enum FRAGMENT_TYPE {
        NONE, DEFAULT, HOME, EVENTS, CONTACT_US, ME, PLAYLIST, SEARCH, COUPON_SCANNING, FRIENDS, NOTIFICATIONS
    };

    public static final int LOGIN_CODE = 123;
    public static final String HOME_SCREEN = "HOME_SCREEN";

    private BaseFragment mCurrentFragment = null;
    private DrawerLayout mDrawer = null;
    private ActionBarDrawerToggle mDrawerToggle = null;
    private NavigationView mNavigationView = null;
    private CircleImageView mCivProfilePix = null;
    private int mTitleResId = R.string.app_name;
    private FloatingActionButton mFab = null;
    private int mSelectedMenuItemId = R.id.nav_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_screen);

        setViews();
        populateViews();

        switchFragment(mSelectedMenuItemId);
    }

    @Override
    protected void onStop() {
        super.onStop();

//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mDrawer.setEnabled(false);

        toggleLoginStatus();

        /*LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver,
                new IntentFilter(MessagingService.TAG));*/
    }

    @Override
    protected void onPause() {
        super.onPause();

//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
    }

    private void setViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbToolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
            this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nvView);
        mNavigationView.setNavigationItemSelectedListener(this);

        mCivProfilePix = (CircleImageView) findViewById(R.id.civProfilePix);

        mFab = (FloatingActionButton) findViewById(R.id.fab);

        findViewById(R.id.rlNavHeader).setOnClickListener(null);
    }

    private void populateViews() {
        invalidateOptionsMenu();
        setNavigationView();
        populateNavMenu();
    }

    private void setNavigationView() {
        mNavigationView.getMenu().clear();
        mNavigationView.inflateMenu(R.menu.activity_home_screen_drawer);
        mNavigationView.setCheckedItem(mSelectedMenuItemId);
    }

    private void setFabIcon() {
        int fabIconResId = mCurrentFragment.getFabIconResId();

        if (fabIconResId == (-1)) {
            mFab.setVisibility(View.GONE);
            return;
        }

        mFab.setImageResource(fabIconResId);
        mFab.setVisibility(View.VISIBLE);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentFragment.handleFabClick(view);
            }
        });

        int gravity = Gravity.BOTTOM;

        if (mCurrentFragment instanceof HomeFragment) {
            gravity |= Gravity.LEFT;
        }/*else if (mCurrentFragment instanceof WallOfFameFragment) {
            gravity |= Gravity.CENTER_HORIZONTAL;
        }  else if (mCurrentFragment instanceof FacesFragment) {

        }*/ else {
            gravity |= Gravity.LEFT;
        }

        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) mFab.getLayoutParams();
        lp.gravity = gravity;
        mFab.setLayoutParams(lp);
    }

    private void switchFragment(int id) {
        boolean shouldAdd = false;
        int titleResId = -1;
        mSelectedMenuItemId = id;
        FRAGMENT_TYPE fragmentType = FRAGMENT_TYPE.NONE;
        Hashtable<String, Object> parameters = new Hashtable<String, Object>();

        switch (id) {
            case R.id.nav_home:
                fragmentType = FRAGMENT_TYPE.HOME;
//                titleResId = R.string.nav_news;
                break;

            /*case R.id.nav_notifs:
                fragmentType = FRAGMENT_TYPE.NOTIFICATIONS;
                break;*/

            case R.id.nav_me:
                fragmentType = FRAGMENT_TYPE.ME;
//                titleResId = R.string.nav_news;
                break;

            case R.id.nav_events:
                fragmentType = FRAGMENT_TYPE.EVENTS;
                break;

            case R.id.nav_playlist:
                fragmentType = FRAGMENT_TYPE.PLAYLIST;
                parameters.put(SongTabFragment.TAB_PARAM, "3");
//                titleResId = R.string.nav_news;
                break;

            case R.id.nav_contact_us:
                fragmentType = FRAGMENT_TYPE.CONTACT_US;
//                titleResId = R.string.nav_direction;
                break;
        }

        Log.d("DEBUGGING", "HERE...");

        mDrawer.closeDrawer(GravityCompat.START);

        if (fragmentType != FRAGMENT_TYPE.NONE) {
            parameters.put(BaseFragment.SCREEN_TITLE_PARAM, "CinéKin"/*getString(titleResId)*/);
            switchFragment(fragmentType, parameters, shouldAdd);

            mNavigationView.getMenu().findItem(mSelectedMenuItemId).setChecked(true);
        }
    }

    public void switchFragment(String fragmentTag, boolean shouldAdd) {
        Hashtable<String, Object> parameters = new Hashtable<String, Object>();
        parameters.put(BaseFragment.SCREEN_TITLE_PARAM, "Extended Profile");
        switchFragment(FRAGMENT_TYPE.DEFAULT, parameters, true);
    }

    public void switchFragment(String fragmentTag, Hashtable<String, Object> parameters,
                               boolean shouldAdd) {

        FRAGMENT_TYPE fragmentType = FRAGMENT_TYPE.NONE;

        Log.d("PARAMETERS3", (fragmentTag != null) ? fragmentTag : "NULL");

        switch (fragmentTag) {
            case HomeFragment.TAG:
                fragmentType = FRAGMENT_TYPE.HOME;
                break;

            case ProfileFragment.TAG:
                fragmentType = FRAGMENT_TYPE.ME;
                break;

            case SongTabFragment.TAG:
                fragmentType = FRAGMENT_TYPE.PLAYLIST;
                break;

            case SongSearchFragment.TAG:
                fragmentType = FRAGMENT_TYPE.SEARCH;
                break;

            case CouponScannerFragment.TAG:
                fragmentType = FRAGMENT_TYPE.COUPON_SCANNING;
                break;

            case ContactUsFragment.TAG:
                fragmentType = FRAGMENT_TYPE.CONTACT_US;
                break;

            /*case NotificationFragment.TAG:
                fragmentType = FRAGMENT_TYPE.NOTIFICATIONS;
                break;*/

            default:
                fragmentType = FRAGMENT_TYPE.DEFAULT;
                break;
        }

        switchFragment(fragmentType, parameters, shouldAdd);
    }

    private void switchFragment(FRAGMENT_TYPE fragmentType, Hashtable<String, Object> parameters,
                                boolean shouldAdd) {

        BaseFragment fragment = null;
        mTitleResId = R.string.app_name;

        switch (fragmentType) {
            case HOME:
                fragment = HomeFragment.newInstance(parameters);
//                parameters.put(BaseFragment.USER_PARAM, user);
                break;

            case ME:
                fragment = ProfileFragment.newInstance(parameters);
//                parameters.put(BaseFragment.USER_PARAM, user);
                break;

            case EVENTS:
                mTitleResId = R.string.ef_title;
                fragment = EventFragment.newInstance(parameters);
//                parameters.put(BaseFragment.USER_PARAM, user);
                break;

            case PLAYLIST:
                fragment = SongTabFragment.newInstance(parameters);
                mTitleResId = R.string.sf_title;
                mSelectedMenuItemId = R.id.nav_playlist;
//                parameters.put(BaseFragment.USER_PARAM, user);
                break;

            case SEARCH:
                fragment = SongSearchFragment.newInstance(parameters);
                mTitleResId = R.string.sf_title;
                mSelectedMenuItemId = R.id.nav_playlist;
//                parameters.put(BaseFragment.USER_PARAM, user);
                break;

            case COUPON_SCANNING:
                fragment = CouponScannerFragment.newInstance(parameters);
//                parameters.put(BaseFragment.USER_PARAM, user);
                break;

            case CONTACT_US:
                mNavigationView.setCheckedItem(R.id.nav_contact_us);
                fragment = ContactUsFragment.newInstance(parameters);
//                ((ProfileFragment) fragment).setProfileUpdateListener(this);
//                mPhotoHelper.addObserver(fragment);
//                parameters.put(BaseFragment.USER_PARAM, user);
                break;

            /*case NOTIFICATIONS:
                fragment = NotificationFragment.newInstance(parameters);
                fragment.setFragmentSwitcher(this);
//                parameters.put(BaseFragment.USER_PARAM, user);
                mTitleResId = R.string.nf_heading;
                break;*/
        }

        if (fragment == null) {
            return;
        }

        fragment.setFragmentSwitcher(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Log.d("DEBUGGING", "SHOULD_ADD: " + fragmentType);

        if (shouldAdd) {
            transaction.addToBackStack((String) parameters.get(BaseFragment.SCREEN_TITLE_PARAM));
            transaction.add(R.id.content_frame, fragment);
        } else {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            transaction.replace(R.id.content_frame, fragment);
        }

        mCurrentFragment = fragment;

        transaction.commit();

        setFabIcon();

        setTitle(getString(mTitleResId));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        Log.d("NEW_FRAGMENT_ID", "" + (mSelectedMenuItemId == R.id.nav_playlist));

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (mSelectedMenuItemId == R.id.nav_home) {
            super.onBackPressed();
        } else if (mSelectedMenuItemId == R.id.nav_me) {
            if (mCurrentFragment instanceof ProfileFragment) {
                if (!((ProfileFragment) mCurrentFragment).handleBack()) {
                    goToFragment(R.id.nav_home);
                }
            } else if (mCurrentFragment instanceof CouponScannerFragment) {
                ((CouponScannerFragment) mCurrentFragment).handleBack();
                goToFragment(R.id.nav_me);
            }
        } else {
            Log.d("NEW_FRAGMENT_ID", "2. " + (mSelectedMenuItemId == R.id.nav_playlist));
            goToFragment(R.id.nav_home);
        }
    }

    private void goToFragment(int navMenuId) {
        mNavigationView.getMenu().findItem(mSelectedMenuItemId).setChecked(false);
        switchFragment(navMenuId);
    }

    public void navigateBack(int code) {
        onBackPressed();
    }

    public void toggleFabIcon(boolean visible) {
        mFab.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public void toggleLoginStatus() {
        populateNavMenu();
    }

    private void populateNavMenu() {
        toggleNavMenu();
        changeProfileDetails();
    }

    private void toggleNavMenu() {
        mNavigationView.getMenu().findItem(R.id.nav_me).setVisible(isUserLoggedIn());
//        mNavigationView.getMenu().findItem(R.id.nav_settings).setVisible(isUserLoggedIn());
        mNavigationView.getMenu().findItem(R.id.nav_login).setVisible(!isUserLoggedIn());
        mNavigationView.getMenu().findItem(R.id.nav_playlist).setVisible(isUserLoggedIn());
        mNavigationView.getMenu().findItem(R.id.nav_logout).setVisible(isUserLoggedIn());
        mNavigationView.invalidate();
    }

    public void showNotification(boolean showDrawer) {
        if (showDrawer) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(1000);

            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        }

//        updateNotificationCount();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            switchFragment(SongSearchFragment.TAG, new Hashtable<String, Object>(), false);
            return true;
        }

        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        boolean itemSelected = true;
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_login) {
            itemSelected = false;
            login();
        } else if (id == R.id.nav_logout) {
            itemSelected = false;
            logout();
        } /*else if (id == R.id.nav_gallery) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        mNavigationView.getMenu().findItem(mSelectedMenuItemId).setChecked(false);

        if (itemSelected) {
            switchFragment(id);

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void login() {
        Intent intent = new Intent(HomeScreen.this, LoginScreen.class);
        intent.putExtra(LoginScreen.SCREEN_PARAM, HOME_SCREEN);
        startActivityForResult(intent, LOGIN_CODE);
    }

    private void logout() {
        User user = Ndule.getUser();
        user.setLogInStatus(User.LOGIN_STATUS_LOGGED_OUT);
        user.save();

        toggleLoginStatus();

        String message = getString(R.string.pf_logout);
        message = String.format(message, user.getFirstName());
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

        switchFragment(R.id.nav_home);
    }

    public void changeProfileDetails(/*String profilePixPath*/) {
        setUserDetails();
        setProfileImage();
    }

    private void setUserDetails() {
        /*mTvUsername.setVisibility(isUserLoggedIn() ? View.VISIBLE : View.GONE);
        mIvProfilePixArrow.setVisibility(isUserLoggedIn() ? View.GONE : View.VISIBLE);

        if (isUserLoggedIn()) {
            User user = CineKin.getUser();

            String userPseudo = user.getFirstName() + " " + user.getSurname();

            if (userPseudo.isEmpty()) {
                userPseudo = user.getUsername();
            }

            mTvUsername.setText(userPseudo);
        }*/
    }

    private void setProfileImage() {
        User me = Ndule.getUser();

        if (me == null) return;

        if (!me.getPixUrl().isEmpty()) {
            mCivProfilePix.setImageBitmap(PixHelper.getBitmapFromMemory(me.getPixUrl()));
        } else {
            /*int width = 144;
            int height = 144;

//            mPbProfileLoader.setVisibility(View.VISIBLE);
//            mTvProfileTxt.setVisibility(View.GONE);

            Glide.with(this).load(me.getPixUrl()).asBitmap().override(width, height)
                    .placeholder(R.drawable.ic_unknown_user).error(R.drawable.ic_unknown_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                    .into(new SimpleTarget<Bitmap>(width, height) {
                              @Override
                              public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                  mCivProfilePix.setImageBitmap(bitmap);
//                                mPbProfileLoader.setVisibility(View.GONE);
                              }
                          }
                    );*/
        }

//        mDrawer.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("PERMISSIONREQUESTED", "PARENT");

        if ((requestCode == LOGIN_CODE) && (resultCode == Activity.RESULT_OK)) {
            toggleLoginStatus();

            String message = getString(R.string.pf_login);
            message = String.format(message, Ndule.getUser().getFirstName());
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();

            mCurrentFragment.onActivityResult(requestCode, resultCode, data);
        }

        Log.d("PERMISSIONREQUESTED", "PARENT2");

        super.onActivityResult(requestCode, resultCode, data);

        Log.d("PERMISSIONREQUESTED", "PARENT2");
    }

    private boolean isUserLoggedIn() {
        User user = Ndule.getUser();

        return ((user != null) && (user.isLoggedIn()));
    }
}