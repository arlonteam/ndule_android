package arlon.android.portfolio.ndule.fragments;

import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.DateHelper;
import arlon.android.portfolio.ndule.helpers.PixHelper;
import arlon.android.portfolio.ndule.objects.User;
import arlon.android.portfolio.ndule.utils.StringUtils;

/**
 * Created by Arlon Mukeba on 1/22/2018.
 */

public class CouponScannerFragment extends BaseFragment {

    public static final String TAG = "COUPON_SCANNER_FRAGMENT";

    private int mColor = -1;
    private int mValue = 100;
    private String mExpiryDate = "02/11/2017";
    private User mUser = Ndule.getUser();

    private TextView mTvHeading = null;
    private TextView mTvCouponCode = null;
    private TextView mTvCouponValue = null;
    private TextView mTvCouponExpiry = null;
    private TextView mTvCouponAdd = null;
    private RelativeLayout mRlDecoderView = null;
    private QRCodeReaderView mQCRVScanner = null;
    private ImageView mIvDecodedQr = null;

    public CouponScannerFragment() {

    }

    public static CouponScannerFragment newInstance(Hashtable<String, Object> params) {
        CouponScannerFragment couponScannerFragment = new CouponScannerFragment();
        return couponScannerFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("PERMISSIONREQUESTED", "ONCREATECALLED");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_coupon_scanner;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        populateViews();
        toggleQR(false);
    }

    private void setupViews() {
        mColor = getContext().getResources().getColor(R.color.blue);

        setupScanner();
        setupOthers();
    }

    private void setupOthers() {
        mTvHeading = (TextView) getView().findViewById(R.id.tvHeading);
        mTvCouponCode = (TextView) getView().findViewById(R.id.tvCouponCode);
        mIvDecodedQr = (ImageView) getView().findViewById(R.id.ivDecodedQr);
        mIvDecodedQr.setOnClickListener(new QRCodeImageClicked());
        mTvCouponValue = (TextView) getView().findViewById(R.id.tvCouponValue);
        mTvCouponExpiry = (TextView) getView().findViewById(R.id.tvCouponExpiry);
        mTvCouponAdd = (TextView) getView().findViewById(R.id.tvCouponAdd);
        mTvCouponAdd.setOnClickListener(new CouponAddClicked());
    }

    private void setupScanner() {
        mRlDecoderView = (RelativeLayout) getView().findViewById(R.id.rlDecoderview);
        mQCRVScanner = (QRCodeReaderView) getView().findViewById(R.id.qrdecoderview);
        mQCRVScanner.setOnQRCodeReadListener(new QRCodeDetectedListener());
        mQCRVScanner.setQRDecodingEnabled(true);
        mQCRVScanner.setAutofocusInterval(2000L);
        mQCRVScanner.setTorchEnabled(true);
        mQCRVScanner.setFrontCamera();
        mQCRVScanner.setBackCamera();
    }

    protected void populateViews() {
        String appName = getContext().getString(R.string.app_name);
        String heading = getContext().getString(R.string.cf_heading);
        heading = String.format(heading, appName);

        mTvHeading.setText(StringUtils.setColourTag(heading, appName, mColor));
    }

    private void startScanner() {
        Log.d("STARTINGCAMERA", "HERE");
        String[] requiredPermissions = requiredPermissions(buildCameraPermissions());
        Log.d("STARTINGCAMERA", "" + new ArrayList<String>(Arrays.asList(requiredPermissions)));

        if (requiredPermissions.length == 0) {
            startScanning();
        } else {
            requestCamPerm(requiredPermissions, SCANNER_REQUEST_CODE);
        }
    }

    protected void handlePermResult() {
        startScanning();
    }

    private void startScanning() {
        toggleQR(true);
        mQCRVScanner.startCamera();
    }

    private void addCoupon() {
        double couponValue = /*Double.parseDouble(*/mUser.getCredit()/*)*/;
        couponValue += mValue;
        mUser.setCredit(couponValue);
        mUser.setCreditExpiryDate(mExpiryDate);
        mUser.save();
        Ndule.setUser(mUser);
        mFragmentSwitcher.navigateBack(-1);
    }

    private void toggleQR(boolean startQR) {
        mTvCouponCode.setVisibility(View.GONE);
        mTvCouponValue.setVisibility(View.GONE);
        mTvCouponExpiry.setVisibility(View.GONE);
        mTvCouponAdd.setVisibility(View.GONE);
        mIvDecodedQr.setVisibility(startQR ? View.GONE : View.VISIBLE);
        mRlDecoderView.setVisibility(startQR ? View.VISIBLE : View.GONE);
    }

    private void displayQRCode(String qrCode) {
        mQCRVScanner.stopCamera();
        toggleQR(false);

        mTvCouponCode.setText(qrCode);

        String[] coupon_details = qrCode.split("NDULE");

        String couponValue = getContext().getString(R.string.cf_coupon_value);
        mValue = 100;

        try {
            mValue = Integer.parseInt(coupon_details[0]);
        } catch (Exception e) {

        }

        couponValue = String.format(couponValue, ("" + mValue));
        mTvCouponValue.setText(StringUtils.setColourTag(couponValue, ("" + mValue), mColor));

        String couponExpiry = getContext().getString(R.string.cf_coupon_expiry);
        mExpiryDate = "02/11/2017";

        try {
            DateHelper.dateFromString(coupon_details[1]);
            mExpiryDate = coupon_details[1];
        } catch (Exception e) {

        }

        couponExpiry = String.format(couponExpiry, mExpiryDate);
        mTvCouponExpiry.setText(StringUtils.setColourTag(couponExpiry, mExpiryDate, mColor));

        QRCodeWriter qrCodeWriter = new QRCodeWriter();

        try {
            BitMatrix bitMatrix = qrCodeWriter.encode(qrCode, BarcodeFormat.QR_CODE, 250, 250);
            mIvDecodedQr.setImageBitmap(PixHelper.BitMatrixToBitmap(bitMatrix));
        } catch (Exception e) {

        }

        mTvCouponCode.setVisibility(View.VISIBLE);
        mTvCouponValue.setVisibility(View.VISIBLE);
        mTvCouponExpiry.setVisibility(View.VISIBLE);
        mTvCouponAdd.setVisibility(View.VISIBLE);
    }

    public int getFabIconResId() {
        return -1;
    }

    protected int getMenuResId() {
        return -1;
    }

    private class QRCodeImageClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startScanner();
        }
    }

    private class QRCodeDetectedListener implements QRCodeReaderView.OnQRCodeReadListener {

        @Override
        public void onQRCodeRead(String qrCode, PointF[] points) {
            displayQRCode(qrCode);
        }
    }

    private class CouponAddClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            addCoupon();
        }
    }

    public void handleBack() {
        mQCRVScanner.stopCamera();
    }
}