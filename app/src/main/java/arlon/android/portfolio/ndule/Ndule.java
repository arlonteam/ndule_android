package arlon.android.portfolio.ndule;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.orm.SugarContext;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Observer;
import java.util.Random;

import arlon.android.portfolio.ndule.helpers.SongHelper;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.LangChanger;
import arlon.android.portfolio.ndule.objects.Song;
import arlon.android.portfolio.ndule.objects.User;

/**
 * Created by Arlon Mukeba on 1/12/2018.
 */

public class Ndule extends Application {

//    public static final String SONG_IMAGE_URL = "http://ndule.skymenji.com/backend/images/songs/";
    public static final String SONG_IMAGE_URL = "http://10.0.3.2/Ndule/images/songs/";
//    public static final String ARTIST_IMAGE_URL = "http://ndule.skymenji.com/backend/images/artists/";
    public static final String ARTIST_IMAGE_URL = "http://10.0.3.2/Ndule/images/artists/";
//    public static final String SONG_URL = "http://ndule.skymenji.com/backend/songs/";
    public static final String SONG_URL = "http://10.0.3.2/Ndule/songs/";
    //    public static final String EVENT_URL = "http://ndule.skymenji.com/backend/events/";
    public static final String EVENT_URL = "http://10.0.3.2/Ndule/events/";

    private static User mUser;
    private static String mDeviceId;
    private static Activity mCurrentActivity = null;
    private static LangChanger mLangChanger = new LangChanger();

    @Override
    public void onCreate() {
        super.onCreate();

        SugarContext.init(this);

        SongHelper.loadDummySongs();
    }

    @Override
    public void onTerminate() {
        SugarContext.terminate();

        super.onTerminate();
    }

    public static void setUser(User user) {
        mUser = user;
    }

    public static User getUser() {
        Log.d("USER_LIST", Select.from(User.class).list().toString());

        if (mUser == null) {
            mUser = Select.from(User.class).where(Condition.prop(User.COLUMN_LOCAL_TYPE)
                    .eq(User.USER_TYPE_ME)).first();
        }

        return mUser;
    }

    public static User getDummy() {
        return Select.from(User.class).where(Condition.prop(User.COLUMN_LOCAL_TYPE)
            .eq(User.USER_TYPE_DUMMY)).first();
    }

    public static String getDeviceId(Context context) {
        if (TextUtils.isEmpty(mDeviceId)) {
            mDeviceId = android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        }

        return mDeviceId;
    }

    public static void changeLanguage(Context context, String langToLoad) {
        mLangChanger.changeLanguage(context, langToLoad);
    }

    public static void setCurrentActivity(Activity currentActivity) {
        mCurrentActivity = currentActivity;
    }

    public static Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public static User getLoggedInUser() {
        return mUser;
    }

    public static void setLoggedInUser(User user) {
        mUser = user;
    }

    public static void addLangObserver(Observer observer) {
        mLangChanger.addObserver(observer);
    }

    public static void removeLangObserver(Observer observer) {
        mLangChanger.deleteObserver(observer);
    }
}