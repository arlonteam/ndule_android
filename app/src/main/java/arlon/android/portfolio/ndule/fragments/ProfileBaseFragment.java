package arlon.android.portfolio.ndule.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.PhotoCropHelper;
import arlon.android.portfolio.ndule.helpers.PixHelper;
import arlon.android.portfolio.ndule.interfaces.ProfileFragmentSwitcher;
import arlon.android.portfolio.ndule.objects.User;

/**
 * Created by Arlon Mukeba on 1/25/2018.
 */

public class ProfileBaseFragment extends BaseFragment {

    protected ProfileFragmentSwitcher mProfileFragmentSwitcher = null;

    protected void setProfileFragmentSwitcher(ProfileFragmentSwitcher profileFragmentSwitcher) {
        mProfileFragmentSwitcher = profileFragmentSwitcher;
    }
}