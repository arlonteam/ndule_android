package arlon.android.portfolio.ndule.interfaces;

/**
 * Created by Arlon Mukeba on 1/16/2018.
 */

public interface LinkListener {

    public void linkClicked(String link);
}