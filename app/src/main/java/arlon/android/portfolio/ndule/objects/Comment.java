package arlon.android.portfolio.ndule.objects;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import arlon.android.portfolio.ndule.Ndule;

/**
 * Created by Arlon Mukeba on 2/21/2018.
 */

public class Comment extends SugarRecord implements Parcelable {

    public final static String COMMENT = "comment";

    public final static String COLUMN_ID = "comment_id";
    public final static String COLUMN_TIMESTAMP = "timestamp";
    public final static String COLUMN_PARENT_ID = "parent_id";
    public final static String COLUMN_SONG_ID = "song_id";
    public final static String COLUMN_AUTHOR_ID = "user_id";
    public final static String COLUMN_AUTHOR_PSEUDO = "user_name";
    public final static String COLUMN_CONTENT = "content";
    public final static String COLUMN_AUTHOR_THUMBNAIL = "user_thumbnail";
    public final static String COLUMN_LIKE_LIST = "like_list";

    @Column(name = COLUMN_ID)
    private String mCommentId = "";

    @Column(name = COLUMN_TIMESTAMP)
    private String mTimeStamp = "";

    @Column(name = COLUMN_CONTENT)
    private String mContent = "";

    @Column(name = COLUMN_PARENT_ID)
    private String mParentId = "";

    @Column(name = COLUMN_SONG_ID)
    private String mSongId = "";

    @Column(name = COLUMN_AUTHOR_ID)
    private String mAuthorId = "";

    @Column(name = COLUMN_AUTHOR_PSEUDO)
    private String mAuthorPseudo = "";

    @Column(name = COLUMN_AUTHOR_THUMBNAIL)
    private String mAuthorThumbnail = "";

    @Column(name = COLUMN_LIKE_LIST)
    private String mLikeList = "";

    public Comment() {
    }

    public Comment(Parcel in) {
        setId(in.readLong());
        setCommentId(in.readString());
        setTimeStamp(in.readString());
        setContent(in.readString());
        setParentId(in.readString());
        setSongId(in.readString());
        setAuthorId(in.readString());
        setAuthorPseudo(in.readString());
        setAuthorThumbnail(in.readString());
        setLikeList(in.readString());
    }

    public void setCommentId(String commentId) {
        mCommentId = commentId;
    }

    public void setTimeStamp(String timeStamp) {
        mTimeStamp = timeStamp;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setParentId(String parentId) {
        mParentId = parentId;
    }

    public void setSongId(String songId) {
        mSongId = songId;
    }

    public void setAuthorId(String authorId) {
        mAuthorId = authorId;
    }

    public void setAuthorPseudo(String authorPseudo) {
        mAuthorPseudo = authorPseudo;
    }

    public void setAuthorThumbnail(String authorThumbnail) {
        mAuthorThumbnail = authorThumbnail;
    }

    public void setLikeList(String likeList) {
        mLikeList = likeList;
    }

    public String getCommentId() {
        return mCommentId;
    }

    public String getTimeStamp() {
        return mTimeStamp;
    }

    public String getContent() {
        return mContent;
    }

    public String getParentId() {
        return mParentId;
    }

    public boolean hasParent() {
        return !mParentId.isEmpty();
    }

    public String getSongId() {
        return mSongId;
    }

    public String getAuthorId() {
        return mAuthorId;
    }

    public String getAuthorPseudo() {
        return mAuthorPseudo;
    }

    public String getAuthorThumbnail() {
        return mAuthorThumbnail;
    }

    public String getLikeList() {
        return mLikeList;
    }

    public ArrayList<String> getLikeIds() {
        return new ArrayList<>(Arrays.asList(mLikeList.split("|")));
    }

    public void like() {
        ArrayList<String> likeIds = getLikeIds();
        likeIds.add(Ndule.getUser().getEmail());
        mLikeList = TextUtils.join("|", likeIds);
    }

    public void unlike() {
        ArrayList<String> likeIds = getLikeIds();
        likeIds.remove(Ndule.getUser().getEmail());
        mLikeList = TextUtils.join("|", likeIds);
    }

    public boolean isLiked() {
        return mLikeList.contains(Ndule.getUser().getEmail());
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {

        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getCommentId());
        dest.writeString(getTimeStamp());
        dest.writeString(getContent());
        dest.writeString(getParentId());
        dest.writeString(getSongId());
        dest.writeString(getAuthorId());
        dest.writeString(getAuthorPseudo());
        dest.writeString(getAuthorThumbnail());
        dest.writeString(getLikeList());
    }

    public long save() {
        User user = Ndule.getUser();
        String pseudo = (user == null) ? "Anonymous" : user.getFirstName();
        mAuthorId = (user == null) ? ("" + pseudo.hashCode()) : user.getEmail();
        mAuthorPseudo = pseudo;

        if (mCommentId.isEmpty()) {
            mCommentId = "Comment_" + System.currentTimeMillis() + "_" + mSongId + "_" + mAuthorId;
        }

        if (mTimeStamp.isEmpty()) {
            mTimeStamp = new SimpleDateFormat("dd/M/yyyy - hh:mm").format(new Date());
        }

        return super.save();
    }

    public String toString () {
        return new StringBuffer("COMMENT: ").append("\n\n")
            .append("Id: ").append(mCommentId).append("\n")
                .append("Content: ").append(mContent).append("\n")
                    .append("Song Id: ").append(mParentId).append("\n")
                        .append("Author Id: ").append(mAuthorId).append("\n")
                            .append("Author Pseudo: ").append(mAuthorPseudo).append("\n")
                                .append("Author Thumbnail: ").append(mAuthorThumbnail).append("\n\n").toString();
    }
}