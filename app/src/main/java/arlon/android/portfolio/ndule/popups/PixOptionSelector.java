package arlon.android.portfolio.ndule.popups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import java.util.ArrayList;
import java.util.Random;
import arlon.android.portfolio.ndule.R;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class PixOptionSelector extends PopupWindow {

    private Context mContext = null;
    private ArrayList<String> mItems = new ArrayList<String>();
    private RadioGroup mRadioGroup = null;
    private PixChoiceListener mChoiceListener = null;

    public PixOptionSelector(Context context, ArrayList<String> items,
                                 PixChoiceListener choiceListener) {
        super(context);

        mContext = context;
        mItems = items;
        mChoiceListener = choiceListener;

        setPopup();
        setViews();
        setOptions();
    }

    private void setPopup() {
        setBackgroundDrawable(null);
        /*setAnimationStyle(R.anim.popup_anim);*/
        setOutsideTouchable(true);
        setFocusable(true);
        setWidth(350);
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private LayoutInflater getInflater() {
        return (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void setViews() {
        LayoutInflater inflater = getInflater();

        LinearLayout choiceOptionsLayout = (LinearLayout) inflater.inflate(
            R.layout.layout_pix_option_selector, null, false);

        setContentView(choiceOptionsLayout);

        mRadioGroup = (RadioGroup) choiceOptionsLayout.findViewById(R.id.rgOptions);

        choiceOptionsLayout.findViewById(R.id.btnSelect).setOnClickListener(new PopupBtnClicked());
        choiceOptionsLayout.findViewById(R.id.btnCancel).setOnClickListener(new PopupBtnClicked());
    }

    public void setOptions() {
        if (mItems.isEmpty()) return;

        LayoutInflater inflater = getInflater();
        RadioButton  radioBtn = null;
        int id = -1;

        for (int i = 0; i < mItems.size(); i++) {
            id = new Random().nextInt(10000000);

            radioBtn = (RadioButton) inflater.inflate(R.layout.layout_choice, null);
            radioBtn.setId(id);
            radioBtn.setText(mItems.get(i));
            mRadioGroup.addView(radioBtn);

            if (i == 0) {
                mRadioGroup.check(id);
            }
        }
    }

    public interface PixChoiceListener {

        public void optionSelected(String choice);
    }

    private class PopupBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            dismiss();

            if (view.getId() == R.id.btnSelect) {
                RadioButton rbShipping = (RadioButton) mRadioGroup.findViewById(mRadioGroup.
                    getCheckedRadioButtonId());

                String choice = rbShipping.getText().toString();

                mChoiceListener.optionSelected(choice);
            }
        }
    }
}