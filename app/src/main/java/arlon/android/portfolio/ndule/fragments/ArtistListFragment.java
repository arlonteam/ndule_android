package arlon.android.portfolio.ndule.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Hashtable;

import arlon.android.portfolio.ndule.adapters.ItemListAdapter;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Song;

/**
 * Created by Arlon Mukeba on 1/30/2018.
 */

public class ArtistListFragment extends SongListFragment {

    public static final String TAG = "ARTIST_LIST_FRAGMENT";

    public ArtistListFragment() {

    }

    public static ArtistListFragment newInstance(Hashtable<String, Object> params) {
        ArtistListFragment songListFragment = new ArtistListFragment();
        return songListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mItemListType = ItemListAdapter.TYPE.ARTIST;

        return inflater.inflate(getFragmentLayout(), container, false);
    }

    public void onResume() {
        super.onResume();
    }

    protected ItemListAdapter getItemListAdapter() {
        return new ArtistAdapter(mItemListType);
    }

    protected DataFetcher getLoader() {
        return new ArtistListFetcher(getContext());
    }

    private static class ArtistListFetcher extends DataFetcher {

        public ArtistListFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            return Select.from(Artist.class);
        }
    }

    private class ArtistAdapter extends ItemListAdapter {

        private LayoutInflater mLayoutInflater;
        private ArrayList<Song> mSongs;

        public ArtistAdapter(TYPE type) {
            super(getContext(), type);
        }

        protected void setItemClickListener(ItemViewHolder songViewHolder, Object item) {
           songViewHolder.mIvPlay.setOnClickListener(new SongClickListener(songViewHolder.mCivImage,
                ACTION.PLAY, (Artist) item));

            songViewHolder.mLlView.setOnClickListener(new SongClickListener(songViewHolder.mCivImage,
                ACTION.PLAY, (Artist) item));
        }

        private class SongClickListener implements View.OnClickListener {

            private View mView = null;
            private ACTION mAction = ACTION.PLAY;
            private Artist mArtist = null;

            public SongClickListener(View view, ACTION action, Artist artist) {
                mView = view;
                mAction = action;
                mArtist = artist;
            }

            @Override
            public void onClick(View view) {
                play(getActivity(), mView, null, mArtist, true);
            }
        }
    }

    /*@Override
    public void onLoadFinished(Loader loader, Object data) {
        Log.d("ARTIST_LIST_TYPE", mItemListType + " : " + ((ArrayList<Object>) data).size());

        super.onLoadFinished(loader, data);
    }*/
}