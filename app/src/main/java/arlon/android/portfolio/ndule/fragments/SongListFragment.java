package arlon.android.portfolio.ndule.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.adapters.ItemListAdapter;
import arlon.android.portfolio.ndule.helpers.DialogHelper;
import arlon.android.portfolio.ndule.interfaces.SongTabFragmentSwitcher;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Song;
import arlon.android.portfolio.ndule.objects.User;

/**
 * Created by Arlon Mukeba on 1/19/2018.
 */

public class SongListFragment extends BaseFragment {

    public static final String TAG = "SONG_LIST_FRAGMENT";
    public static final String SONG_LIST_TYPE_PARAM = "SONG_LIST_TYPE_PARAM";

    private RecyclerView mRvSongList = null;
    protected ItemListAdapter.TYPE mItemListType = ItemListAdapter.TYPE.ALL;
    protected SongTabFragmentSwitcher mTabFragmentSwitcher = null;

    public SongListFragment() {

    }

    public static SongListFragment newInstance(Hashtable<String, Object> params) {
        SongListFragment songListFragment = new SongListFragment();
        Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_PARAMS, params);
        songListFragment.setArguments(args);
        return songListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mParams != null) {
            if (mParams.containsKey(SONG_LIST_TYPE_PARAM)) {
                mItemListType = (ItemListAdapter.TYPE) mParams.get(SONG_LIST_TYPE_PARAM);
            }
        }

        return inflater.inflate(getFragmentLayout(), container, false);
    }

    public void onResume() {
        super.onResume();

        refresh(null);
    }

    public void setTabSwitcher(SongTabFragmentSwitcher tabFragmentSwitcher) {
        mTabFragmentSwitcher = tabFragmentSwitcher;
    }

    public void refresh(@Nullable Bundle savedInstanceState) {
        super.initFragment(savedInstanceState);
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_song_list;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
//        super.initFragment(savedInstanceState);
    }

    private void setupViews() {
        mRvSongList = (RecyclerView) getView().findViewById(R.id.rvItemList);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        loadSongs((Select) data);
    }

    protected void populateViews() {
        refresh(null);
    }

    private void loadSongs(Select select) {
        ItemListAdapter artistAdapter = getItemListAdapter();

        if (mItemListType == ItemListAdapter.TYPE.FAVS) {
            select = select.where(Condition.prop(Song.COLUMN_LIKED).eq("YES"));
        } else if (mItemListType == ItemListAdapter.TYPE.PLAYLIST) {
            select = select.where(Condition.prop(Song.COLUMN_ADDED_TO_PLAYLIST).eq("YES"));
        }

        ArrayList<Object> songs = (ArrayList<Object>) select/*.limit("5")*/.list();
        Collections.sort(songs, new SongComparator());

        artistAdapter.setItems(songs);

//        Log.d("SONG_LIST_TYPE", mItemListType + " : " + songs.size());

        mRvSongList.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvSongList.setItemAnimator(new DefaultItemAnimator());
        mRvSongList.setAdapter(artistAdapter);
    }

    protected ItemListAdapter getItemListAdapter() {
        return new SongAdapter(mItemListType);
    }

    public int getFabIconResId() {
        return -1;
    }

    protected DataFetcher getLoader() {
        return new HomeDataFetcher(getContext());
    }

    private static class HomeDataFetcher extends DataFetcher {

        public HomeDataFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            return Select.from(Song.class);
        }
    }

    private class SongAdapter extends ItemListAdapter {

        private LayoutInflater mLayoutInflater;
        private ArrayList<Song> mSongs;

        public SongAdapter(TYPE type) {
            super(getContext(), type);
        }

        protected void setItemClickListener(ItemViewHolder songViewHolder, Object item) {
            songViewHolder.mIvAddToPlaylist.setOnClickListener(new SongClickListener(
                songViewHolder.mCivImage, ACTION.ADDTOPLAYLIST, (Song) item));

            songViewHolder.mIvAddToFav.setOnClickListener(new SongClickListener(
                songViewHolder.mCivImage, ACTION.ADDTOFAVS, (Song) item));

            songViewHolder.mLlView.setOnClickListener(new SongClickListener(songViewHolder.mCivImage,
                ACTION.PLAY, (Song) item));

            songViewHolder.mIvPlay.setOnClickListener(new SongClickListener(songViewHolder.mCivImage,
                    ACTION.PLAY, (Song) item));

            songViewHolder.mIvRemove.setOnClickListener(new SongClickListener(
                songViewHolder.mCivImage, ACTION.REMOVE, (Song) item));
        }

        private class SongClickListener implements View.OnClickListener {

            private View mView = null;
            private ACTION mAction = ACTION.PLAY;
            private Song mSong = null;

            public SongClickListener(View view, ACTION action, Song song) {
                mView = view;
                mAction = action;
                mSong = song;
            }

            @Override
            public void onClick(View view) {
                switch (mAction) {
                    case PLAY:
                        play(getActivity(), mView, mSong, null, (mType == TYPE.PLAYLIST));
                        break;

                    case ADDTOFAVS:
                        addToFavs(mSong);
                        break;

                    case ADDTOPLAYLIST:
                        addToPlayList(mSong);
                        break;

                    case REMOVE:
                        if (mType == TYPE.FAVS) {
                            removeFromFavs(mSong);
                        } else {
                            removeFromPlayList(mSong);
                        }

                        break;
                }
            }
        }

        protected void addToPlayList(Song song) {
            Log.d("ITEM_CLICKED", "PLAYLIST: " + isUserLoggedIn());
            if (!isUserLoggedIn()) {
                showLoginDialog("playlist");
                return;
            }

            song.addToPlayList();
            song.save();
            notifyDataSetChanged();
        }

        protected void addToFavs(Song song) {
            Log.d("ITEM_CLICKED", "FAV: " + isUserLoggedIn());
            if (!isUserLoggedIn()) {
                showLoginDialog("favourites");
                return;
            }

            song.like();
            song.save();
            notifyDataSetChanged();
        }

        protected void removeFromPlayList(Song song) {
            song.removeFromPlayList();
            song.save();
            refresh(null);
        }

        protected void removeFromFavs(Song song) {
            song.unlike();
            song.save();
            refresh(null);
        }
    }

    private boolean isUserLoggedIn() {
        User user = Ndule.getUser();

        return ((user != null) && (user.isLoggedIn()));
    }

    private void showLoginDialog(String type) {
        String heading = getString(R.string.sf_title);
        String message = getString(R.string.sf_dg_login_message);
        message = String.format(message, type);
        String okayBtn = getString(R.string.btn_okay);
        String cancelBtn = getString(R.string.btn_cancel);

        DialogHelper.twoButtonDialog(getContext(), heading, message, okayBtn, cancelBtn,
            new LoginDialogClicked()).show();
    }

    private class LoginDialogClicked extends DialogHelper.Listener {

        @Override
        public void onPositive(MaterialDialog dialog) {
            mTabFragmentSwitcher.handleLoginRequest();
        }
    }

    private class SongComparator implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            if ((o1 instanceof Song) && (o2 instanceof Song)) {
                return ((Song) o1).getTitle().compareTo(((Song) o2).getTitle());
            }

            if ((o1 instanceof Artist) && (o2 instanceof Artist)) {
                return ((Artist) o1).getName().compareTo(((Artist) o2).getName());
            }

            return 0;
        }
    }
}