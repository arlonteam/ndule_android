package arlon.android.portfolio.ndule.utils;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import arlon.android.portfolio.ndule.interfaces.LinkListener;

/**
 * Created by Arlon Mukeba on 1/16/2018.
 */

public class StringUtils {

    private static final String EMAIL_PATTERN = Patterns.EMAIL_ADDRESS.pattern();
    //"\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    private static final String WEB_URL_PATTERN = Patterns.WEB_URL.pattern();
    private static final String CELL_NBR_PATTERN = Patterns.PHONE.pattern();

    public static SpannableStringBuilder setColourTag(String inputString, String link, int color) {
        return setColourTag(new SpannableStringBuilder(inputString), link, color);
    }

    public static SpannableStringBuilder setColourTag(String inputString, String[] links,
                                                      int color) {

        SpannableStringBuilder ssb = new SpannableStringBuilder(inputString);

        for (String link : links) {
            ssb = setColourTag(ssb, link, color);
        }

        return ssb;
    }

    public static SpannableStringBuilder setColourTag(SpannableStringBuilder ssb, String link,
                                                      int color) {

        int indexOfPattern = 0;

        while ((indexOfPattern < ssb.toString().length()) &&
                ((indexOfPattern = ssb.toString().indexOf(link, indexOfPattern)) != (-1))) {

            ssb.setSpan(new ClickableLink(link, color), indexOfPattern,
                    (indexOfPattern + (link.length())), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            indexOfPattern++;
            indexOfPattern += link.length();
        }

        return ssb;
    }

    public static SpannableStringBuilder setStyleTag(String inputString, String link, int style) {
        return setStyleTag(new SpannableStringBuilder(inputString), link, style);
    }

    public static SpannableStringBuilder setStyleTag(SpannableStringBuilder ssb, String link,
                                                     int style) {

        int indexOfPattern = 0;

        while ((indexOfPattern < ssb.toString().length()) &&
                ((indexOfPattern = ssb.toString().indexOf(link, indexOfPattern)) != (-1))) {

            ssb.setSpan(new StyleSpan(style), indexOfPattern, (indexOfPattern + (link.length())),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            indexOfPattern++;
            indexOfPattern += link.length();
        }

        return ssb;
    }

    private static Matcher getMatcher(String inputString, String pattern) {
        return Pattern.compile(pattern).matcher(inputString);
    }

    public static SpannableStringBuilder setPhoneTag(LinkListener listener, String inputString,
                                                     String link) {

        return setPhoneTag(listener, new SpannableStringBuilder(inputString), link);
    }

    public static SpannableStringBuilder setPhoneTag(LinkListener listener,
                                                     SpannableStringBuilder ssb, String link) {

        return setLinkTag(listener, ssb, link, CELL_NBR_PATTERN);
    }

    public static SpannableStringBuilder setURLTag(LinkListener listener, String inputString,
                                                   String link) {

        return setURLTag(listener, new SpannableStringBuilder(inputString), link);
    }

    public static SpannableStringBuilder setURLTag(LinkListener listener,
                                                   SpannableStringBuilder ssb, String link) {

        return setLinkTag(listener, ssb, link, WEB_URL_PATTERN);
    }

    public static SpannableStringBuilder setEmailTag(LinkListener listener, String inputString,
                                                     String link) {

        return setEmailTag(listener, new SpannableStringBuilder(inputString), link);
    }

    public static SpannableStringBuilder setEmailTag(LinkListener listener,
                                                     SpannableStringBuilder ssb, String link) {

        return setLinkTag(listener, ssb, link, EMAIL_PATTERN);
    }

    private static SpannableStringBuilder setLinkTag(LinkListener listener,
                                                     SpannableStringBuilder ssb, String link,
                                                     String pattern) {
        int indexOfPattern = -1;
        String detectedUrl;

        Matcher matcher = getMatcher(ssb.toString(), pattern/*URL_PATTERN*/);

        while (matcher.find()) {
            detectedUrl = matcher.group();

            if ((detectedUrl == null) || (detectedUrl.isEmpty())) continue;

            indexOfPattern = ssb.toString().indexOf(detectedUrl);

            ssb.setSpan(new ClickableLink(listener, detectedUrl), indexOfPattern,
                    (indexOfPattern + detectedUrl.length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return ssb;
    }

    public static boolean validateEmail(String emailAddress) {
        Matcher matcher = Pattern.compile(Patterns.EMAIL_ADDRESS.pattern()).matcher(emailAddress);
        return matcher.matches();
    }

    private static class ClickableLink extends ClickableSpan {

        private LinkListener mLinkListener = null;
        private String mLink;
        private int mColor = -1;

        public ClickableLink(String link, int color) {
            mLink = link;
            mColor = color;
        }

        public ClickableLink(LinkListener linkClickedListener, String link) {
            mLinkListener = linkClickedListener;
            mLink = link;
        }

        @Override
        public void onClick(View widget) {
            Log.d("DEBUGGINGLINK", "HERE");
            if (mLinkListener != null) {
                Log.d("DEBUGGINGLINK", "HERE2");
                mLinkListener.linkClicked(mLink);
            }
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);

            if (mColor == (-1)) {
                ds.setUnderlineText(true);
            } else {
                ds.setUnderlineText(false);
                ds.setColor(mColor);
            }
        }
    }
}