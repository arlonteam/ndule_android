package arlon.android.portfolio.ndule.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.PermissionHelper;
import arlon.android.portfolio.ndule.helpers.PhotoCropHelper;
import arlon.android.portfolio.ndule.helpers.PixHelper;
import arlon.android.portfolio.ndule.interfaces.FragmentSwitcher;
import arlon.android.portfolio.ndule.popups.PixOptionSelector;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class BaseFragment extends Fragment implements LoaderManager.LoaderCallbacks, Observer {

    public static final String TAG = "DEFAULT";
    public static final String FRAGMENT_PARAMS = "FRAGMENT_PARAMS";
    public static final String SCREEN_ID_PARAM = "SCREEN_ID_PARAM";
    public static final String SCREEN_TITLE_PARAM = "SONG_LIST_TYPE_PARAM";

    public static final int CAMERA_REQUEST_CODE = 1;
    public static final int PICTURE_ACCESS_REQUEST_CODE = 2;
    public static final int SCANNER_REQUEST_CODE = 3;
    public static final int SONG_ACCESS_REQUEST_CODE = 4;

    protected FragmentSwitcher mFragmentSwitcher = null;
    protected View mFragmentView = null;
    protected Hashtable<String, Object> mParams = new Hashtable<String, Object>();
    protected String mScreenTitle = null;
    protected int mId = -1;
    protected boolean mShouldReload = true;

    public BaseFragment() {
    }

    public static BaseFragment newInstance(Hashtable<String, Object> params) {
        return new BaseFragment();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_empty;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            try {
                mParams = (Hashtable<String, Object>) getArguments().getSerializable(FRAGMENT_PARAMS);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mFragmentView = inflater.inflate(getFragmentLayout(), container, false);

        setHasOptionsMenu(true);

        if (mParams != null) {
            if (mParams.containsKey(SCREEN_TITLE_PARAM)) {
                mScreenTitle = (String) mParams.get(SCREEN_TITLE_PARAM);
            }

            /*if (mParams.containsKey(SCREEN_ID_PARAM)) {
                mId = Integer.parseInt("" + mParams.get(SCREEN_ID_PARAM));
            }*/
        }

        return mFragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initFragment(savedInstanceState);
    }

    public void setFragmentSwitcher(FragmentSwitcher fragmentSwitcher) {
        mFragmentSwitcher = fragmentSwitcher;
    }

    @Override
    public void onAttach(Context context) {
//        CineKin.addLangObserver(this);
        super.onAttach(context);
    }

    public void showNotifMenuItem() {
        getActivity().invalidateOptionsMenu();
    }

    protected ArrayList<String> buildAudioPermissions() {
        return new ArrayList<String>() {
            {
                add(Manifest.permission.RECORD_AUDIO);
            }
        };
    }

    protected ArrayList<String> buildCameraPermissions() {
        return new ArrayList<String>() {
            {
                add(Manifest.permission.CAMERA);
                add(Manifest.permission.READ_EXTERNAL_STORAGE);
                add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        };
    }

    protected void requestCamPerm(String[] permissions, int requestCode) {
        requestPermissions(permissions, requestCode);
    }

    protected void requestPictureAccessPerm(String[] permissions) {
        requestPermissions(permissions, PICTURE_ACCESS_REQUEST_CODE);
    }

    protected void requestSongAccessPerm(String[] permissions) {
        requestPermissions(permissions, SONG_ACCESS_REQUEST_CODE);
    }

    protected void selectPixOption(View view) {
        ArrayList<String> options = new ArrayList<String>(Arrays.asList(getResources().
                getStringArray(R.array.pf_image_source_options)));

        new PixOptionSelector(getContext(), options, new PixOptionSelected())
            .showAsDropDown(view, 0, 0);
    }

    private class PixOptionSelected implements PixOptionSelector.PixChoiceListener {

        @Override
        public void optionSelected(String choice) {
            ArrayList<String> options = new ArrayList<String>(Arrays.asList(getResources().
                getStringArray(R.array.pf_image_source_options)));

            if (options.indexOf(choice) == 0) {
                startCamera();
            } else {
                startPictureSelection();
            }
        }
    }

    protected void startCamera() {
        Log.d("STARTINGCAMERA", "HERE");
        String[] requiredPermissions = requiredPermissions(buildCameraPermissions());
        Log.d("STARTINGCAMERA", "" + new ArrayList<String>(Arrays.asList(requiredPermissions)));

        if (requiredPermissions.length == 0) {
            takePicture();
        } else {
            requestCamPerm(requiredPermissions, CAMERA_REQUEST_CODE);
        }
    }

    protected void startPictureSelection() {
        String[] requiredPermissions = requiredPermissions(buildFileAccessPermissions());

        if (requiredPermissions.length == 0) {
            selectPicture();
        } else {
            requestPictureAccessPerm(requiredPermissions);
        }
    }

    protected void startSongSelection() {
        String[] requiredPermissions = requiredPermissions(buildFileAccessPermissions());

        if (requiredPermissions.length == 0) {
            handlePermResult(true);
        } else {
            requestSongAccessPerm(requiredPermissions);
        }
    }

    private ArrayList<String> buildFileAccessPermissions() {
        return new ArrayList<String>() {
            {
                add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        };
    }

    protected String[] requiredPermissions(ArrayList<String> permissions) {
        return PermissionHelper.checkPermissions(getContext(), permissions);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        Log.d("STARTINGCAMERA", "" + new ArrayList<String>(Arrays.asList(permissions)) + " ") /* +
                new ArrayList<String>(grantResults))*/;

        if (PermissionHelper.isPermGranted(grantResults)) {
            switch (requestCode) {
                case CAMERA_REQUEST_CODE:
                    takePicture();
                    break;

                case PICTURE_ACCESS_REQUEST_CODE:
                    selectPicture();
                    break;

                default:
                    handlePermResult(true);
            }
        } else {
            handlePermResult(false);
        }
    }

    private void takePicture() {
        Log.d("STARTINGCAMERA", "HERE3");
        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), CAMERA_REQUEST_CODE);
    }

    private void selectPicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICTURE_ACCESS_REQUEST_CODE);
    }

    protected void handlePermResult(boolean permGranted) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("STARTINGCAMERA", "FROM FRAGMENT: REQUEST: " + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                cropPicture(retrieveCameraPicture(data));
            } else if (requestCode == PICTURE_ACCESS_REQUEST_CODE) {
                cropPicture(retrieveSelectedPicture(data));
                Log.d("INTENTDATA", "" + data);
            }
        }
    }

    private void cropPicture(Bitmap bitmap) {
        Log.d("PERMISSIONREQUESTED", "TAKINGPICTURE3");
        String path = PixHelper.getRealPathFromURI(getContext(), bitmap);
        getPictureHelper().cropImage(bitmap, path);
    }

    protected PhotoCropHelper getPictureHelper() {
        return null;
    }

    private Bitmap retrieveCameraPicture(Intent data) {
        Bundle extras = data.getExtras();
        return (Bitmap) extras.get("data");
    }

    private Bitmap retrieveSelectedPicture(Intent data) {
        Bitmap bitmap = null;

        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),
                    data.getData());
        } catch (Exception e) {

        }

        return bitmap;
    }

    protected String getPopupMessage() {
        return null;
    }

    private boolean showPopup() {
        String popupMsg = getPopupMessage();

        if (popupMsg != null) {
//            DialogHelper.showMessage(getContext(), "Message", popupMsg);
            return true;
        }

        return false;
    }

    @Override
    public void onDetach() {
//        CineKin.removeLangObserver(this);
        super.onDetach();
    }

    protected int getMenuResId() {
        return R.menu.menu_home;
    }

    public void onPrepareOptionsMenu(Menu menu) {
        /*try {
            long count = Select.from(Notification.class).where(Condition.prop(
                    Notification.COLUMN_STATUS).eq(Notification.STATUS_UNREAD)).count();

            menu.findItem(R.id.action_notifications).setVisible(count > 0);
        } catch (Exception e) {

        }*/

        super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        /*if (item.getItemId() == R.id.action_notifications) {
            mTabFragmentSwitcher.switchFragment(NotificationFragment.TAG,
                    new Hashtable<String, Object>(), false);

            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        int menuResId = getMenuResId();

        if (menuResId == (-1)) {
            return;
        }

        menu.clear();
        menuInflater.inflate(menuResId, menu);

        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public int getFabIconResId() {
        return -1;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        if (mShouldReload) {
            getLoaderManager().initLoader(TAG.hashCode(), null, this).forceLoad();
        }
    }

    public void handleFabClick(View view) {

    }

    public void update(Observable observable, Object object) {
        Log.d("LANGUPDATE", "OBJECT: " + object.toString());
        if (/*(observable.equals(CineKin.mLangChanger)) && */(object instanceof String)) {
            populateViews();
        }
    }

    protected void populateViews() {
        getActivity().invalidateOptionsMenu();
    }

    protected DataFetcher getLoader() {
        return new DataFetcher(getContext());
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return getLoader();
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {

    }

    protected static class DataFetcher extends AsyncTaskLoader<Object> {

        public DataFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            return null;
        }

        @Override
        public void deliverResult(Object data) {
            super.deliverResult(data);
        }
    }
}