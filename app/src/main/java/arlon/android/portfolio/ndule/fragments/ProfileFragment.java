package arlon.android.portfolio.ndule.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.PhotoCropHelper;
import arlon.android.portfolio.ndule.helpers.PixHelper;
import arlon.android.portfolio.ndule.interfaces.ProfileFragmentSwitcher;
import arlon.android.portfolio.ndule.objects.User;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Arlon Mukeba on 1/20/2018.
 */

public class ProfileFragment extends BaseFragment implements ProfileFragmentSwitcher {

    public static final String TAG = "PROFILE_FRAGMENT";
    private static final String TAKE_PICTURE_OPTION = "Take Pix";
    private static final String SELECT_FROM_GALLERY_OPTION = "Select from Gallery";

    private User mUser = Ndule.getUser();
    private String mProfilePixPath = "";
    private Bitmap mProfilePixBitmap = null;

    private MaterialDialog mDialog = null;
//    private ImageView mIvProfilePix = null;
    private CircleImageView mCivProfilePix = null;
    private TextView mTvPixChange = null;
    private boolean mProfilePixChanged = false;

    public ProfileFragment() {

    }

    public static ProfileFragment newInstance(Hashtable<String, Object> params) {
        ProfileFragment profileFragment = new ProfileFragment();
        return profileFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("PERMISSIONREQUESTED", "ONCREATECALLED");
        super.onCreate(savedInstanceState);

        viewProfile();
    }

    @Override
    public void onResume() {
        super.onResume();

        populateViews();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_profile;
    }

    public void switchFragment(String fragmentTag, Hashtable<String, Object> parameters) {

        ProfileBaseFragment fragment = null;

        switch (fragmentTag) {
            case ProfileViewFragment.TAG:
                fragment = ProfileViewFragment.newInstance(parameters);
                break;

            case ProfileEditFragment.TAG:
                fragment = ProfileEditFragment.newInstance(parameters);
                break;
        }

        if (fragment == null) return;

        fragment.setProfileFragmentSwitcher(this);

        FragmentManager fragmentManager = getChildFragmentManager();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

//        transaction.add(R.id.content_frame, fragment);
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(fragment.getTag()/*null*/);
        transaction.commit();
    }

    public boolean handleBack() {
        if (isEditing()) {
            moveBack();
            return true;
        }

        return false;
    }

    public void profileEdited(boolean changed, boolean saved) {
        if (saved) {
            if (mProfilePixChanged) {
                mUser = Ndule.getUser();
                mUser.setPictureUrl(mProfilePixPath);
                mUser.save();
                Ndule.setUser(mUser);
            }
        }

        moveBack();

        /*if (changed) {
            populateViews();
        } else {
            populateViews();
        }

        populateViews();*/
    }

    protected int getMenuResId() {
        return R.menu.menu_profile_fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);

        menu.findItem(R.id.action_coupon).setVisible(!isEditing());
        menu.findItem(R.id.action_edit).setVisible(!isEditing());
        menu.findItem(R.id.action_reset).setVisible(isEditing());
        menu.findItem(R.id.action_reset).setTitle(R.string.action_reset);
        mTvPixChange.setVisibility(isEditing() ? View.VISIBLE : View.GONE);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            editProfile();
            return true;
        } else if (item.getItemId() == R.id.action_coupon) {
            scanCoupon();
            return true;
        } else if (item.getItemId() == R.id.action_reset) {
            moveBack();
            return true;
        }

        return false;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        populateViews();
    }

    private void setupViews() {
//        mCivProfilePix = (ImageView) getView().findViewById(R.id.ivProfilePix);
        mCivProfilePix = (CircleImageView) getView().findViewById(R.id.civProfilePix);
        mCivProfilePix.setOnClickListener(new ProfilePixClicked());

        mTvPixChange = (TextView) getView().findViewById(R.id.tvPixChange);
        mTvPixChange.setOnClickListener(new ProfilePixClicked());
    }

    protected void populateViews() {
        mUser = Ndule.getUser();

        if (mUser == null) return;

        if (!mUser.getPixUrl().isEmpty()) {
            mCivProfilePix.setImageBitmap(PixHelper.getBitmapFromMemory(mUser.getPixUrl()));
        } else {
            mCivProfilePix.setImageResource(R.drawable.ic_unknown_user);
            /*final int width = 500;
            final int height = 500;

            Glide.with(getContext()).load(mUser.getPixUrl()).asBitmap().override(width, height)
                .placeholder(R.drawable.ic_unknown_user).error(R.drawable.ic_unknown_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                        .into(new SimpleTarget<Bitmap>(width, height) {
                              @Override
                              public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                  mCivProfilePix.setImageBitmap(bitmap);
                              }
                          }
                    );*/
        }

//        super.populateViews();
    }

    public int getFabIconResId() {
        return -1;
    }

    private boolean isEditing() {
        return (getChildFragmentManager().getBackStackEntryCount() > 1);
    }

    private class ProfilePixClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (isEditing()) {
                selectPixOption(view);
            }
        }
    }

    private void scanCoupon() {
        mFragmentSwitcher.switchFragment(CouponScannerFragment.TAG, new Hashtable<String, Object>(), false);
    }

    private void moveBack() {
        getChildFragmentManager().popBackStack();
        getActivity().invalidateOptionsMenu();
        populateViews();
    }

    private void viewProfile() {
        switchFragment(ProfileViewFragment.TAG, new Hashtable<String, Object>());
    }

    private void editProfile() {
        switchFragment(ProfileEditFragment.TAG, new Hashtable<String, Object>());
    }

    protected PhotoCropHelper getPictureHelper() {
        return new ProfilePictureHelper(getContext());
    }

    private class ProfilePictureHelper extends PhotoCropHelper {

        public ProfilePictureHelper(Context context) {
            super(context);
        }

        @Override
        public void handleFinalImage(Bitmap bitmap, String path) {
//            setChanged();
//            notifyObservers(bitmap);
            Log.d("PERMISSIONREQUESTED", "PATH: " + path);
            setProfilePicture(path);
        }
    }

    private void setProfilePicture(String path) {
        mProfilePixPath = path;
        mProfilePixBitmap = PixHelper.getBitmapFromMemory(mProfilePixPath);
        mCivProfilePix.setImageBitmap(mProfilePixBitmap);
        mProfilePixChanged = true;
    }
}