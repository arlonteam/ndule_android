package arlon.android.portfolio.ndule.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.DialogHelper;
import arlon.android.portfolio.ndule.objects.User;

/**
 * Created by Arlon Mukeba on 1/19/2018.
 */

public class ContactUsFragment extends BaseFragment {

    public static final String TAG = "CONTACT_US_FRAGMENT";

    private MaterialDialog mDialog = null;
    private EditText mEdtTitle = null;
    private EditText mEdtEmail = null;
    private EditText mEdtMsg = null;
    private TextView mTvSubmit = null;

    public ContactUsFragment() {

    }

    public static ContactUsFragment newInstance(Hashtable<String, Object> params) {
        ContactUsFragment contactFragment = new ContactUsFragment();
        return contactFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_contact_us;
    }

    /*protected int getMenuResId() {
        return R.menu.menu_feedback_fragment;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_submit) {
            confirmSubmitMsg();
            return true;
        }

        return false;
    }*/

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews(savedInstanceState);
        populateViews();
    }

    private void setupViews(@Nullable Bundle savedInstanceState) {
        mEdtTitle = (EditText) getView().findViewById(R.id.edtTitle);
        mEdtEmail = (EditText) getView().findViewById(R.id.edtEmail);
        mEdtMsg = (EditText) getView().findViewById(R.id.edtMsg);
        mEdtMsg.setMovementMethod(null);
        mTvSubmit = (TextView) getView().findViewById(R.id.tvSubmit);
        mTvSubmit.setOnClickListener(new SubmitBtnClicked());

        User user = Ndule.getUser();

        if ((user != null) && (user.isLoggedIn())) {
            mEdtEmail.setText(user.getEmail());
        }
    }

    public int getFabIconResId() {
        return -1;
    }

    private class SubmitBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            confirmSubmitMsg();
        }
    }

    private boolean validateInput() {
        String errorMsg = "";
        boolean isValid = true;

        if (mEdtEmail.getText().toString().isEmpty()) {
            errorMsg = getResources().getString(R.string.cuf_empty_email_error);
            mEdtEmail.setError(errorMsg);
            isValid = false;
        }

        if (mEdtTitle.getText().toString().isEmpty()) {
            errorMsg = getResources().getString(R.string.cuf_empty_title_error);
            mEdtTitle.setError(errorMsg);
            isValid = false;
        }

        if (mEdtMsg.getText().toString().isEmpty()) {
            errorMsg = getResources().getString(R.string.cuf_empty_message_error);
            mEdtMsg.setError(errorMsg);
            isValid = false;
        }

        return isValid;
    }

    private void confirmSubmitMsg() {
        if (!validateInput()) {
            return;
        }

        DialogHelper.twoButtonDialog(getContext(), R.string.cuf_send_dialog_title,
            R.string.cuf_send_dialog_content, R.string.cuf_send_dialog_btn_proceed,
                R.string.cuf_send_dialog_btn_cancel, new MsgSendListener()).show();
    }

    private class MsgSendListener extends DialogHelper.Listener {

        @Deprecated
        public void onPositive(MaterialDialog dialog) {
            submitMessage();
        }
    }

    private void submitMessage() {
        User user = Ndule.getUser();

        if (user == null) {
            user = new User();
            user.setFirstName("Anonymous");
            user.setEmail(mEdtEmail.getText().toString());
        }

        String message = mEdtMsg.getText().toString();

        Log.d(TAG, "HERE...");

        mFragmentSwitcher.navigateBack(0);

        /*mDialog = DialogHelper.loadingDialog(getContext(), getString(R.string.af_dialog_message))
                .show();*/

//        NotificationHelper.sendMessage(getContext(), user, message, new MsgUploadListener());
    }

    /*private class MsgUploadListener implements NetworkListener  {

        @Override
        public void success(Object data) {
            mDialog.dismiss();

            Log.d(ContactUsFragment.TAG, "RETURNED_RESULT" + data.toString());

            DialogHelper.showMessage(getContext(), R.string.cuf_send_dialog_title,
                R.string.cuf_msg_dialog_content, new MsgDialogListener());
        }

        @Override
        public void error(String errorMessage) {
            mDialog.dismiss();
        }
    }

    private class MsgDialogListener extends DialogHelper.Listener {

        @Deprecated
        public void onPositive(MaterialDialog dialog) {
            mTabFragmentSwitcher.navigateBack(0);
        }
    }*/
}