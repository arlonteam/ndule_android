package arlon.android.portfolio.ndule.objects;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

import org.json.JSONObject;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.helpers.JsonHelper;
import arlon.android.portfolio.ndule.helpers.PixHelper;

/**
 * Created by Arlon Mukeba on 1/20/2018.
 */

public class Pix extends SugarRecord implements Parcelable {

    public final static String COLUMN_IMAGE_URL = "url";

    @Column(name = COLUMN_IMAGE_URL)
    private String mImageUrl;

    public Pix() {

    }

    public Pix(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public Pix(Parcel in) {
        setId(in.readLong());
        setImageUrl(in.readString());
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public static final Creator<Pix> CREATOR = new Creator<Pix>() {

        @Override
        public Pix createFromParcel(Parcel source) {
            return new Pix(source);
        }

        @Override
        public Pix[] newArray(int size) {
            return new Pix[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getImageUrl());
    }

    public static Pix fromRegisterApi(JSONObject jsonObject) {
        return fromRegisterApi(jsonObject, false);
    }

    public static Pix fromRegisterApi(JSONObject jsonObject, boolean isProfile) {
        Pix picture = new Pix();

        String imageUrl = (isProfile) ? PixHelper.PROFILES_IMAGE_URL : PixHelper.ADS_IMAGE_URL;
        imageUrl += JsonHelper.getString(jsonObject, COLUMN_IMAGE_URL);

        Log.d("IMAGE_URL_TEST", imageUrl);

        picture.setImageUrl(imageUrl);

        return picture;
    }

    public String toString () {
        return new StringBuffer("PICTURE: ").append("\n\n")
                .append("Image URL: ").append(mImageUrl).toString();
    }
}