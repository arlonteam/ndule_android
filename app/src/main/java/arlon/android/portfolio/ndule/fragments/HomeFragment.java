package arlon.android.portfolio.ndule.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.adapters.PlaylistBlockAdapter;
import arlon.android.portfolio.ndule.adapters.SongSliderAdapter;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Song;
import arlon.android.portfolio.ndule.objects.User;
import arlon.android.portfolio.ndule.objects.ZoomOutPageTransformer;
import arlon.android.portfolio.ndule.screens.SongPlayerScreen;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class HomeFragment extends BaseFragment {

    private FragmentManager mFragmentManager = null;
    private Context mContext = null;
    private ViewPager mViewPager = null;
    private CirclePageIndicator mCirclePageIndicator = null;
    private TextView mTvArtistName = null;
    private TextView mTvSongTitle = null;
//    private StackView mSvPlayLists = null;
    private LinearLayout mLlShowAll = null;
    private ImageView mIvFeaturedArtists = null;
    private SliderAdapter mSongSliderAdapter = null;
    private PagerChangeListener mPagerChangeListener = new PagerChangeListener();
    private RecyclerView mRvPlayList = null;
    private TextView mTvEmptyPlayList = null;
    private RelativeLayout mRlClickable = null;
    private int mSliderIndex = 0;
    private boolean mIsSliderActive = false;
    private boolean mFirstTime = true;

    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mIsSliderActive) {
                automateViewPager();
                scheduleAnimation();
            }
        }
    };

    public HomeFragment() {
//        this.mFragmentManager = fragmentManager;
    }

    public static HomeFragment newInstance(Hashtable<String, Object> params) {
        HomeFragment fragment = new HomeFragment();
        /*Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_PARAMS, params);
        fragment.setArguments(args);*/
        return fragment;
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_home;
    }

    protected int getMenuResId() {
        return R.menu.menu_home_fragment;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!mFirstTime) {
            loadSongs((ArrayList<Song>) Select.from(Song.class).limit("5").list());
            setListFrontImage();
            loadPlayList();
        }
    }

    @Override
    public void onDetach() {
        mViewPager.removeOnPageChangeListener(mPagerChangeListener);
        mIsSliderActive = false;
        super.onDetach();
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        mContext = getContext();
        setupViews();
//        populateViews();
        super.initFragment(savedInstanceState);
    }

    private void setupViews() {
        mViewPager = (ViewPager) getView().findViewById(R.id.vpImageSlider);
        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mViewPager.addOnPageChangeListener(mPagerChangeListener);
        mCirclePageIndicator = (CirclePageIndicator) getView().findViewById(R.id.cpiPageIndicator);
        mTvArtistName = (TextView) getView().findViewById(R.id.tvArtistName);
        mTvSongTitle = (TextView) getView().findViewById(R.id.tvSongTitle);
        mLlShowAll = (LinearLayout) getView().findViewById(R.id.llSelectAll);
        mLlShowAll.setOnClickListener(new ShowListClicked(0));
        mIvFeaturedArtists = (ImageView) getView().findViewById(R.id.ivFeaturedArtists);
        mIvFeaturedArtists.setOnClickListener(new ShowListClicked(1));
        mTvEmptyPlayList = (TextView) getView().findViewById(R.id.tvEmptyPlaylist);
        mRlClickable = (RelativeLayout) getView().findViewById(R.id.rvClickable);
        mRlClickable.setOnClickListener(new PlayListBlockClicked());
        mRvPlayList = (RecyclerView) getView().findViewById(R.id.rvPlaylist);
        mRvPlayList.setItemAnimator(new DefaultItemAnimator());
//        mSvPlayLists = (StackView) getView().findViewById(R.id.svPlayLists);
    }

    protected void populateViews() {
        /*((TextView) getView().findViewById(R.id.tvActorLbl)).setText(getString(
                R.string.hf_trending_actor));

        ((TextView) getView().findViewById(R.id.tvListLbl)).setText(getString(
                R.string.hf_show_all));

        ((TextView) getView().findViewById(R.id.tvMore)).setText(getString(
                R.string.hf_more));

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        mIvFeaturedArtists.setColorFilter(new ColorMatrixColorFilter(matrix));*/
    }

    private void scheduleAnimation() {
        mIsSliderActive = true;
        new Handler().postDelayed(mRunnable, 2500);
    }

    private void automateViewPager() {
        mSliderIndex = (++mSliderIndex) % mSongSliderAdapter.getCount();
        mViewPager.setCurrentItem(mSliderIndex);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        loadSongs((ArrayList<Song>) data);

//        mViewPager.setClipToPadding(false);
//        mViewPager.setPadding(2, 0, 2, 0);
//        mViewPager.setPageMargin(5);

        /*pager.setClipToPadding(false);
        pager.setPageMargin(24);
        pager.setPadding(48, 8, 48, 8);
        pager.setOffscreenPageLimit(3);*/

        setListFrontImage();
        loadPlayList();
    }

    private void loadSongs(ArrayList<Song> songs) {
        mSongSliderAdapter = new SliderAdapter();
        mSongSliderAdapter.setSongs(songs);
        mViewPager.setAdapter(mSongSliderAdapter);

        mCirclePageIndicator.setViewPager(mViewPager);
        mCirclePageIndicator.invalidate();

        setSongDetails(songs.get(0));

        if (!mIsSliderActive) {
            scheduleAnimation();
        }

        mFirstTime = true;
    }

    private void setSongDetails(Song song) {
        mTvSongTitle.setText(song.getTitle());
        mTvArtistName.setText(song.getArtist().getName());
    }

    private void setListFrontImage() {
        ArrayList<Artist> featuredArtists = new ArrayList<Artist>(Select.from(Artist.class).list());

        int randomIdx = new Random().nextInt(featuredArtists.size());
        Artist artist = featuredArtists.get(randomIdx);

        String artistImage = Ndule.ARTIST_IMAGE_URL + artist.getImage() + ".jpg";

        Glide.with(mContext).load(artistImage).asBitmap().override(500, 500)
            /*.placeholder(R.drawable.ic_unknown_user)*/.error(R.drawable.ic_treble_key)
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                .into(new SimpleTarget<Bitmap>(500, 500) {
                          @Override
                          public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                              mIvFeaturedArtists.setImageBitmap(bitmap);
                          }
                      }
                );
    }

    private void loadPlayList() {
        User user = Ndule.getUser();

        if ((user == null) || (!user.isLoggedIn())) {
            mRvPlayList.setVisibility(View.GONE);
            mTvEmptyPlayList.setVisibility(View.VISIBLE);
            mTvEmptyPlayList.setText(getString(R.string.hf_login_msg));
            return;
        }

        ArrayList<Object> songs = new ArrayList<Object>(Select.from(Song.class).where(Condition.prop(
            Song.COLUMN_ADDED_TO_PLAYLIST).eq("YES")).limit("8").list());

        if (songs.isEmpty()) {
            mRvPlayList.setVisibility(View.GONE);
            mTvEmptyPlayList.setVisibility(View.VISIBLE);
            mTvEmptyPlayList.setText(getString(R.string.hf_playlist_msg));
            return;
        }

        mRvPlayList.setVisibility(View.VISIBLE);
        mTvEmptyPlayList.setVisibility(View.GONE);

        int span = (int) Math.sqrt(songs.size());
        span += ((songs.size() - (int) Math.pow(span, 2)) > 0) ? 1 : 0;

        int measuredWidth = mRvPlayList.getMeasuredWidth() / span;
        int measuredHeight = mRvPlayList.getMeasuredHeight() / span;

        mRvPlayList.setHasFixedSize(true);
        mRvPlayList.setLayoutManager(new GridLayoutManager(getContext(), span));
        mRvPlayList.addItemDecoration(new SpacesItemDecoration(2));

        mRvPlayList.setAdapter(new PlaylistBlockAdapter(getContext(), songs, measuredWidth,
            measuredHeight));
    }

    private void setStackView() {

    }

    protected DataFetcher getLoader() {
        return new HomeDataFetcher(getContext());
    }

    private static class HomeDataFetcher extends DataFetcher {

        public HomeDataFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            return Select.from(Song.class).limit("5").list();
        }
    }

    private class PagerChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            setSongDetails(mSongSliderAdapter.getSongs(position));
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    private class SliderAdapter extends SongSliderAdapter {

        private LayoutInflater mLayoutInflater;
        private ArrayList<Song> mSongs;

        public SliderAdapter() {
            super(mContext);
        }

        protected void setSongClickListener(RelativeLayout songLayout, Object song) {
            songLayout.setOnClickListener(new SongClicked((Song) song));
        }
    }

    private class SongClicked implements View.OnClickListener {

        private Song mSong = null;

        public SongClicked(Song song) {
            mSong = song;
        }

        @Override
        public void onClick(View view) {
            moveToSongScreen(mSong, false);
        }
    }

    private class PlayListBlockClicked implements View.OnClickListener {

        public void onClick(View view) {
            moveToSongScreen(null, true);
        }
    }

    private void moveToSongScreen(Song song, boolean isPlayList) {
        Intent intent = new Intent(getActivity(), SongPlayerScreen.class);
        intent.putExtra(SongPlayerScreen.PLAYLIST_PARAM, isPlayList);
        intent.putExtra(SongPlayerScreen.SONG_PARAM, song);
        startActivity(intent);
    }

    private class ShowListClicked implements View.OnClickListener {

        private int mTabIndex = 0;

        public ShowListClicked(int tabIndex) {
            mTabIndex = tabIndex;
        }

        @Override
        public void onClick(View view) {
            Hashtable<String, Object> params = new Hashtable<String, Object>() {
                {
                    put(SongTabFragment.TAB_PARAM, "" + mTabIndex);
                }
            };

            mFragmentSwitcher.switchFragment(SongTabFragment.TAG, params, false);
        }
    }

    /*public class StackAdapter extends ArrayAdapter<Artist> {

        private List<Artist> items;
        private Context context;

        public StackAdapter(Context context, int textViewResourceId, List<Artist> objects) {
            super(context, textViewResourceId, objects);
            this.items = objects;
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            if (itemView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                itemView = layoutInflater.inflate(R.layout.stack_item, null);
            }

            Artist stackItem = items.get(position);

            if (stackItem != null) {
                // TextView defined in the stack_item.xml
                TextView textView = (TextView) itemView.findViewById(R.id.textView);
                // ImageView defined in the stack_item.xml
                ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

                if (textView != null) {
                    textView.setText(stackItem.getItemText());

                    // "image1", "image2",..
                    String imageName= stackItem.getImageName();

                    int resId= this.getDrawableResIdByName(imageName);

                    imageView.setImageResource(resId);
                    imageView.setBackgroundColor(Color.rgb(211,204,188));
                }

            }

            return itemView;
        }

        // Find Image ID corresponding to the name of the image (in the drawable folder).
        public int getDrawableResIdByName(String resName)  {
            String pkgName = context.getPackageName();
            // Return 0 if not found.
            int resID = context.getResources().getIdentifier(resName, "drawable", pkgName);
            Log.i("MyLog", "Res Name: " + resName + "==> Res ID = " + resID);
            return resID;
        }
    }*/

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {

            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;

            // Add top margin only for the first item to avoid double space between items
            outRect.top = (parent.getChildLayoutPosition(view) == 0) ? space : 0;
        }
    }
}