package arlon.android.portfolio.ndule.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.fragments.BaseFragment;
import arlon.android.portfolio.ndule.fragments.HomeFragment;
import arlon.android.portfolio.ndule.fragments.LoginFragment;
import arlon.android.portfolio.ndule.fragments.LostPwdFragment;
import arlon.android.portfolio.ndule.fragments.SignUpFragment;
import arlon.android.portfolio.ndule.interfaces.FragmentSwitcher;

/**
 * Created by Arlon Mukeba on 1/15/2018.
 */

public class LoginScreen extends AppCompatActivity implements FragmentSwitcher {

    public static final String SKIP_OPTION = "SKIP_OPTION";
    public static final String SCREEN_PARAM = "SCREEN_PARAM";

    private TextView mTvSkip = null;
    private boolean mSkipOption = false;
    private String mScreenParam = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login_screen);

        setToolBar();
        setupViews();
        getParameters();

        goToLogin();
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbToolbar);
        setSupportActionBar(toolbar);
        setTitle("");
    }

    private void setupViews() {
        mTvSkip = (TextView) findViewById(R.id.tvSkip);
        mTvSkip.setOnClickListener(new SkipBtnClicked());
    }

    private void getParameters() {
        mSkipOption = getIntent().getBooleanExtra(SKIP_OPTION, false);
        mTvSkip.setVisibility(mSkipOption ? View.VISIBLE : View.GONE);
    }

    private void goToLogin() {
        switchFragment(LoginFragment.TAG, new Hashtable<String, Object>(), true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        /*FragmentManager fm = getSupportFragmentManager();

        for(int entry = 0; entry < fm.getBackStackEntryCount(); entry++){
            Log.i("FRAGMENT_STACK", "" + fm.getBackStackEntryAt(entry).getName());
        }*/

        Log.d("FRAGMENT_STACK", "" + getSupportFragmentManager().getBackStackEntryCount());

        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            navigateBack(Activity.RESULT_CANCELED);
            return;
        }

        super.onBackPressed();
    }

    private void moveBack(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            Log.d("SAVED_USER", resultCode + " : " + mSkipOption);
        }

        if (!mSkipOption) {
            setResult(resultCode, null);
            finish();
        } else {
            moveToHomeScreen();
        }
    }

    @Override
    public void switchFragment(String fragmentTag, Hashtable<String, Object> parameters,
                               boolean shouldAdd) {

        BaseFragment fragment = null;

        switch (fragmentTag) {
            case LoginFragment.TAG:
                fragment = LoginFragment.newInstance(parameters);
//                parameters.put(BaseFragment.USER_PARAM, user);
                fragment.setFragmentSwitcher(this);
                break;

            case SignUpFragment.TAG:
                fragment = SignUpFragment.newInstance(parameters);
//                parameters.put(BaseFragment.USER_PARAM, user);
                fragment.setFragmentSwitcher(this);
                break;

            case LostPwdFragment.TAG:
                fragment = LostPwdFragment.newInstance(parameters);
//                parameters.put(BaseFragment.USER_PARAM, user);
                fragment.setFragmentSwitcher(this);
                break;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

        transaction.add(R.id.content_frame, fragment);
//        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(fragment.getTag()/*null*/);
        transaction.commit();
    }

    public void moveToHomeScreen() {
        Intent intent = new Intent(LoginScreen.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void navigateBack(int code) {
        moveBack(code);
    }

    @Override
    public void toggleLoginStatus() {

    }

    @Override
    public void changeProfileDetails() {

    }

    @Override
    public void toggleFabIcon(boolean visible) {

    }

    @Override
    public void login() {

    }

    @Override
    public void showNotification(boolean visible) {

    }

    private class SkipBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            moveToHomeScreen();
        }
    }
}