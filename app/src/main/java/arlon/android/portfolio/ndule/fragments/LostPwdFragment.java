package arlon.android.portfolio.ndule.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.utils.StringUtils;

/**
 * Created by Arlon Mukeba on 1/17/2018.
 */

public class LostPwdFragment extends BaseFragment {

    public static final String TAG = "LostPwdFragment";

    private EditText mEdtEmail = null;
    private TextView mTvSubmit = null;

    public static LostPwdFragment newInstance(Hashtable<String, Object> params) {
        return new LostPwdFragment();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_lost_pwd;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        populateViews();
    }

    public void setupViews() {
        mEdtEmail = (EditText) getView().findViewById(R.id.edtEmail);
        mTvSubmit = (TextView) getView().findViewById(R.id.tvSubmit);
        mTvSubmit.setOnClickListener(new SubmitBtnClicked());
    }

    public void populateViews() {

    }

    private class SubmitBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (!validateLogin()) return;

            mFragmentSwitcher.navigateBack(Activity.RESULT_CANCELED);
        }
    }

    private boolean validateLogin() {
        boolean valid = true;

        if (mEdtEmail.getText().toString().isEmpty()) {
            mEdtEmail.setError(getString(R.string.ls_empty_username_error));
            valid = false;
        } else {
            if (!StringUtils.validateEmail(mEdtEmail.getText().toString())) {
                mEdtEmail.setError(getString(R.string.ls_invalid_email_error));
                valid = false;
            }
        }

        return valid;
    }
}