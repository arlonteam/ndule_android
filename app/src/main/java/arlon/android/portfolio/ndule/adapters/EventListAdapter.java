package arlon.android.portfolio.ndule.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.DialogHelper;
import arlon.android.portfolio.ndule.objects.Event;

/**
 * Created by Arlon Mukeba on 3/3/2018.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventItemViewHolder> {

    protected Context mContext = null;
    private ArrayList<Event> mEvents = new ArrayList<Event>();

    public EventListAdapter(Context context) {
        mContext = context;
    }

    public void setEventItems(ArrayList<Event> events) {
        mEvents = events;
    }

    public EventItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView itemListLayout = (CardView) LayoutInflater.from(parent.getContext())
            .inflate(R.layout.layout_event, parent, false);

        return new EventItemViewHolder(itemListLayout);
    }

    public void onBindViewHolder(final EventItemViewHolder eventItemViewHolder, int position) {
        Event event = mEvents.get(position);

        String eventThumbnail = Ndule.EVENT_URL + event.getThumbnail() + ".jpg";

        Glide.with(mContext).load(eventThumbnail).asBitmap().override(1000, 1000)
            .error(R.drawable.ic_treble_key)
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                    .into(new SimpleTarget<Bitmap>(1000, 1000) {
                              @Override
                              public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                  eventItemViewHolder.mIvEventImage.setImageBitmap(bitmap);
                              }
                          }
                    );

        eventItemViewHolder.mView.setOnClickListener(new NotificationClicked(event));
    }

    public int getItemCount() {
        return ((mEvents == null) || (mEvents.isEmpty())) ? 0 : mEvents.size();
    }

    private class NotificationClicked implements View.OnClickListener {

        Event mFeedItem = null;

        public NotificationClicked(Event feedItem) {
            mFeedItem = feedItem;
        }

        @Override
        public void onClick(View v) {
            DialogHelper.showMessage(mContext, "Feed Item",
                    "Clicking this feed item will open it on a larger screen", null);
        }
    }

    public class EventItemViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public ImageView mIvEventImage;
        public TextView mTvShareAction;
        public TextView mTvInfoAction;

        public EventItemViewHolder(View view) {
            super(view);

            mView = view;
            mIvEventImage = (ImageView) view.findViewById(R.id.ivEventImage);
            mTvShareAction = (TextView) view.findViewById(R.id.tvShare);
            mTvInfoAction = (TextView) view.findViewById(R.id.tvInfo);
        }
    }
}