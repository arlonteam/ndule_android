package arlon.android.portfolio.ndule.objects;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

import java.util.Locale;
import java.util.Observable;

/**
 * Created by Arlon Mukeba on 1/15/2018.
 */

public class LangChanger  extends Observable {

    public void changeLanguage(Context context, String langRef) {
        setLanguage(context, langRef);
        saveUserLangPref(langRef);
        setChanged();
        notifyObservers(langRef);
    }

    private void setLanguage(Context context, String langToLoad) {
        Locale locale = new Locale(langToLoad);
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.locale = locale;

        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }

    private void saveUserLangPref(String langRef) {
        /*GroupeAvenir.getMe().setLangRef(langRef);
        GroupeAvenir.getMe().save();*/
    }
}