package arlon.android.portfolio.ndule.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import java.util.Hashtable;
import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.DialogHelper;
import arlon.android.portfolio.ndule.objects.User;
import arlon.android.portfolio.ndule.utils.StringUtils;

/**
 * Created by Arlon Mukeba on 1/22/2018.
 */

public class ProfileEditFragment extends ProfileBaseFragment {

    public static final String TAG = "ProfileEditFragment";

    private EditText mEdtFirstName = null;
    private EditText mEdtSurname = null;
    private EditText mEdtEmail = null;
    private EditText mEdtCellNbr = null;
    private EditText mEdtPwd = null;
    private EditText mEdtConfirmPwd = null;
    private TextView mTvSave = null;

    private User mUser = Ndule.getUser();

    public static ProfileEditFragment newInstance(Hashtable<String, Object> params) {
        return new ProfileEditFragment();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_profile_edit;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        populateViews();
    }

    protected int getMenuResId() {
        return -1;
    }

    public void setupViews() {
        mEdtFirstName = (EditText) getView().findViewById(R.id.edtFirstName);
        mEdtSurname = (EditText) getView().findViewById(R.id.edtSurname);
        mEdtEmail = (EditText) getView().findViewById(R.id.edtEmail);
        mEdtCellNbr = (EditText) getView().findViewById(R.id.edtCellNbr);
        mEdtPwd = (EditText) getView().findViewById(R.id.edtPwd);
        mEdtConfirmPwd = (EditText) getView().findViewById(R.id.edtConfirmPwd);

        mTvSave = (TextView) getView().findViewById(R.id.tvSave);
        mTvSave.setOnClickListener(new SaveBtnClicked());
    }

    public void populateViews() {
        mEdtFirstName.setText(mUser.getFirstName());
        mEdtSurname.setText(mUser.getSurname());
        mEdtEmail.setText(mUser.getEmail());
        mEdtCellNbr.setText(mUser.getCellNbr());
        mEdtPwd.setText(mUser.getPassword());
        mEdtConfirmPwd.setText(mUser.getPassword());
    }

    private class SaveBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            saveProfileChanges();
        }
    }

    private void saveProfileChanges() {
        if (validateProfileChanges()) {
            confirmProfileChanges();
        }
    }

    private boolean validateProfileChanges() {
        String errorMsg = "";
        boolean isValid = true;

        if (mEdtFirstName.getText().toString().isEmpty()) {
            errorMsg = getResources().getString(R.string.pf_empty_first_name_error);
            mEdtFirstName.setError(errorMsg);
            isValid = false;
        }

        if (mEdtSurname.getText().toString().isEmpty()) {
            errorMsg = getResources().getString(R.string.pf_empty_surname_error);
            mEdtSurname.setError(errorMsg);
            isValid = false;
        }

        if (mEdtEmail.getText().toString().isEmpty()) {
            mEdtEmail.setError(getString(R.string.ls_empty_email_error));
            isValid = false;
        } else {
            if (!StringUtils.validateEmail(mEdtEmail.getText().toString())) {
                mEdtEmail.setError(getString(R.string.ls_invalid_email_error));
                isValid = false;
            }
        }

        if (mEdtPwd.getText().toString().isEmpty()) {
            errorMsg = getResources().getString(R.string.pf_empty_password_error);
            mEdtPwd.setError(errorMsg);
            isValid = false;
        }

        if ((mEdtConfirmPwd.getText().toString().isEmpty()) || (!(mEdtConfirmPwd.getText().toString()
                .equals(mEdtPwd.getText().toString())))) {

            errorMsg = getResources().getString(R.string.pf_password_not_matching_error);
            mEdtConfirmPwd.setError(errorMsg);
            isValid = false;
        }

        return isValid;
    }

    private void confirmProfileChanges() {
        DialogHelper.twoButtonDialog(getContext(), R.string.pf_save_dialog_title,
            R.string.pf_save_dialog_content, R.string.pf_save_dialog_btn_save,
                R.string.pf_save_dialog_btn_cancel, new ProfileChangeListener()).show();
    }

    private class ProfileChangeListener extends DialogHelper.Listener {

        @Deprecated
        public void onPositive(MaterialDialog dialog) {
            updateProfileDetails();
        }

        @Deprecated
        public void onNegative(MaterialDialog dialog) {
//            toggleView();
        }
    }

    private void updateProfileDetails() {
        User user = mUser;

        Log.d("PICTUREUPLOAD", "USER DETAILS: " + mUser.toString());

        user.setFirstName(mEdtFirstName.getText().toString());
        user.setSurname(mEdtSurname.getText().toString());
        user.setEmail(mEdtEmail.getText().toString());
        user.setCellNbr(mEdtCellNbr.getText().toString());
        user.setPassword(mEdtPwd.getText().toString());
//        user.setPictureUrls(mProfilePixPath);

        Log.d("PICTUREUPLOAD", "USER DETAILS2: " + user.toString());

        try {
            updateLoginStatus(user);
        } catch (Exception e) {
            Log.d("RANDOM_EXCEPTION", e.getMessage());
        }

        /*if (mDialog == null) {
            mDialog = DialogHelper.loadingDialog(getContext(), getString(
                    R.string.pf_updating_profile_msg_dialog)).show();
        } else {
            mDialog.setContent(R.string.pf_updating_profile_msg_dialog);
        }*/

//        UserHelper.update(getContext(), user, new ProfileUpdateListener());
    }

    private class ProfileUpdatedListener extends DialogHelper.Listener {

        @Override
        public void onAny(MaterialDialog dialog) {
//            toggleView();
            mFragmentSwitcher.changeProfileDetails();
        }
    }

    private void updateLoginStatus(User user) {
        user.setLogInStatus(User.LOGIN_STATUS_LOGGED_IN);
        user.save();

        boolean profileEdited = (user == mUser);

        Ndule.setUser(user);

        mProfileFragmentSwitcher.profileEdited(true, true);
    }

    /*private void updateProfilePicture() {
        mDialog = DialogHelper.loadingDialog(getContext(), getString(
                R.string.pf_saving_pix_msg_dialog)).show();

        PixUploadHelper.uploadProfilePix(getContext(), mProfilePixBitmap,
                new PixListResultHandler(getContext(), PixListResultHandler.PIX_OPTIONS.PROFILE_PICTURE,
                        new PixUpdateListener()));
    }

    private class PixUpdateListener implements NetworkListener {

        private String message = getString(R.string.pf_saving_error);

        @Override
        public void success(Object data) {
//            mDialog.dismiss();

            ArrayList<Pix> pictures = (ArrayList<Pix>) data;

            Log.d("PICTUREUPLOAD", pictures.size() + "");

            if (!pictures.isEmpty()) {
                Pix pix = pictures.get(0);
                mUser = Ndule.getUser();
                mUser.setPictureUrl(pix.getImageUrl());

                Log.d("PICTUREUPLOAD", "RESULT: " + pix.getImageUrl());

                *//*DialogHelper.showMessage(getContext(), R.string.pf_save_dialog_title,
                        R.string.pf_pix_saved_msg_dialog, new PixUpdatedListener());*//*

                updateProfileDetails();
            } else {
                error("");
            }
        }

        @Override
        public void error(String message) {
//            mDialog.dismiss();

            updateProfileDetails();

            Log.d("PICTUREUPLOAD", "RESULT2: " + message);
//            loadUsers();
//            Log.d("USERS_COUNT", "MESSAGE: " + message);
            Toast.makeText(getContext(), this.message, Toast.LENGTH_SHORT).show();
        }
    }*/

    /*private class ProfileUpdateListener implements NetworkListener {

        private String message = getString(R.string.pf_saving_error);

        @Override
        public void success(Object data) {
            mDialog.dismiss();

            ArrayList<User> users = (ArrayList<User>) data;

            Log.d("LOGINRETURNED", "" + users.toString());

            if (!users.isEmpty()) {
                updateLoginStatus(users.get(0));

                DialogHelper.showMessage(getContext(), R.string.pf_save_dialog_title,
                    R.string.pf_profile_updated_msg_dialog, new ProfileUpdatedListener());
            }
        }

        @Override
        public void error(String message) {
            mDialog.dismiss();

            mUser = Ndule.getUser();
            toggleView();
//            loadUsers();
//            Log.d("USERS_COUNT", "MESSAGE: " + message);
            Toast.makeText(getContext(), this.message, Toast.LENGTH_SHORT).show();
        }
    }*/
}