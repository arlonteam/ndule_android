package arlon.android.portfolio.ndule.helpers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.android.volley.Request;
import com.orm.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.interfaces.ResultHandler;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Event;
import arlon.android.portfolio.ndule.objects.Song;

/**
 * Created by Arlon Mukeba on 2/6/2018.
 */

public class SongHelper {

    private static final String SONG_SERVER_URL = "";

    private static byte[] generateKey(String password) throws Exception {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        secureRandom.setSeed(password.getBytes("UTF-8"));
        keyGen.init(128, secureRandom);
        SecretKey secretKey = keyGen.generateKey();
        return secretKey.getEncoded();
    }

    private static byte[] encodeFile(byte[] key, byte[] fileData) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

        return cipher.doFinal(fileData);
    }

    public static byte[] decodeFile(byte[] key, byte[] fileData) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);

        return cipher.doFinal(fileData);
    }

    public static byte[] readDownloadedAudio(String filename, String password) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        FileInputStream fis = new FileInputStream(new File(filename));
        byte data[] = new byte[1024];
        int count = 0;

        while ((count = fis.read(data)) != -1) {
            out.write(data, 0, count);
        }

        fis.close();

        return decodeFile(generateKey(password), out.toByteArray());
    }

    public static String saveDownloadedAudio(File storage, String filename, byte[] fileData,
        String password) throws Exception {

        String dirname = new StringBuffer(storage.getAbsolutePath()).append(File.separator)
            .append("ndule").append(File.separator).append("songs").toString();

        File dir = new File(dirname);
        dir.mkdirs();

        File file = new File(dir, filename);
        file.setReadable(true, false);

        FileOutputStream fos = new FileOutputStream(file);
        byte[] fileBytes = encodeFile(generateKey(password), fileData);
        fos.write(fileBytes);
        fos.close();

        return file.getAbsolutePath();
    }

    public static boolean wasSongSaved(Song song) {
        if (song.getSavedUrl().trim().isEmpty()) return false;

        return new File(song.getSavedUrl()).exists();
    }

    public static boolean wasSongTempSaved(Context context, Song song) {
        String filename = song.getTitle().replaceAll(" ", "_");
        filename = context.getCacheDir().getAbsolutePath() + "/songs/" + filename + ".mp3";

        return new File(filename).exists();
    }

    public static FileDescriptor getTempAudio(Context context, String name, byte[] bytes) throws IOException {
        File dir = new File(context.getCacheDir(), "songs");

        if (!dir.exists()) {
            dir.mkdir();
        }

        File tempMp3 = File.createTempFile(name, ".mp3", dir);
        tempMp3.deleteOnExit();
        tempMp3.setReadable(true, false);
        Log.d("SONG_DATA", "ABSOLUTE_PATH: " + tempMp3.getAbsolutePath());

        FileOutputStream fos = new FileOutputStream(tempMp3);
        fos.write(bytes);
        fos.close();

        return new FileInputStream(tempMp3).getFD();
    }

    public static void deleteTempAudioFile(Context context, String filename) {
        filename = context.getCacheDir().getAbsolutePath() + "/songs/" + filename + ".mp3";
        File file = new File(filename);

        if (file.exists()) {
            file.delete();
        }
    }

    public static void listTempDir(Context context) {
        String dirname = context.getCacheDir().getAbsolutePath() + "/songs";
        File dir = new File(dirname);

        String strToDisplay = "";

        if ((dir.exists()) && (dir.isDirectory())) {
            String[] children = dir.list();

            for (int i = 0; i < children.length; i++) {
                strToDisplay += (new File(dir, children[i]).getAbsolutePath() + "\n");
            }
        }

        Log.d("DIRECTORY_LIST", "FILE_LIST:\n" + strToDisplay);
    }

    public static void deleteTempAudioDir(Context context) {
        String dirname = context.getCacheDir().getAbsolutePath() + "/songs";
        File dir = new File(dirname);

        if ((dir.exists()) && (dir.isDirectory())) {
            String[] children = dir.list();

            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }
    }

    public static void retrieveSongData(Context context, String songId, ResultHandler resultHandler)
            throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Song.COLUMN_ID, songId);

        CustomRequest customRequest = new CustomRequest(context, Request.Method.POST,
            Ndule.SONG_URL, jsonObject, resultHandler);

        NetworkHelper networkHelper = NetworkHelper.getNetworkManager(context);
        networkHelper.makeRequest(customRequest);
    }

    public static void loadDummySongs() {
        createArtists();
        createSongs();
        createEvents();
    }

    private static void createArtists() {
        Artist artist = Select.from(Artist.class).first();

        if (artist != null) return;

        String[] names = {
                "Céline Dion", "Adèle", "Luis Fonsi", "One Direction",
                "Ed Sheeran", "Passenger", "Maroon 5", "Tamela Mann", "Mary Mary", "The Vamps",
                "Taylor Swift"
        };

        String[] images = {
                "celine_dion", "adele", "luis_fonsi", "one_direction", "ed_sheeran", "passenger",
                "maroon_5", "tamela_mann", "mary_mary", "the_vamps", "taylor_swift"
        };

        for (int i = 0; i < names.length; i++) {
            artist = new Artist();
            artist.setName(names[i]);
            artist.setImage(images[i]);
            artist.save();
        }
    }

    private static void createSongs() {
        Song song = Select.from(Song.class).first();

        if (song != null) return;

        String[] titles = {
                "I'm Alive", "Rolling in the Deep", "Despacito", "Little Things",
                "Shape of You", "Let Her Go", "Payphone", "Take Me to the King", "Shackles",
                "Oh Cecilia", "I Knew You Were Trouble", "A New Day Has Come", "Sunday Morning", "Hello",
                "I See Fire", "That's What Makes You Beautiful"
        };

        String[] albums = {
                "A New Day Has Come", "21", "Daddy K - The Mix 11", "Take Me Home",
                "÷", "All the Little Lights", "Overexposed", "Best Days", "Thankful",
                "Meet the Vamps", "Red", "A New Day Has Come", "Songs about Jane", "25",
                "The Hobbit: The Desolation of Smaug", "Up All Night"
        };

        String[] artists = {
                "Céline Dion", "Adèle", "Luis Fonsi", "One Direction",
                "Ed Sheeran", "Passenger", "Maroon 5", "Tamela Mann", "Mary Mary", "The Vamps",
                "Taylor Swift", "Céline Dion", "Maroon 5", "Adèle", "Ed Sheeran", "One Direction"
        };

        String[] images = {
                "i_am_alive", "rolling_in_the_deep", "despacito", "little_things", "shape_of_you",
                "let_her_go", "payphone", "take_me_to_the_king", "shackles", "oh_cecilia",
                "i_knew_you_were_trouble", "a_new_day_has_come", "sunday_morning", "hello",
                "i_see_fire", "that_is_what_makes_you_beautiful"
        };

        String[] audios = {
            "i_am_alive", "rolling_in_the_deep", "despacito", "little_things", "shape_of_you",
            "let_her_go", "payphone", "take_me_to_the_king", "shackles", "oh_cecilia",
            "i_knew_you_were_trouble", "a_new_day_has_come", "sunday_morning", "hello", "i_see_fire",
            "that_is_what_makes_you_beautiful"
        };

        ArrayList<Artist> artistList = (ArrayList<Artist>) Select.from(Artist.class).list();
        Artist artist = null;

        for (int i = 0; i < titles.length; i++) {
            song = new Song();
            song.setTitle(titles[i]);
            song.setAlbumTitle(albums[i]);
            song.setThumbnail(images[i]);
            song.setOriginalUrl(audios[i]);
            song.setType(Song.SONG_TYPE_REMOTE);

            double price = (250 * new Random().nextDouble());
            song.setPrice(price);

            try {
                artist = new Artist();
                artist.setName(artists[i]);
                artist = artistList.get(artistList.indexOf(artist));
                song.setArtist(artist);
            } catch (Exception e) {

            }

            song.save();
        }
    }

    private static void createEvents() {
        Event event = Select.from(Event.class).first();

        if (event != null) return;

        String[] eventPosters = { "event_1", "event_2" };

        for (int i = 0; i < eventPosters.length; i++) {
            event = new Event();
            event.setThumbnail(eventPosters[i]);
            event.save();
        }
    }
}