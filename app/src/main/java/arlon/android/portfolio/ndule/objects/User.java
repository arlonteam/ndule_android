package arlon.android.portfolio.ndule.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class User extends SugarRecord implements Parcelable {

    public final static String USER = "user";

    public enum ACTION { INVITE, SHARE, TAG };

    public final static String COLUMN_ID = "user_id";
    public final static String COLUMN_TIMESTAMP = "timestamp";
    public final static String COLUMN_DEVICE_TOKEN = "device_token";
    public final static String COLUMN_LOCAL_TYPE = "local_type";
    public final static String COLUMN_USERNAME = "username";
    public final static String COLUMN_FIRST_NAME = "first_name";
    public final static String COLUMN_SURNAME = "surname";
    public final static String COLUMN_GENDER = "gender";
    public final static String COLUMN_CELL_NBR = "cell_nbr";
    public final static String COLUMN_BIRTH_DATE = "birth_date";
    public final static String COLUMN_EMAIL = "email";
    public final static String COLUMN_PASSWORD = "password";
    public final static String COLUMN_PICTURE = "picture";
    public final static String COLUMN_PHYSICAL_ADDRESS = "physical_address";
    public final static String COLUMN_CREDIT = "credit";
    public final static String COLUMN_CREDIT_EXPIRY_DATE = "credit_expiry_date";
    public final static String COLUMN_LOGIN_STATUS = "login_status";

    public final static String LOGIN_STATUS_LOGGED_OUT = "LOGGED_OUT";
    public final static String LOGIN_STATUS_LOGGED_IN = "LOGGED_IN";

    public final static String USER_TYPE_ME = "ME";
    public final static String USER_TYPE_OTHER = "OTHER";
    public final static String USER_TYPE_DUMMY = "DUMMY";

    @Column(name = COLUMN_ID)
    private String mId = "";

    @Column(name = COLUMN_DEVICE_TOKEN)
    private String mDeviceToken = "";

    @Column(name = COLUMN_TIMESTAMP)
    private String mTimeStamp = "";

    @Column(name = COLUMN_LOCAL_TYPE)
    private String mLocalType = USER_TYPE_OTHER;

    @Column(name = COLUMN_USERNAME)
    private String mUsername = "";

    @Column(name = COLUMN_FIRST_NAME)
    private String mFirstName = "";

    @Column(name = COLUMN_SURNAME)
    private String mSurname = "";

    @Column(name = COLUMN_GENDER)
    private String mGender = "1";

    @Column(name = COLUMN_CELL_NBR)
    private String mCellNbr = "0027715983658";

    @Column(name = COLUMN_BIRTH_DATE)
    private String mBirthDate = "02/11/2017";

    @Column(name = COLUMN_EMAIL)
    private String mEmail = "";

    @Column(name = COLUMN_PASSWORD)
    private String mPassword = "";

    @Column(name = COLUMN_PICTURE)
    private String mPictureUrl = "";

    @Column(name = COLUMN_PHYSICAL_ADDRESS)
    private String mPhysicalAddress = "";

    @Column(name = COLUMN_CREDIT)
    private double mCredit = 0.00;

    @Column(name = COLUMN_CREDIT_EXPIRY_DATE)
    private String mCreditExpiryDate = "02/11/2017";

    @Column(name = COLUMN_LOGIN_STATUS)
    private String mLoginStatus = LOGIN_STATUS_LOGGED_OUT;

    public User() {
    }

    public User(Parcel in) {
        setId(in.readLong());
        setUserId(in.readString());
        setDeviceToken(in.readString());
        setUsername(in.readString());
        setFirstName(in.readString());
        setSurname(in.readString());
        setGender(in.readString());
        setCellNbr(in.readString());
        setBirthDate(in.readString());
        setEmail(in.readString());
        setPassword(in.readString());
        setCredit(in.readDouble());
        setCreditExpiryDate(in.readString());
        setPictureUrl(in.readString());
        setPhysicalAddress(in.readString());
        setLogInStatus(in.readString());
        setLocalType(in.readString());
    }

    public void setUserId(String id) {
        mId = id;
    }

    public void setDeviceToken(String deviceToken) {
        mDeviceToken = deviceToken;
    }

    public void setLocalType(String localType) {
        mLocalType = localType;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public void setCellNbr(String cellNbr) {
        mCellNbr = cellNbr;
    }

    public void setBirthDate(String birthDate) {
        mBirthDate = birthDate;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public void setCredit(double credit) {
        mCredit = credit;
    }

    public void setCreditExpiryDate(String creditExpiryDate) {
        mCreditExpiryDate = creditExpiryDate;
    }

    public void setPictureUrl(String pictureUrl) {
        mPictureUrl = pictureUrl;
    }

    public void setPhysicalAddress(String physicalAddress) {
        mPhysicalAddress = physicalAddress;
    }

    public void setLogInStatus(String loginStatus) {
        mLoginStatus = loginStatus;
    }

    public String getUserId() {
        return mId;
    }

    public String getDeviceToken() {
        return mDeviceToken;
    }

    public String getLocalType() {
        return mLocalType;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getSurname() {
        return mSurname;
    }

    public String getGender() {
        return mGender;
    }

    public String getCellNbr() {
        return mCellNbr;
    }

    public String getBirthDate() {
        return mBirthDate;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPassword() {
        return mPassword;
    }

    public double getCredit() {
        return mCredit;
    }

    public String getCreditExpiryDate() {
        return mCreditExpiryDate;
    }

    public String getPixUrl() {
        return mPictureUrl;
    }

    public String getPhysicalAddress() {
        return mPhysicalAddress;
    }

    private String getLoginStatus() {
        return mLoginStatus;
    }

    public boolean isLoggedIn() {
        return mLoginStatus.equalsIgnoreCase(LOGIN_STATUS_LOGGED_IN);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {

        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getUserId());
        dest.writeString(getDeviceToken());
        dest.writeString(getUsername());
        dest.writeString(getFirstName());
        dest.writeString(getSurname());
        dest.writeString(getGender());
        dest.writeString(getCellNbr());
        dest.writeString(getBirthDate());
        dest.writeString(getEmail());
        dest.writeString(getPassword());
        dest.writeDouble(getCredit());
        dest.writeString(getCreditExpiryDate());
        dest.writeString(getPixUrl());
        dest.writeString(getPhysicalAddress());
        dest.writeString(getLoginStatus());
        dest.writeString(getLocalType());
    }

    public boolean equals(Object object) {
        if (!(object instanceof User)) return false;

        boolean checker = true;

        User user = (User) object;

        checker &= (user.getFirstName().equalsIgnoreCase(getFirstName()));
        checker &= (user.getSurname().equalsIgnoreCase(getSurname()));
        checker &= (user.getEmail().equalsIgnoreCase(getEmail()));
        checker &= (user.getCellNbr().equalsIgnoreCase(getCellNbr()));

        return checker;
    }

    public long save() {
        if (mTimeStamp.isEmpty()) {
            mTimeStamp = "" + System.currentTimeMillis();
        }

        if (mId.isEmpty()) {
            mId = "User_" + mTimeStamp;
        }

        return super.save();
    }

    public String toString () {
        return new StringBuffer("USER: ").append("\n\n")
            .append("Device Token: ").append(mDeviceToken).append("\n")
                .append("First Name: ").append(mFirstName).append("\n")
                    .append("Surname: ").append(mSurname).append("\n")
                        .append("Cell Nbr: ").append(mCellNbr).append("\n")
                            .append("Birth Date: ").append(mBirthDate).append("\n")
                                .append("Email: ").append(mEmail).append("\n")
                                    .append("Username: ").append(mUsername).append("\n")
                                        .append("Password: ").append(mPassword).append("\n")
                                            .append("Physical Address: ").append(mPhysicalAddress).append("\n")
                                                .append("Pix URL: ").append(mPictureUrl).append("\n")
                                                    .append("Login Status: ").append(mLoginStatus).toString();
    }

    /*public static JSONObject toLoginApi(User user) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CustomRequest.OBJECT_PARAM, CustomRequest.USER);
            jsonObject.put(CustomRequest.ACTION_PARAM, UserHelper.REQUEST_TAG_LOGIN);
            jsonObject.put(COLUMN_EMAIL, user.getEmail());
            jsonObject.put(COLUMN_PASSWORD, user.getPassword());
        } catch (Exception e) {
            Log.d("REQUESTBODY", "EXCEPTION: " + e.getMessage());
        }

        return jsonObject;
    }

    public static JSONObject toRegisterApi(User user, String action) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CustomRequest.OBJECT_PARAM, CustomRequest.USER);
            jsonObject.put(CustomRequest.ACTION_PARAM, action*//*isCreating ? "create" : "update"*//*);
            jsonObject.put(COLUMN_ID, user.getUserId());
            jsonObject.put(COLUMN_DEVICE_TOKEN, user.getDeviceToken());
            jsonObject.put(COLUMN_USERNAME, user.getUsername());
            jsonObject.put(COLUMN_FIRST_NAME, user.getFirstName());
            jsonObject.put(COLUMN_SURNAME, user.getSurname());
            jsonObject.put(COLUMN_GENDER, user.getGender());
            jsonObject.put(COLUMN_BIRTH_DATE, user.getBirthDate());
            jsonObject.put(COLUMN_EMAIL, user.getEmail());
            jsonObject.put(COLUMN_PASSWORD, user.getPassword());
            jsonObject.put(COLUMN_PHYSICAL_ADDRESS, user.getPhysicalAddress());
            jsonObject.put(COLUMN_PICTURE, user.getPixUrl());
        } catch (JSONException e) {

        }

        return jsonObject;
    }

    public static User fromRegisterApi(JSONObject jsonObject) {
        User user = new User();

        user.setUserId(JsonHelper.getString(jsonObject, COLUMN_ID));
        user.setDeviceToken(JsonHelper.getString(jsonObject, COLUMN_DEVICE_TOKEN));
        user.setUsername(JsonHelper.getString(jsonObject, COLUMN_USERNAME));
        user.setFirstName(JsonHelper.getString(jsonObject, COLUMN_FIRST_NAME));
        user.setSurname(JsonHelper.getString(jsonObject, COLUMN_SURNAME));
        user.setGender(JsonHelper.getString(jsonObject, COLUMN_GENDER));
        user.setBirthDate(JsonHelper.getString(jsonObject, COLUMN_BIRTH_DATE));
        user.setEmail(JsonHelper.getString(jsonObject, COLUMN_EMAIL));
        user.setPassword(JsonHelper.getString(jsonObject, COLUMN_PASSWORD));
        user.setPhysicalAddress(JsonHelper.getString(jsonObject, COLUMN_PHYSICAL_ADDRESS));
        user.setPictureUrl(JsonHelper.getString(jsonObject, COLUMN_PICTURE));

        return user;
    }

    public static JSONObject userIdToJSON(User user) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(COLUMN_USERNAME, user.getUsername());
        return jsonObject;
    }

    public static JSONObject userIdsToJSONArray(ArrayList<User> users) {
        JSONArray userArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        try {
            for (User user : users) {
                userArray.put(userIdToJSON(user));
            }

            jsonObject.put("", userArray);
        } catch (Exception e) {

        }

        return jsonObject;
    }*/
}