package arlon.android.portfolio.ndule.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.DialogHelper;
import arlon.android.portfolio.ndule.objects.User;
import arlon.android.portfolio.ndule.utils.StringUtils;

/**
 * Created by Arlon Mukeba on 1/17/2018.
 */

public class SignUpFragment extends BaseFragment {

    public static final String TAG = "SignUpFragment";

    private EditText mEdtFirstName = null;
    private EditText mEdtSurname = null;
    private EditText mEdtCellNbr = null;
    private EditText mEdtEmail = null;
    private EditText mEdtPwd = null;
    private EditText mEdtConfirmPwd = null;
    private TextView mTvSignUp = null;

    public static SignUpFragment newInstance(Hashtable<String, Object> params) {
        return new SignUpFragment();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_sign_up;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
    }

    public void setupViews() {
        mEdtFirstName = (EditText) getView().findViewById(R.id.edtFirstName);
        mEdtSurname = (EditText) getView().findViewById(R.id.edtSurname);
        mEdtCellNbr = (EditText) getView().findViewById(R.id.edtCellNbr);
        mEdtEmail = (EditText) getView().findViewById(R.id.edtEmail);
        mEdtPwd = (EditText) getView().findViewById(R.id.edtPwd);
        mEdtConfirmPwd = (EditText) getView().findViewById(R.id.edtConfirmPwd);

        mTvSignUp = (TextView) getView().findViewById(R.id.tvSignUp);
        mTvSignUp.setOnClickListener(new SignUpBtnClicked());
    }

    private void signUp() {
        User user = validateSignUp();

        if (user == null) {
            Toast.makeText(getContext(), R.string.ls_empty_fields_error, Toast.LENGTH_SHORT).show();
            return;
        }

        user.setLocalType(User.USER_TYPE_ME);
        user.setUsername(Ndule.getDeviceId(getContext()));
        user.setLogInStatus(User.LOGIN_STATUS_LOGGED_IN);
        user.save();

        Ndule.setUser(user);

        mFragmentSwitcher.navigateBack(Activity.RESULT_OK);

        /*mDialog = DialogHelper.loadingDialog(this, getString(R.string.af_dialog_message)).show();

        UserHelper.signUp(this, user, new SignUpHandler());*/
    }

    private User validateSignUp() {
        User user = new User();

        if (mEdtFirstName.getText().toString().isEmpty()) {
            mEdtFirstName.setError(getString(R.string.ls_empty_first_name_error));
            user = null;
        } else {
            user.setFirstName(mEdtFirstName.getText().toString());
        }

        if (mEdtSurname.getText().toString().isEmpty()) {
            mEdtSurname.setError(getString(R.string.ls_empty_surname_error));
            user = null;
        } else {
            if (user != null) {
                user.setSurname(mEdtSurname.getText().toString());
            }
        }

        if (mEdtCellNbr.getText().toString().isEmpty()) {
            mEdtCellNbr.setError(getString(R.string.ls_empty_cell_nbr_error));
            user = null;
        } else {
            if (user != null) {
//                user.setCellNbr(mEdtCellNbr.getText().toString());
            }
        }

        if (mEdtEmail.getText().toString().isEmpty()) {
            mEdtEmail.setError(getString(R.string.ls_empty_email_error));
            user = null;
        } else {
            if (!StringUtils.validateEmail(mEdtEmail.getText().toString())) {
                mEdtEmail.setError(getString(R.string.ls_invalid_email_error));
                user = null;
            } else {
                if (user != null) {
                    user.setEmail(mEdtEmail.getText().toString());
                }
            }
        }

        if (mEdtPwd.getText().toString().isEmpty()) {
            mEdtPwd.setError(getString(R.string.ls_empty_password_error));
            user = null;
        } else {
            if (user != null) {
                user.setPassword(mEdtPwd.getText().toString());
            }
        }

        if (mEdtConfirmPwd.getText().toString().isEmpty()) {
            mEdtConfirmPwd.setError(getString(R.string.ls_password_not_matching_error));
            user = null;
        } else {
            if (user != null) {
                user.setPassword(mEdtConfirmPwd.getText().toString());
            }
        }

        return user;
    }

    private class SignUpBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            signUp();
        }
    }

    /*private class SignUpHandler implements NetworkListener {

        private String message = getString(R.string.ls_sign_up_error);

        @Override
        public void success(Object data) {
            mDialog.dismiss();

            ArrayList<User> users = (ArrayList<User>) data;

            Log.d("LOGINRETURNED", users.size() + "");

            if (!users.isEmpty()) {
                DialogHelper.showMessage(LoginScreen.this, R.string.ls_dialog_heading,
                        R.string.ls_su_msg, new DialogBtnClicked(RESULT_CANCELED));
            }
        }

        @Override
        public void error(String message) {
            mDialog.dismiss();
//            loadUsers();
//            Log.d("USERS_COUNT", "MESSAGE: " + message);
            Toast.makeText(LoginScreen.this, this.message, Toast.LENGTH_SHORT).show();
        }
    }*/
}