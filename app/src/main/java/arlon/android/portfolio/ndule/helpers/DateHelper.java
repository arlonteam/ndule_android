package arlon.android.portfolio.ndule.helpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import arlon.android.portfolio.ndule.R;

/**
 * Created by Arlon Mukeba on 1/24/2018.
 */

public class DateHelper {

    public static Date dateFromString(String stringDate) {
        try {
            return new SimpleDateFormat("dd/mm/yyyy", Locale.ENGLISH).parse(stringDate);
        } catch (Exception exception) {
            return null;
        }
    }

    public static String dateToString(Date date) {
        return new SimpleDateFormat("dd/mm/yyyy", Locale.ENGLISH).format(date);
    }

    public static int ageFromDOB(String stringDob) {
        Date dob = new Date();

        try {
            dob = new SimpleDateFormat("dd/mm/yyyy", Locale.ENGLISH).parse(stringDob);
        } catch (Exception e) {

        }

        return ageFromDOB(dob);
    }

    public static int ageFromDOB(Date dob) {
        return computeAge(dob);
    }

    private static int computeAge(Date date) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date());

        Calendar c2 = Calendar.getInstance();
        c2.setTime(date);

        Log.d("SELECTEDDATE", c1.get(Calendar.YEAR) + " : " + c2.get(Calendar.YEAR));

        return c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
    }
}