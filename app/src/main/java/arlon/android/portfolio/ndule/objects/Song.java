package arlon.android.portfolio.ndule.objects;

import android.os.Parcel;
import android.os.Parcelable;
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.query.Select;

import java.util.ArrayList;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class Song extends SugarRecord implements Parcelable {

    public final static String SONG = "song";
    public final static String SONG_TYPE_LOCAL = "LOCAL";
    public final static String SONG_TYPE_REMOTE = "REMOTE";

    public final static String COLUMN_ID = "song_id";
    public final static String COLUMN_TIMESTAMP = "timestamp";
    public final static String COLUMN_TITLE = "title";
    public final static String COLUMN_ALBUM_TITLE = "album_title";
    public final static String COLUMN_ARTIST_ID = "artist_id";
    public final static String COLUMN_THUMBNAIL = "thumbnail";
    public final static String COLUMN_URL = "url";
    public final static String COLUMN_SONG_BYTES = "data";
    public final static String COLUMN_TYPE = "is_local";
    public final static String COLUMN_RELEASE_DATE = "release_date";
    public final static String COLUMN_LIKED = "liked";
    public final static String COLUMN_ADDED_TO_PLAYLIST = "add_to_playlist";
    public final static String COLUMN_PRICE = "price";
    public final static String COLUMN_PURCHASED = "purchased";

    @Column(name = COLUMN_ID)
    private String mSongId = "";

    @Column(name = COLUMN_TIMESTAMP)
    private String mTimeStamp = "";

    @Column(name = COLUMN_TITLE)
    private String mTitle = "";

    @Column(name = COLUMN_ALBUM_TITLE)
    private String mAlbumTitle = "";

    @Column(name = COLUMN_ARTIST_ID)
    private String mArtistId = "";

    @Column(name = COLUMN_THUMBNAIL)
    private String mThumbnail = "";

    @Column(name = COLUMN_URL)
    private String mOriginalUrl = "";

    @Column(name = COLUMN_SONG_BYTES)
    private String mSavedUrl = "";

    @Column(name = COLUMN_TYPE)
    private String mType = "";

    @Column(name = COLUMN_RELEASE_DATE)
    private String mReleaseDate = "01/05/12";

    @Column(name = COLUMN_LIKED)
    private String mLiked = "NO";

    @Column(name = COLUMN_ADDED_TO_PLAYLIST)
    private String mBelongsToPlaylist = "NO";

    @Column(name = COLUMN_PRICE)
    private double mPrice = 0.0;

    @Column(name = COLUMN_PURCHASED)
    private String mPurchased = "NO";

    private Artist mArtist = null;

    public Song() {
    }

    public Song(Parcel in) {
        setId(in.readLong());
        setSongId(in.readString());
        setTitle(in.readString());
        setAlbumTitle(in.readString());
        setArtist((Artist) in.readParcelable(Artist.class.getClassLoader()));
        setThumbnail(in.readString());
        setOriginalUrl(in.readString());
        setReleaseDate(in.readString());
        setPrice(in.readDouble());
        setLiked(in.readString());
        setPlaylistStatus(in.readString());
        setOwnershipStatus(in.readString());
        /*byte[] data = new byte[in.readInt()];
        in.readByteArray(data);*/
        setSavedUrl(in.readString());
        setType(in.readString());
    }

    public void setSongId(String songId) {
        mSongId = songId;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setAlbumTitle(String albumTitle) {
        mAlbumTitle = albumTitle;
    }

    public void setArtistId(String artistId) {
        mArtistId = artistId;
    }

    public void setArtist(Artist artist) {
        mArtist = artist;
        setArtistId(mArtist.getArtistId());
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public void setOriginalUrl(String originalUrl) {
        mOriginalUrl = originalUrl;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public void like() {
        setLiked("YES");
    }

    public void unlike() {
        setLiked("NO");
    }

    private void setLiked(String liked) {
        mLiked = liked;
    }

    private void setPlaylistStatus(String belongsToPlaylist) {
        mBelongsToPlaylist = belongsToPlaylist;
    }

    public void setOwnershipStatus(String purchased) {
        mPurchased = purchased;
    }

    public void setSavedUrl(String savedUrl) {
        mSavedUrl = savedUrl;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getSongId() {
        return mSongId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getAlbumTitle() {
        return mAlbumTitle;
    }

    public Artist getArtist() {
        if (mArtist == null) {
            Artist artist = new Artist();
            artist.setArtistId(mArtistId);
            ArrayList<Artist> artistList = (ArrayList<Artist>) Select.from(Artist.class).list();
            mArtist = artistList.get(artistList.indexOf(artist));
        }

        return mArtist;
    }

    public String getArtistId() {
        return mArtistId;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public String getOriginalUrl() {
        return mOriginalUrl;
    }

    public String getSavedUrl() {
        return mSavedUrl;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public double getPrice() {
        return mPrice;
    }

    public boolean isLiked() {
        return mLiked.equalsIgnoreCase("YES");
    }

    public boolean belongsToPlaylist() {
        return mBelongsToPlaylist.equalsIgnoreCase("YES");
    }

    public void addToPlayList() {
        setPlaylistStatus("YES");
    }

    public void removeFromPlayList() {
        setPlaylistStatus("NO");
    }

    public boolean belongsToUser() {
        return mPurchased.equalsIgnoreCase("YES");
    }

    public boolean isLocal() {
        return (mType.equalsIgnoreCase(SONG_TYPE_LOCAL));
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {

        @Override
        public Song createFromParcel(Parcel source) {
            return new Song(source);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getSongId());
        dest.writeString(getTitle());
        dest.writeString(getAlbumTitle());
        dest.writeParcelable(getArtist(), flags);
        dest.writeString(getThumbnail());
        dest.writeString(getOriginalUrl());
        dest.writeString(getReleaseDate());
        dest.writeDouble(getPrice());
        dest.writeString(isLiked() ? "YES" : "NO");
        dest.writeString(belongsToPlaylist() ? "YES" : "NO");
        dest.writeString(belongsToUser() ? "YES" : "NO");
        dest.writeString(getSavedUrl());
        /*dest.writeByteArray(getSavedUrl());*/
        dest.writeString(isLocal() ? SONG_TYPE_LOCAL : SONG_TYPE_REMOTE);
    }

    public long save() {
        if (mTimeStamp.isEmpty()) {
            mTimeStamp = "" + System.currentTimeMillis();
        }

        if (mSongId.isEmpty()) {
            mSongId = "Song_" + mTimeStamp + "_" + mTitle;
        }

        return super.save();
    }

    public boolean equals(Object object) {
        boolean equals = (object != null);
        equals &= (object instanceof Song);

        if (equals) {
            equals = getTitle().equalsIgnoreCase(((Song) object).getTitle());
            equals |= getSongId().equalsIgnoreCase(((Song) object).getSongId());
        }

        return equals;
    }

    public String toString () {
        return new StringBuffer("COMMENT: ").append("\n\n")
                .append("Id: ").append(mSongId).append("\n")
                .append("Title: ").append(mTitle).append("\n")
                .append("Album Title: ").append(mAlbumTitle).append("\n")
                .append("Artist ID: ").append(mArtistId).append("\n")
                .append("Thumbnail: ").append(mThumbnail).append("\n")
                .append("Playlist: ").append(mBelongsToPlaylist).append("\n")
                .append("Data: ").append(mSavedUrl).append("\n")
                .append("Release Date: ").append(mReleaseDate).append("\n")
                .append("Fav: ").append(mLiked).append("\n\n").toString();
    }

    /*public static JSONObject toLoginApi(User user) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CustomRequest.OBJECT_PARAM, CustomRequest.USER);
            jsonObject.put(CustomRequest.ACTION_PARAM, UserHelper.REQUEST_TAG_LOGIN);
            jsonObject.put(COLUMN_EMAIL, user.getEmail());
            jsonObject.put(COLUMN_PASSWORD, user.getPassword());
        } catch (Exception e) {
            Log.d("REQUESTBODY", "EXCEPTION: " + e.getMessage());
        }

        return jsonObject;
    }

    public static JSONObject toRegisterApi(User user, String action) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CustomRequest.OBJECT_PARAM, CustomRequest.USER);
            jsonObject.put(CustomRequest.ACTION_PARAM, action*//*isCreating ? "create" : "update"*//*);
            jsonObject.put(COLUMN_ID, user.getArtist());
            jsonObject.put(COLUMN_DEVICE_TOKEN, user.getDeviceToken());
            jsonObject.put(COLUMN_NAME, user.getName());
            jsonObject.put(COLUMN_ALBUM_TITLE, user.getAlbumTitle());
            jsonObject.put(COLUMN_AUTHOR_PSEUDO, user.getArtist());
            jsonObject.put(COLUMN_GENDER, user.getGender());
            jsonObject.put(COLUMN_BIRTH_DATE, user.getBirthDate());
            jsonObject.put(COLUMN_EMAIL, user.getEmail());
            jsonObject.put(COLUMN_PASSWORD, user.getPassword());
            jsonObject.put(COLUMN_PHYSICAL_ADDRESS, user.getPhysicalAddress());
            jsonObject.put(COLUMN_IMAGE, user.getImage());
        } catch (JSONException e) {

        }

        return jsonObject;
    }

    public static User fromRegisterApi(JSONObject jsonObject) {
        User user = new User();

        user.setArtist(JsonHelper.getString(jsonObject, COLUMN_ID));
        user.setDeviceToken(JsonHelper.getString(jsonObject, COLUMN_DEVICE_TOKEN));
        user.setName(JsonHelper.getString(jsonObject, COLUMN_NAME));
        user.setAlbumTitle(JsonHelper.getString(jsonObject, COLUMN_ALBUM_TITLE));
        user.setArtist(JsonHelper.getString(jsonObject, COLUMN_AUTHOR_PSEUDO));
        user.setGender(JsonHelper.getString(jsonObject, COLUMN_GENDER));
        user.setBirthDate(JsonHelper.getString(jsonObject, COLUMN_BIRTH_DATE));
        user.setEmail(JsonHelper.getString(jsonObject, COLUMN_EMAIL));
        user.setPassword(JsonHelper.getString(jsonObject, COLUMN_PASSWORD));
        user.setPhysicalAddress(JsonHelper.getString(jsonObject, COLUMN_PHYSICAL_ADDRESS));
        user.setImage(JsonHelper.getString(jsonObject, COLUMN_IMAGE));

        return user;
    }

    public static JSONObject userIdToJSON(User user) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(COLUMN_NAME, user.getName());
        return jsonObject;
    }

    public static JSONObject userIdsToJSONArray(ArrayList<User> users) {
        JSONArray userArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        try {
            for (User user : users) {
                userArray.put(userIdToJSON(user));
            }

            jsonObject.put("", userArray);
        } catch (Exception e) {

        }

        return jsonObject;
    }*/
}