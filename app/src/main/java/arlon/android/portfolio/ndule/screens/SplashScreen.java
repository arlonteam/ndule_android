package arlon.android.portfolio.ndule.screens;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.objects.User;

/**
 * Created by Arlon Mukeba on 1/12/2018.
 */

public class SplashScreen extends AppCompatActivity {

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            User user = Ndule.getUser();

            /*if ((user != null) && (user.getLoginStatus().equalsIgnoreCase(
                User.LOGIN_STATUS_LOGGED_IN))) {

                moveToHome();
            } else {
                moveToLogin();
            }*/

            moveToHome();
        }
    };

    private ImageView mIvSplashLogo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        mIvSplashLogo = (ImageView) findViewById(R.id.ivSplashLogo);
//        mIvSplashLogo.setOnClickListener(new SplashLogoClicked());

        animateLogo();
    }

    private void animateLogo() {
//        mIvSplashLogo.setAlpha(0.0f);
        Animation randomAnimation = AnimationUtils.loadAnimation(this, R.anim.splash_logo);
        randomAnimation.setFillAfter(true);
        randomAnimation.setAnimationListener(new MapAnimation());
        mIvSplashLogo.startAnimation(randomAnimation);
    }

    private void moveToLogin() {
        Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(LoginScreen.SKIP_OPTION, true);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                this, mIvSplashLogo, "image_name");

            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    private void moveToHome() {
        startActivity(new Intent(SplashScreen.this, HomeScreen.class));
    }

    private class MapAnimation implements Animation.AnimationListener {

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
//            mIvSplashLogo.setAlpha(1.0f);
            new Handler().postDelayed(mRunnable, 2500);
        }
    }

    private class SplashLogoClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            moveToLogin();
        }
    }
}