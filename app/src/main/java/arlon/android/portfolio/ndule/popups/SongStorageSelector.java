package arlon.android.portfolio.ndule.popups;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.IdRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import arlon.android.portfolio.ndule.R;

/**
 * Created by Arlon Mukeba on 3/5/2018.
 */

public class SongStorageSelector extends PopupWindow {

    private RadioGroup mRadioGroup = null;
    private RelativeLayout mRlMemoryDetails = null;
    private TextView mTvStorage = null;
    private TextView mTvSize = null;
    private TextView mTvSelect = null;
    private TextView mTvCancel = null;
    private TextView mTvMemoryStatus = null;
    private Context mContext = null;
    private File mSelectedOption = null;
    private StorageChoiceListener mChoiceListener = null;
    private int mSongSize = 0;

    private ArrayList<String> mOptions = new ArrayList<String>() {
        {
            add("Internal Memory");
            add("Memory Card");
        }
    };

    public SongStorageSelector(Context context, int songSize, StorageChoiceListener choiceListener) {
        super(context);

        mContext = context;
        mSongSize = songSize;
        mChoiceListener = choiceListener;

        setPopup();
        setViews();
        setOptions();
    }

    private void setPopup() {
        setBackgroundDrawable(null);
        /*setAnimationStyle(R.anim.popup_anim);*/
        setOutsideTouchable(true);
        setFocusable(true);
        setWidth(/*350*/LinearLayout.LayoutParams.WRAP_CONTENT);
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private LayoutInflater getInflater() {
        return (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void setViews() {
        LayoutInflater inflater = getInflater();

        LinearLayout choiceOptionsLayout = (LinearLayout) inflater.inflate(
            R.layout.layout_song_location_selector, null, false);

        setContentView(choiceOptionsLayout);

        mRadioGroup = (RadioGroup) choiceOptionsLayout.findViewById(R.id.rgOptions);
        mRadioGroup.setOnCheckedChangeListener(new StorageOptionChecked());

        mRlMemoryDetails = (RelativeLayout) choiceOptionsLayout.findViewById(R.id.rlMemoryDetails);

        mTvStorage = (TextView) choiceOptionsLayout.findViewById(R.id.tvLocation);
        mTvSize = (TextView) choiceOptionsLayout.findViewById(R.id.tvSize);
        mTvMemoryStatus = (TextView) choiceOptionsLayout.findViewById(R.id.tvMemoryStatus);

        mTvSelect = (TextView) choiceOptionsLayout.findViewById(R.id.btnSelect);
        mTvSelect.setOnClickListener(new PopupBtnClicked());

        mTvCancel = (TextView) choiceOptionsLayout.findViewById(R.id.btnCancel);
        mTvCancel.setOnClickListener(new PopupBtnClicked());
    }

    public void setOptions() {
        LayoutInflater inflater = getInflater();
        RadioButton  radioBtn = null;
        int id = -1;

        for (int i = 0; i < mOptions.size(); i++) {
            id = new Random().nextInt(10000000);

            radioBtn = (RadioButton) inflater.inflate(R.layout.layout_choice, null);
            radioBtn.setId(id);
            radioBtn.setText(mOptions.get(i));
            mRadioGroup.addView(radioBtn);

            if (i == 0) {
                mRadioGroup.check(id);
//                setMemoryDetails(id);
            }
        }
    }

    private class StorageOptionChecked implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            mRlMemoryDetails.setVisibility(View.VISIBLE);
            try {
                setMemoryDetails(checkedId);
            } catch (Exception e) {

            }
        }
    }

    private void setMemoryDetails(int checkedId) {
        RadioButton radioButton = (RadioButton) mRadioGroup.findViewById(checkedId);
        String storageName = radioButton.getText().toString();
        mTvStorage.setText(storageName);

        int size = -1;
        String memoryStats = "";
        int indexOf = mOptions.indexOf(storageName);

        if (indexOf == 0) {
            mSelectedOption = Environment.getDataDirectory();
            size = getAvailableInternalMemory();

            memoryStats = new StringBuilder("").append(getMemoryInMb(size)).append("/").append(
                    getMemoryInMb(getTotalInternalMemory())).append(" MB").toString();
        } else {
            size = getAvailableExternalMemory();

            if (size != (-1)) {
                mSelectedOption = Environment.getExternalStorageDirectory();

                memoryStats = new StringBuilder("").append(getMemoryInMb(size)).append("/").append(
                    getMemoryInMb(getTotalExternalMemory())).append(" MB").toString();

                mTvSize.setTextColor(mContext.getResources().getColor(R.color.white));
            } else {
                mSelectedOption = null;
                memoryStats = "UNAVAILABLE";
                mTvSize.setTextColor(mContext.getResources().getColor(R.color.red));
            }
        }

        if ((size < 0) || (size < mSongSize)) {
            mTvMemoryStatus.setVisibility(View.VISIBLE);

            String storageErrorMsg = mContext.getResources().getString(R.string.sls_space_error);
            storageErrorMsg = String.format(storageErrorMsg, (getMemoryInMb(size - mSongSize)));
            
            mTvMemoryStatus.setText(storageErrorMsg);
        } else {
            mTvMemoryStatus.setVisibility(View.GONE);
        }

        mTvSize.setText(memoryStats);
    }

    private String getMemoryInMb(int memory) {
        return new StringBuilder("").append(memory / (1024 * 1024)).toString();
    }

    private int getAvailableInternalMemory() {
        return getAvailableMemory(Environment.getDataDirectory());
    }

    private int getTotalInternalMemory() {
        return getTotalMemory(Environment.getDataDirectory());
    }

    private int getAvailableExternalMemory() {
        int size = -1;

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            size = getAvailableMemory(Environment.getExternalStorageDirectory());
        }

        return size;
    }

    private int getTotalExternalMemory() {
        int size = -1;

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            size = getTotalMemory(Environment.getExternalStorageDirectory());
        }

        return size;
    }

    private int getAvailableMemory(File path) {
        StatFs stats = new StatFs(path.getPath());
        int blockSize = stats.getBlockSize();
        int availableBlocs = stats.getAvailableBlocks();
        return blockSize * availableBlocs;
    }

    private int getTotalMemory(File path) {
        StatFs stats = new StatFs(path.getPath());
        int blockSize = stats.getBlockSize();
        int totalBlocs = stats.getBlockCount();
        return blockSize * totalBlocs;
    }

    public interface StorageChoiceListener {

        public void optionSelected(File option);
    }

    private class PopupBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            dismiss();

            if (view.getId() == R.id.btnSelect) {
                mChoiceListener.optionSelected(mSelectedOption);
            }
        }
    }
}