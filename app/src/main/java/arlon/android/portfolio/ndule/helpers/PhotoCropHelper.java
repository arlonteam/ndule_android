package arlon.android.portfolio.ndule.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
//import com.theartofdev.edmodo.cropper.CropImageView;
import arlon.android.portfolio.ndule.R;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public abstract class PhotoCropHelper implements /*CropImageView.OnCropImageCompleteListener,*/
        MaterialDialog.SingleButtonCallback {

    private Context mContext = null;
//    private CropImageView mCropIv = null;
    private Bitmap mBitmap = null;
    private String mPath = null;

    public PhotoCropHelper(Context context) {
        mContext = context;
    }

    public void cropImage(Bitmap bitmap, String path) {
        mBitmap = bitmap;
        mPath = path;

        RelativeLayout feedItemLayout = /*(RelativeLayout) LayoutInflater.from(mContext)
            .inflate(R.layout.layout_pix_crop, null, false)*/ null;

        MaterialDialog.Builder builder = DialogHelper.buildImageCropDialog(mContext, feedItemLayout,
            this);

        /*mCropIv = ((CropImageView) feedItemLayout.findViewById(R.id.civProfilePix));
        mCropIv.setImageBitmap(mBitmap);
        mCropIv.setOnCropImageCompleteListener(this);*/

        builder.show();
    }

    public void onClick(MaterialDialog dialog, DialogAction which) {
        if (which == DialogAction.POSITIVE) {
//            mCropIv.getCroppedImageAsync();
        } else {
            handleFinalImage(mBitmap, mPath);
        }
    }

    /*public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        if (result.getError() == null) {
            mBitmap = result.getBitmap();
            PixHelper.saveImageToMemory(mBitmap, mPath);
        }

        handleFinalImage(mBitmap, mPath);
    }*/

    public abstract void handleFinalImage(Bitmap finalImage, String path);
}