package arlon.android.portfolio.ndule.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import java.util.Date;
import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.helpers.DateHelper;
import arlon.android.portfolio.ndule.objects.User;

/**
 * Created by Arlon Mukeba on 1/22/2018.
 */

public class ProfileViewFragment extends ProfileBaseFragment {

    public static final String TAG = "PROFILE_VIEW_FRAGMENT";

    private User mUser = null;

    private TextView mTvNames = null;
    private TextView mTvEmail = null;
    private TextView mTvCellNbr = null;
    private TextView mTvCredit = null;
    private TextView mTvCreditExpiryDate = null;

    public ProfileViewFragment() {

    }

    public static ProfileViewFragment newInstance(Hashtable<String, Object> params) {
        ProfileViewFragment profileViewFragment = new ProfileViewFragment();
        return profileViewFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("PERMISSIONREQUESTED", "ONCREATECALLED");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_profile_view;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        populateViews();
    }

    private void setupViews() {
        mTvNames = (TextView) getView().findViewById(R.id.tvNames);
        mTvEmail = (TextView) getView().findViewById(R.id.tvEmail);
        mTvCellNbr = (TextView) getView().findViewById(R.id.tvCellNbr);
        mTvCredit = (TextView) getView().findViewById(R.id.tvBalance);
        mTvCreditExpiryDate = (TextView) getView().findViewById(R.id.tvExpiryDate);
    }

    protected void populateViews() {
        mUser = Ndule.getUser();

        Log.d("USER_DETAILS", (mUser == null) ? "NULL" : mUser.toString());

        if (mUser == null) return;

        String names = new StringBuffer(mUser.getFirstName()).append(" ").append(mUser.getSurname())
            .toString();

        mTvNames.setText(names);
        mTvEmail.setText(mUser.getEmail());
        mTvCellNbr.setText(mUser.getCellNbr());

        String expiryDateString = mUser.getCreditExpiryDate();

        mTvCreditExpiryDate.setText("Expiry Date: " + expiryDateString);

        Date expiryDate = DateHelper.dateFromString(expiryDateString);

        mTvCredit.setText("Balance: R" + mUser.getCredit() + (new Date().after(expiryDate) ?
            " (Expired!)" : ""));

        super.populateViews();
    }

    public int getFabIconResId() {
        return -1;
    }

    protected int getMenuResId() {
        return -1;
    }
}