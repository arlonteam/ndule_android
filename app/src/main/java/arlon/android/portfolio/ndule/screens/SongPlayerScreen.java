package arlon.android.portfolio.ndule.screens;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chibde.visualizer.LineBarVisualizer;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.adapters.CommentListAdapter;
import arlon.android.portfolio.ndule.adapters.PlaylistBlockAdapter;
import arlon.android.portfolio.ndule.helpers.SongHelper;
import arlon.android.portfolio.ndule.helpers.DialogHelper;
import arlon.android.portfolio.ndule.helpers.PermissionHelper;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Comment;
import arlon.android.portfolio.ndule.objects.Payment;
import arlon.android.portfolio.ndule.objects.Song;
import arlon.android.portfolio.ndule.objects.User;
import arlon.android.portfolio.ndule.popups.SongStorageSelector;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Arlon Mukeba on 1/14/2018.
 */

public class SongPlayerScreen extends AppCompatActivity {

    public static final String PLAYLIST_PARAM = "PLAYLIST_PARAM";
    public static final String ARTIST_PARAM = "ARTIST_PARAM";
    public static final String SONG_PARAM = "SONG_PARAM";
    public static final String SONG_SCREEN = "SONG_SCREEN";
    public static final int AUDIO_PERM_REQUEST_CODE = 123;
    public static final int LOGIN_REQUEST_CODE = 255;
    private enum REPEAT_STATES { NO_REPEAT, REPEAT_ONE, REPEAT_ALL };
    public enum SCREEN_MODES { DEFAULT, PURCHASE, COMMENT };

    private RelativeLayout mRlTopView = null;
    private LinearLayout mLlPlayer = null;
    private LinearLayout mLlPurchase = null;
    private LinearLayout mLlComments = null;
    private AppBarLayout mAbParentBar = null;
    private CollapsingToolbarLayout mCtbLayout = null;
    private ImageView mIvCover = null;
    private ImageView mIvLike = null;
    private TextView mTvTitle = null;
    private CircleImageView mCivImage = null;
    private TextView mTvArtistName = null;
    private TextView mTvAlbumTitle = null;
    private TextView mTvListCounter = null;
    private TextView mTvTimePlayed = null;
    private TextView mTvTotalTime = null;
    private ImageView mIvActivePrevBtn = null;
    private ImageView mIvPrevBtn = null;
    private ImageView mIvPlayBtn = null;
    private ImageView mIvActiveNextBtn = null;
    private ImageView mIvNextBtn = null;
    private ImageView mIvLoopBtn = null;
    private SeekBar mSbProgress = null;
    private MediaPlayer mMediaPlayer = null;
    private LineBarVisualizer mLineVisualizer = null;
    private FloatingActionButton mFabMenu = null;
    private SeekBarThread mSeekBarThread = null;
    private RecyclerView mRvPlayList = null;
    private EditText mEdtComment = null;
    private ImageView mIvCancel = null;
    private ImageView mIvSubmit = null;
    private RelativeLayout mRlCommentList = null;
    private CommentAdapter mCommentListAdapter = null;
    private RecyclerView mRvComments = null;
    private RelativeLayout mRlReplyBubble = null;
    private TextView mTvReplyUser = null;
    private TextView mTvReplyText = null;
    private ImageView mIvReplyCancel = null;
    private MaterialDialog mDialog = null;
    private TextView mTvPayment = null;
    private TextView mTvAmount = null;
    private RadioGroup mRgPayment = null;
    private RelativeLayout mRlVoucher = null;
    private RelativeLayout mRlVoucher1 = null;
    private EditText mEdtVoucher = null;
    private ImageView mIvVoucher = null;
    private ImageView mIvVoucher2 = null;
    private RelativeLayout mRlVoucher2 = null;
    private TextView mTvVoucher = null;
    private ImageView mIvVoucher3 = null;
    private RelativeLayout mRlBalance = null;
    private TextView mTvBalance = null;
    private TextView mTvBalanceStatus = null;
    private RelativeLayout mRlRef = null;
    private TextView mTvRef = null;
    private TextView mTvNbr = null;
    private RelativeLayout mRlBtns = null;
    private TextView mTvProceed = null;
    private TextView mTvCancel = null;

    private ArrayList<Song> mSongs = new ArrayList<Song>();
    private Artist mArtist = null;
    private Song mSong = null;
    private Comment mComment = null;
    private boolean mIsPlayList = false;
    private int mPlayerPosition = 0;
    private int mActionId = R.id.action_like;
    private REPEAT_STATES mRepeatState = REPEAT_STATES.NO_REPEAT;
    private SCREEN_MODES mScreenMode = SCREEN_MODES.DEFAULT;

    private boolean mIsVisualizerReady = false;
    private boolean mIsPlayerPrepared = false;
    private boolean mIsPlayerPlaying = false;
    private boolean mAllowedStreaming = false;
    private byte[] mSongBytes = null;

    private Payment mPayment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_song_player_screen);

        getParameters();
        setupViews();
//        populateViews();
        setupPlayerPermissions();
    }

    @Override
    protected void onPause() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }

            setupPlayerControls();
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d("DIRECTORY_LIST", "HERE2");
//        stopPlayer(1);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("DIRECTORY_LIST", "HERE1");
        stopPlayer(2);
        super.onDestroy();
    }

    private void getParameters() {
        mArtist = getIntent().getParcelableExtra(ARTIST_PARAM);
        mSong = getIntent().getParcelableExtra(SONG_PARAM);
        mIsPlayList = getIntent().getBooleanExtra(PLAYLIST_PARAM, false);

        if (mIsPlayList) {
            Select select = Select.from(Song.class);

            if (mArtist != null) {
                select = select.where(Condition.prop(Song.COLUMN_ARTIST_ID).eq(
                        mArtist.getArtistId()));
            }

            mSongs = new ArrayList<Song>(select.list());
            mSong = mSongs.get(0);
        }

        mSong = getSongById();
    }

    private void setupViews() {
        /*mAbParentBar = (AppBarLayout) findViewById(R.id.abParentBar);
        mCtbLayout = (CollapsingToolbarLayout) findViewById(R.id.ctbParentBar);*/
        mFabMenu = (FloatingActionButton) findViewById(R.id.fabMenu);
        mFabMenu.setOnClickListener(new FabClicked(this));

        mRlTopView = (RelativeLayout) findViewById(R.id.rlTopView);
        mIvCover = (ImageView) mRlTopView.findViewById(R.id.ivCover);
        mIvLike = (ImageView) mRlTopView.findViewById(R.id.ivLike);
        mRvPlayList = (RecyclerView) mRlTopView.findViewById(R.id.rvPlaylist);

        mLlPlayer = (LinearLayout) findViewById(R.id.llPlayer);
        mCivImage = (CircleImageView) mLlPlayer.findViewById(R.id.civImage);
        mTvTitle = (TextView) mLlPlayer.findViewById(R.id.tvTitle);
        mTvArtistName = (TextView) mLlPlayer.findViewById(R.id.tvArtistName);
        mTvAlbumTitle = (TextView) mLlPlayer.findViewById(R.id.tvAlbumTitle);
        mTvListCounter = (TextView) mLlPlayer.findViewById(R.id.tvListCounter);
        mTvTimePlayed = (TextView) mLlPlayer.findViewById(R.id.tvTimePlayed);
        mTvTotalTime = (TextView) mLlPlayer.findViewById(R.id.tvTotalTime);
        mIvActivePrevBtn = (ImageView) mLlPlayer.findViewById(R.id.ivActivePrevBtn);
        mIvPrevBtn = (ImageView) mLlPlayer.findViewById(R.id.ivPrevBtn);
        mIvPlayBtn = (ImageView) mLlPlayer.findViewById(R.id.ivPlayBtn);
        mIvActiveNextBtn = (ImageView) mLlPlayer.findViewById(R.id.ivActiveNextBtn);
        mIvNextBtn = (ImageView) mLlPlayer.findViewById(R.id.ivNextBtn);
        mIvLoopBtn = (ImageView) mLlPlayer.findViewById(R.id.ivLoopBtn);
        mSbProgress = (SeekBar) mLlPlayer.findViewById(R.id.sbProgress);

        mLineVisualizer = (LineBarVisualizer) mLlPlayer.findViewById(R.id.visualizer);
        mLineVisualizer.setColor(getResources().getColor(R.color.blue));
        mLineVisualizer.setDensity(200);

        /*mAbParentBar.setExpanded(true, true);
        mAbParentBar.setEnabled(false);*/

        mLlPurchase = (LinearLayout) findViewById(R.id.llPurchase);
        mTvAmount = (TextView) mLlPurchase.findViewById(R.id.tvAmount);

        mTvPayment = (TextView) mLlPurchase.findViewById(R.id.tvPayment);
        mTvPayment.setOnClickListener(new PaymentToggleClicked());

        mRgPayment = (RadioGroup) mLlPurchase.findViewById(R.id.rgPayment);
        mRgPayment.setOnCheckedChangeListener(new PaymentOptionChecked());

        mRlVoucher = (RelativeLayout) mLlPurchase.findViewById(R.id.rlVoucher);

        mRlVoucher1 = (RelativeLayout) mRlVoucher.findViewById(R.id.rlVoucher1);
        mEdtVoucher = (EditText) mRlVoucher1.findViewById(R.id.edtVoucher);
        mIvVoucher = (ImageView) mRlVoucher1.findViewById(R.id.ivVoucher);
        mIvVoucher.setOnClickListener(new VoucherBtnClicked());
        mIvVoucher2 = (ImageView) mRlVoucher1.findViewById(R.id.ivVoucher2);
        mIvVoucher2.setOnClickListener(new VoucherBtnClicked());

        mRlVoucher2 = (RelativeLayout) mRlVoucher.findViewById(R.id.rlVoucher2);
        mTvVoucher = (TextView) mRlVoucher2.findViewById(R.id.tvVoucher);
        mIvVoucher3 = (ImageView) mRlVoucher2.findViewById(R.id.ivVoucher3);
        mIvVoucher3.setOnClickListener(new VoucherBtnClicked());

        mRlBalance = (RelativeLayout) mLlPurchase.findViewById(R.id.rlBalance);
        mTvBalance = (TextView) mRlBalance.findViewById(R.id.tvBalance);
        mTvBalanceStatus = (TextView) mRlBalance.findViewById(R.id.tvBalanceStatus);

        mRlRef = (RelativeLayout) mLlPurchase.findViewById(R.id.rlRef);
        mTvRef = (TextView) mRlRef.findViewById(R.id.tvRef);
        mTvNbr = (TextView) mRlRef.findViewById(R.id.tvNbr);

        mRlBtns = (RelativeLayout) mLlPurchase.findViewById(R.id.rlBtns);
        mTvProceed = (TextView) mRlBtns.findViewById(R.id.tvProceed);
        mTvProceed.setOnClickListener(new VoucherBtnClicked());
        mTvCancel = (TextView) mRlBtns.findViewById(R.id.tvCancel);
        mTvCancel.setOnClickListener(new VoucherBtnClicked());

        mLlComments = (LinearLayout) findViewById(R.id.llComments);

        mRlReplyBubble = (RelativeLayout) mLlComments.findViewById(R.id.rlReplyBubble);
        mTvReplyUser = (TextView) mRlReplyBubble.findViewById(R.id.tvReplyUser);
        mTvReplyText = (TextView) mRlReplyBubble.findViewById(R.id.tvReplyText);
        mIvReplyCancel = (ImageView) mRlReplyBubble.findViewById(R.id.ivReplyCancel);
        mIvReplyCancel.setOnClickListener(new CancelBtnClicked());

        mEdtComment = (EditText) mLlComments.findViewById(R.id.edtComment);
        mEdtComment.setOnFocusChangeListener(new EditTextFocusChanged());
        mEdtComment.setOnClickListener(new CommentEdtClicked());
        mIvCancel = (ImageView) mLlComments.findViewById(R.id.ivCancel);
        mIvCancel.setOnClickListener(new CancelBtnClicked());
        mIvSubmit = (ImageView) mLlComments.findViewById(R.id.ivSubmit);
        mIvSubmit.setOnClickListener(new SubmitBtnClicked());

        mRlCommentList = (RelativeLayout) mLlComments.findViewById(R.id.rlCommentList);

        mCommentListAdapter = new CommentAdapter(this);

        mRvComments = (RecyclerView) findViewById(R.id.rvComments);
        mRvComments.setAdapter(mCommentListAdapter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRvComments.setLayoutManager(mLayoutManager);
        mRvComments.setItemAnimator(new DefaultItemAnimator());
    }

    private void resetPurchaseMode() {
        mTvPayment.setText("Click to select");
        mRgPayment.setVisibility(View.GONE);
        resetPurchaseView();
    }

    private void resetPurchaseView() {
        mRlVoucher.setVisibility(View.GONE);
        mRlVoucher1.setVisibility(View.GONE);
        mRlVoucher2.setVisibility(View.GONE);
        mRlBalance.setVisibility(View.GONE);
        mRlRef.setVisibility(View.GONE);
        mRlBtns.setVisibility(View.GONE);
    }

    private void setPaymentOption(int checkedId) {
        resetPurchaseView();

        String paymentMethod = "";

        switch (checkedId) {
            case R.id.rbPaymentFirst:
                switchVoucherEdit();
                paymentMethod = Payment.PAYMENT_METHOD_VOUCHER;
                break;

            case R.id.rbPaymentSecond:
                switchAccPayment();
                paymentMethod = Payment.PAYMENT_METHOD_ACCOUNT;
                break;

            case R.id.rbPaymentThird:
                switchReference();
                paymentMethod = Payment.PAYMENT_METHOD_AIRTEL_MONEY;
                break;

            case R.id.rbPaymentFourth:
                switchReference();
                paymentMethod = Payment.PAYMENT_METHOD_MPESA;
                break;

            case R.id.rbPaymentFifth:
                switchReference();
                paymentMethod = Payment.PAYMENT_METHOD_ORANGE_MONEY;
                break;
        }

        mTvPayment.setText(paymentMethod);
        mPayment.setMethod(paymentMethod);
    }

    private void switchAccPayment() {
        mRlBalance.setVisibility(View.VISIBLE);
        double credit = Ndule.getUser().getCredit();
        mTvBalance.setText("R " + String.format("%02f", credit));

        int balanceMsg = R.string.ss_funds_deduction_msg;
        int balanceMsgColor = R.color.gray;

        if (credit < mSong.getPrice()) {
            balanceMsg = R.string.ss_insufficient_funds_msg;
            balanceMsgColor = R.color.red;
        } else {
        }

        mRlBtns.setVisibility(View.VISIBLE);

        mTvBalanceStatus.setText(balanceMsg);
        mTvBalanceStatus.setTextColor(getResources().getColor(balanceMsgColor));
    }

    private void switchReference() {
        mRlRef.setVisibility(View.VISIBLE);
        /*mTvRef.setText("");
        mTvNbr.setText("");*/
        mRlBtns.setVisibility(View.VISIBLE);
    }

    private void switchVoucherEdit() {
        mRlVoucher.setVisibility(View.VISIBLE);
        mRlVoucher1.setVisibility(View.VISIBLE);
        mEdtVoucher.setText("");
        mRlVoucher2.setVisibility(View.GONE);
        mRlBtns.setVisibility(View.GONE);
    }

    private class VoucherBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (view.equals(mIvVoucher)) {
                String voucher = mEdtVoucher.getText().toString();

                if (voucher.isEmpty()) {
                    DialogHelper.showMessage(SongPlayerScreen.this, R.string.ss_dialog_title,
                            R.string.ss_dialog_empty_voucher);

                    return;
                }

                mTvVoucher.setText(voucher);

                switchVoucherView();
            } else if (view.equals(mIvVoucher2)) {
                switchVoucherView();
            } else if (view.equals(mIvVoucher3)) {
                switchVoucherEdit();
            } else if (view.equals(mTvProceed)) {
                selectSongStorage();
            } else if (view.equals(mTvCancel)) {
                switchDefaultMode();
            }
        }
    }

    private void selectSongStorage() {
        new SongStorageSelector(this, mSongBytes.length, new StorageSelector()).showAtLocation(
            mLlPurchase, Gravity.CENTER, 0, 0);
    }

    private void proceedToPayment(File storage) {
        User user = Ndule.getUser();

        try {
            String filename = mSong.getTitle().replaceAll(" ", "_");

            String password = new StringBuilder(user.getUsername()).append("_").append(
                getString(R.string.app_name)).append("_").append(user.getPassword()).toString();

            filename = SongHelper.saveDownloadedAudio(storage, filename, mSongBytes, password);

            Log.d("SAVING_FILE_ABSOLUTE", "2:" + filename);

            mSong.setSavedUrl(filename);
        } catch (Exception e) {
            mPayment = new Payment();

            Log.d("SAVING_FILE_EXCEPTION", "" + e.getMessage());
            return;
        }

        mPayment.setAmount(mSong.getPrice());

        if ((mPayment.getMethod().equalsIgnoreCase(Payment.PAYMENT_METHOD_VOUCHER)) ||
                (mPayment.getMethod().equalsIgnoreCase(Payment.PAYMENT_METHOD_ACCOUNT))) {
            mPayment.setStatus(Payment.COLUMN_STATUS_PAID);
        }

        if (mPayment.getMethod().equalsIgnoreCase(Payment.PAYMENT_METHOD_ACCOUNT)) {
            user.setCredit(user.getCredit() - mSong.getPrice());
            user.save();
            Ndule.setUser(user);
        }

        mPayment.setUserId(user.getEmail());
        mPayment.setSongId(mSong.getSongId());
        mPayment.save();

        mSong.setOwnershipStatus("YES");
        mSong.save();

        switchDefaultMode();
    }

    private class StorageSelector implements SongStorageSelector.StorageChoiceListener {

        @Override
        public void optionSelected(File option) {
            proceedToPayment(option);
        }
    }

    private void switchVoucherView() {
        resetPurchaseView();

        mRlVoucher.setVisibility(View.VISIBLE);
        mRlVoucher1.setVisibility(View.GONE);
        mRlVoucher2.setVisibility(View.VISIBLE);
        mRlBtns.setVisibility(View.VISIBLE);
    }

    private class PaymentOptionChecked implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            mRgPayment.setVisibility(View.GONE);

            setPaymentOption(checkedId);
        }
    }

    private class PaymentToggleClicked implements View.OnClickListener {

        @Override
        public void onClick(android.view.View v) {
            resetPurchaseView();

            mRgPayment.setVisibility((mRgPayment.getVisibility() == View.VISIBLE) ? View.GONE :
                    View.VISIBLE);
        }
    }

    private void setPlayList() {
        if (!mIsPlayList) return;

        mTvListCounter.setVisibility(View.VISIBLE);

        int index = mSongs.indexOf(mSong);
        index++;

        ArrayList<Object> blocks = new ArrayList<Object>();

        for (int i = 0; i < (Math.min(5, mSongs.size())); i++) {
            blocks.add(mSongs.get(((index + i) % mSongs.size())));
        }

        if (mSongs.size() > 5) {
            blocks.add("+" + (mSongs.size() - 5) + "\nSongs");
        }

        mRvPlayList.setHasFixedSize(true);
        mRvPlayList.setLayoutManager(new GridLayoutManager(this, 2));
        mRvPlayList.setAdapter(new PlaylistBlockAdapter(this, blocks, -1, -1));
    }

    private void loadComments() {
        ArrayList<Comment> comments = (ArrayList) Select.from(Comment.class).where(
                Condition.prop(Comment.COLUMN_SONG_ID).eq(mSong.getSongId()))
                .and(Condition.prop(Comment.COLUMN_PARENT_ID).eq("")).list();

        mCommentListAdapter.setComments(comments);
        mCommentListAdapter.notifyDataSetChanged();

        if (comments.isEmpty()) {
            mRvComments.setVisibility(View.GONE);
        } else {
            mRvComments.setVisibility(View.VISIBLE);
        }
    }

    private class CommentEdtClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
//            toggleScreenState();
        }
    }

    private class CancelBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            resetCommentEdt();
        }
    }

    private void resetCommentEdt() {
        mEdtComment.setText("");
        initComment();
    }

    private class SubmitBtnClicked implements View.OnClickListener {

        private String mCommentTxt = null;

        @Override
        public void onClick(View v) {
            mCommentTxt = mEdtComment.getText().toString();

            if ((mCommentTxt == null) || (mCommentTxt.isEmpty())) return;

            saveComment(mCommentTxt);

            resetCommentEdt();
        }
    }

    private void saveComment(String commentTxt) {
        mComment.setContent(commentTxt);
        mComment.save();

        Log.d("SAVING", mComment.toString());

        loadComments();

        mRvComments.scrollToPosition(mRvComments.getLayoutManager().getHeight());

        initComment();
    }

    private class EditTextFocusChanged implements View.OnFocusChangeListener {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
//            toggleScreenState();
        }
    }

    private void setupPlayerPermissions() {
        String[] permissions = PermissionHelper.checkPermissions(this, getPermissions());

        if ((permissions.length != 0) && (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)) {
            getPermissions(permissions);
        } else if (permissions.length == 0) {
            setSongPlayer();
        }
    }

    private void setupVisualizer() {
//        mLineVisualizer.setPlayer(mMediaPlayer);
        mIsVisualizerReady = true;
    }

    private void togglePlayer() {
        if ((mMediaPlayer != null) && (mMediaPlayer.isPlaying())) {
            pausePlayer();
        } else {
            startPlayer();
        }

//        populateViews();
        setupPlayerControls();
    }

    private void setupPlayerControls() {
        mIvPlayBtn.setImageResource((mMediaPlayer.isPlaying()) ? R.drawable.ic_pause_btn :
                R.drawable.ic_play_btn);

//        mLineVisualizer.setVisibility((mMediaPlayer.isPlaying()) ? View.VISIBLE : View.INVISIBLE);
    }

    private void showPlayerProgress() {
        mTvTimePlayed.setText(lengthToTime(mPlayerPosition));
        mTvTotalTime.setText(lengthToTime(mMediaPlayer.getDuration()));
        mSbProgress.setProgress(mPlayerPosition);
    }

    private String lengthToTime(int length) {
        length /= 1000;
        String minutes = String.format("%02d", (length / 60));
        String seconds = String.format("%02d", (length % 60));

        return new StringBuilder(minutes).append(":").append(seconds).toString();
    }

    private int seekProgressToTime() {
        double progress = (double) mSbProgress.getProgress();
        double max = (double) mSbProgress.getMax();
        double duration = (double) mMediaPlayer.getDuration();

        return (int) ((progress * max) / duration);
    }

    private int timeToSeekProgress() {
        double progress = (double) mMediaPlayer.getCurrentPosition();
        double duration = (double) mMediaPlayer.getDuration();
        double max = (double) mSbProgress.getMax();

        return (int) ((progress * duration) / max);
    }

    private void updatePlayerStatus() {
        showPlayerProgress();
    }

    @TargetApi(23)
    private void getPermissions(String[] permissions) {
        requestPermissions(permissions, AUDIO_PERM_REQUEST_CODE);
    }

    private ArrayList<String> getPermissions() {
        return new ArrayList<String>() {
            {
                add(Manifest.permission.RECORD_AUDIO);
                add(Manifest.permission.MODIFY_AUDIO_SETTINGS);
                add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        };
    }

    private void populateViews() {
        setSongImage();
        setPlayList();
        setSongDetails();
        loadComments();
        initComment();
    }

    private void resetProgress() {
        mTvTimePlayed.setText(lengthToTime(0));
        mTvTotalTime.setText(lengthToTime(0));
        mSbProgress.setProgress(mPlayerPosition);
    }

    private void setSongPlayer() {
        SongHelper.listTempDir(this);

        Log.d("SONG_DETAILS", mSong.toString());

        populateViews();

        if ((!mSong.isLocal()) && ((!SongHelper.wasSongSaved(mSong)) &&
                (!SongHelper.wasSongTempSaved(this, mSong)))) {

            askSongData();
            return;
        }

        setPlayerBtns();

        try {
            setPlayerFromBytes();
        } catch (Exception exc) {
            Log.d("SAVED_PASSWORD", "EXCEPTION: " + exc.getMessage());
        }
    }

    private void askSongData() {
        if (mAllowedStreaming) {
            retrieveSongData();
            return;
        }

        DialogHelper.twoButtonDialog(this, R.string.ss_title, R.string.ss_song_data_request,
                R.string.btn_proceed, R.string.btn_cancel, new SongRequestDialog()).show();
    }

    private class SongRequestDialog extends DialogHelper.Listener {

        @Override
        public void onPositive(MaterialDialog dialog) {
            /*if (!NetworkHelper.googleServicesAvailable()) {
                DialogHelper.showMessage(SongPlayerScreen.this, "", "");
                return;
            }*/

            mAllowedStreaming = true;

            retrieveSongData();
        }
    }

    private void retrieveSongData() {
        String songUrl = new StringBuffer(Ndule.SONG_URL).append(mSong.getOriginalUrl()).append(".mp3")
                .toString();

        new SongDataDownloader().execute(songUrl);
    }

    private void setPlayerFromBytes() throws Exception {
        if (mMediaPlayer == null) mMediaPlayer = new MediaPlayer();

        mMediaPlayer.reset();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        String name = mSong.getOriginalUrl();

        if (!mSong.isLocal()) {
            User user = Ndule.getUser();

            if ((SongHelper.wasSongSaved(mSong)) && ((user != null) && (user.isLoggedIn()))) {
                String password = new StringBuilder(user.getUsername()).append("_").append(
                    getString(R.string.app_name)).append("_").append(user.getPassword()).toString();

                mSongBytes = SongHelper.readDownloadedAudio(mSong.getSavedUrl(), password);

                Log.d("SAVED_PASSWORD", password + " " + mSong.getSavedUrl());
            }

            name = mSong.getTitle().replaceAll(" ", "_");
            mMediaPlayer.setDataSource(SongHelper.getTempAudio(this, name, mSongBytes));
        } else {
            mMediaPlayer.setDataSource(name);
        }

        mMediaPlayer.setOnPreparedListener(new MediaPreparedListener());
        mMediaPlayer.prepare();
    }

    private void setSongImage() {
        String songImage = Ndule.SONG_IMAGE_URL + mSong.getThumbnail() + ".jpg";

        Glide.with(this).load(songImage).asBitmap().override(1000, 1000)
                .error(R.drawable.ic_treble_key)
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                .into(new SimpleTarget<Bitmap>(1000, 1000) {
                          @Override
                          public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                              mIvCover.setImageBitmap(bitmap);
                          }
                      }
                );

        Glide.with(this).load(songImage).asBitmap().override(100, 100)
                .error(R.drawable.ic_treble_key)
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                .into(new SimpleTarget<Bitmap>(100, 100) {
                          @Override
                          public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                              mCivImage.setImageBitmap(bitmap);
                          }
                      }
                );

        mIvLike.setVisibility(mSong.isLiked() ? View.VISIBLE : View.GONE);
        mTvTitle.setText(mSong.getTitle());
    }

    private void setPlayerDetails(MediaPlayer mediaPlayer) {
        mMediaPlayer = mediaPlayer;
        mMediaPlayer.seekTo(0);
        mMediaPlayer.setOnCompletionListener(new MediaCompletionListener());

        setupVisualizer();
        setupPlayerControls();
        setPlayerProgress();
        showPlayerProgress();
    }

    private void setSongDetails() {
        mTvAlbumTitle.setText(mSong.getAlbumTitle());
        mTvArtistName.setText(mSong.getArtist().getName());
        mTvAmount.setText("R " + String.format("%02f", mSong.getPrice()));

        Log.d("SONG_INDEX", "2: " + mSongs.indexOf(mSong));

        if (mIsPlayList) {
            mTvListCounter.setText((mSongs.indexOf(mSong) + 1) + "/" + mSongs.size());
        }
    }

    private void setPlayerBtns() {
        mIvPlayBtn.setOnClickListener(new PlayBtnClicked());
        mIvActivePrevBtn.setOnClickListener(new MoveBtnClicked(-1));
        mIvActiveNextBtn.setOnClickListener(new MoveBtnClicked(1));
        mIvLoopBtn.setOnClickListener(new LoopBtnClicked());

        mIvActivePrevBtn.setVisibility((mIsPlayList) ? View.VISIBLE : View.GONE);
        mIvActiveNextBtn.setVisibility((mIsPlayList) ? View.VISIBLE : View.GONE);
        mIvPrevBtn.setVisibility((!mIsPlayList) ? View.VISIBLE : View.GONE);
        mIvNextBtn.setVisibility((!mIsPlayList) ? View.VISIBLE : View.GONE);
    }

    private void setPlayerProgress() {
        mSbProgress.setMax(mMediaPlayer.getDuration());
        mSbProgress.setProgress(0);
        mSbProgress.setOnSeekBarChangeListener(new SeekBarChangeListener());
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (PermissionHelper.isPermGranted(grantResults)) {
            switch (requestCode) {
                case AUDIO_PERM_REQUEST_CODE:
                    setupPlayerPermissions();
                    break;
            }
        }
    }

    private void startPlayer() {
        if (!mIsPlayerPrepared) {
            try {
                setPlayerFromBytes();
            } catch (Exception exc) {

            }

            return;
        }

        playMusic();
    }

    private void playMusic() {
        mIsPlayerPlaying = true;
        mMediaPlayer.start();
        startPlayerThread();
    }

    private void pausePlayer() {
        stopPlayer(0);
    }

    private void stopPlayer(int action) {
        if (mMediaPlayer != null) {
            if ((mMediaPlayer.isPlaying()) && (action == 0)) {
                mMediaPlayer.pause();
                mIsPlayerPlaying = false;
                pausePlayerThread();
            } else if ((action == 1) || (action == 2)) {
                mMediaPlayer.stop();
                mMediaPlayer.release();

                mIsPlayerPlaying = false;
                mIsPlayerPrepared = false;
                mPlayerPosition = 0; //mMediaPlayer.getCurrentPosition();

                resetProgress();

                pausePlayerThread();

                Log.d("DIRECTORY_LIST", "HERE3");

                SongHelper.listTempDir(this);

                /*if (!mSong.isLocal()) {
                    Log.d("REMOVING_SONG", "HERE1");
                    if (action == 1) {
                        Log.d("REMOVING_SONG", "HERE2");
                        SongHelper.deleteTempAudioFile(this, mSong.getOriginalLoc());
                    } else {
                        Log.d("REMOVING_SONG", "HERE3");
                        SongHelper.deleteTempAudioDir(this);
                    }
                    Log.d("REMOVING_SONG", "HERE4");
                }*/

                if (action == 2) {
                    SongHelper.deleteTempAudioDir(this);
                }

                SongHelper.listTempDir(this);

                /*mSong.setSavedLoc(mSongBytes);
                mSong.save();*/
            }
        }
    }

    private void movePlayerTo() {
        mMediaPlayer.seekTo(mSbProgress.getProgress());
        showPlayerProgress();
    }

    private void movePlayerListTo(int direction) {
        int index = mSongs.indexOf(mSong);
        index += direction;
        index %= mSongs.size();

        Log.d("SONG_INDEX", "" + index);

        stopPlayer(1);

        mSong = mSongs.get(index);
        mSong = getSongById();

        setSongPlayer();
    }

    private Song getSongById() {
        return Select.from(Song.class).where(Condition.prop(Song.COLUMN_ID).eq(mSong.getSongId()))
                .first();
    }

    private void startPlayerThread() {
        mSeekBarThread = new SeekBarThread();
        mSeekBarThread.start();

        mLineVisualizer.setVisibility(View.VISIBLE);
    }

    private void pausePlayerThread() {
        if ((mSeekBarThread != null) && (mSeekBarThread.isAlive())) {
            mSeekBarThread.setThreadStatus(false);
        }

        mLineVisualizer.setVisibility(View.GONE);
    }

    private class PlayBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            togglePlayer();
        }
    }

    private class MoveBtnClicked implements View.OnClickListener {

        private int mDirection = 0;

        public MoveBtnClicked(int direction) {
            mDirection = direction;
        }

        @Override
        public void onClick(View view) {
            if (!mIsPlayList) return;

            mIsPlayerPrepared = false;

            movePlayerListTo(mDirection);
        }
    }

    private class LoopBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            toggleLooping();
        }
    }

    private void toggleLooping() {
        if (mMediaPlayer == null) return;

        int loopBtnResId = R.drawable.ic_no_repeat;

        if (mRepeatState == REPEAT_STATES.NO_REPEAT) {
            mRepeatState = REPEAT_STATES.REPEAT_ONE;
        } else if (mRepeatState == REPEAT_STATES.REPEAT_ONE) {
            mRepeatState = (mIsPlayList) ? REPEAT_STATES.REPEAT_ALL : REPEAT_STATES.NO_REPEAT;
        } else if (mRepeatState == REPEAT_STATES.REPEAT_ALL) {
            mRepeatState = REPEAT_STATES.NO_REPEAT;
        }

        mMediaPlayer.setLooping(mRepeatState == REPEAT_STATES.REPEAT_ONE);

        if (mRepeatState == REPEAT_STATES.NO_REPEAT) {
            loopBtnResId = R.drawable.ic_no_repeat;
        } else if (mRepeatState == REPEAT_STATES.REPEAT_ONE) {
            loopBtnResId = R.drawable.ic_repeat_one;
        } else if (mRepeatState == REPEAT_STATES.REPEAT_ALL) {
            loopBtnResId = R.drawable.ic_repeat_all;
        }

        mIvLoopBtn.setImageResource(loopBtnResId);
    }

    private class MediaPreparedListener implements MediaPlayer.OnPreparedListener {

        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            mIsPlayerPrepared = true;
            setPlayerDetails(mediaPlayer);

            if (mIsPlayerPlaying) {
                togglePlayer();
            }
        }
    }

    private class MediaCompletionListener implements MediaPlayer.OnCompletionListener {

        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            if (mIsPlayList && (mRepeatState == REPEAT_STATES.REPEAT_ALL)) {
                movePlayerListTo(1);
            } else {
                setupPlayerControls();
            }
        }
    }

    private class SeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        boolean mIsPlaying = false;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                mPlayerPosition = progress;
                movePlayerTo();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mIsPlaying = mIsPlayerPlaying;
            pausePlayer();
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if (mIsPlaying) {
                playMusic();
            }
        }
    }

    private class SeekBarThread extends Thread {

        private boolean mSeekBarActive = true;

        public  SeekBarThread() {

        }

        public void setThreadStatus(boolean seekBarActive) {
            mSeekBarActive = seekBarActive;
        }

        @Override
        public void run() {
            try {
                while ((mSeekBarActive) && ((mMediaPlayer != null) && (mMediaPlayer.isPlaying()))) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mPlayerPosition = mMediaPlayer.getCurrentPosition();
                            updatePlayerStatus();
                        }
                    });

                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {

                    }
                }
            } catch (IllegalStateException ise) {

            }
        }
    }

    private class FabClicked implements View.OnClickListener {

        private Context mContext = null;

        public FabClicked(Context context) {
            mContext = context;
        }

        @Override
        public void onClick(View view) {
            PopupMenu menu = new PopupMenu(mContext, view);
            menu.inflate(R.menu.song_options_menu);
            menu.setOnMenuItemClickListener(new MenuItemClicked());

            menu.getMenu().findItem(R.id.action_like).setVisible(!mSong.isLiked());
            menu.getMenu().findItem(R.id.action_unlike).setVisible(mSong.isLiked());

            MenuPopupHelper menuHelper = new MenuPopupHelper(mContext, (MenuBuilder) menu.getMenu(),
                view);

            boolean isPlaying = (mMediaPlayer != null) && (mMediaPlayer.isPlaying());

            menuHelper.setOnDismissListener(new MenuPopupDismissed(isPlaying));
            menuHelper.setForceShowIcon(true);
            menuHelper.show();

            if (isPlaying) pausePlayer();
        }
    }

    private class MenuItemClicked implements PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (!isUserLoggedIn()) {
                DialogHelper.twoButtonDialog(SongPlayerScreen.this, R.string.ss_title,
                        R.string.ss_login_msg, R.string.ss_dialog_btn_proceed,
                        R.string.ss_dialog_btn_cancel, new SongLoginDialog(item.getItemId()))
                        .show();

                return true;
            }

            handleFabClick(item.getItemId());

            return false;
        }
    }

    private void handleFabClick(int itemId) {
        switch (itemId) {
            case R.id.action_like:
                toggleLike(true);
                break;

            case R.id.action_unlike:
                toggleLike(false);
                break;

            case R.id.action_comment:
                leaveAComment();
                break;

            case R.id.action_purchase:
                askPurchase();
                break;

            case R.id.action_share:
                break;
        }
    }

    private class SongLoginDialog extends DialogHelper.Listener {

        private int mMenuId = -1;

        public SongLoginDialog(int menuId) {
            mMenuId = menuId;
        }

        @Override
        public void onPositive(MaterialDialog dialog) {
            login(mMenuId);
        }
    }

    public void login(int actionId) {
        mActionId = actionId;
        Intent intent = new Intent(SongPlayerScreen.this, LoginScreen.class);
        startActivityForResult(intent, LOGIN_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == LOGIN_REQUEST_CODE) && (resultCode == Activity.RESULT_OK)) {
            handleFabClick(mActionId);
        }
    }

    private class CommentAdapter extends CommentListAdapter {

        public CommentAdapter(Context context) {
            super(context);
        }

        @Override
        protected void reply(Comment comment) {
//            mEdtComment.setText("@" + comment.getAuthorPseudo());
            initComment();

            mRlReplyBubble.setVisibility(View.VISIBLE);
            mTvReplyUser.setText(comment.getAuthorPseudo());
            mTvReplyText.setText(comment.getContent());

            mComment.setParentId(comment.getCommentId());
            notifyDataSetChanged();
        }
    }

    private void initComment() {
        mComment = new Comment();
        mComment.setSongId(mSong.getSongId());
        mRlReplyBubble.setVisibility(View.GONE);
    }

    private void toggleLike(boolean liked) {
        if (liked) {
            mSong.like();
        } else {
            mSong.unlike();
        }

        mSong.save();

        mIvLike.setVisibility(liked ? View.VISIBLE : View.GONE);
    }

    private boolean isUserLoggedIn() {
        User user = Ndule.getUser();

        return ((user != null) && (user.isLoggedIn()));
    }

    private void leaveAComment() {
        switchCommentMode();
    }

    private void askPurchase() {
        if (!mSong.belongsToUser()) {
            switchMode(SCREEN_MODES.PURCHASE);
            return;
        }

        DialogHelper.showMessage(this, R.string.ss_title, R.string.ss_song_owned);
    }

    private void share() {

    }

    private void switchMode(SCREEN_MODES screenMode) {
        switch (screenMode) {
            case DEFAULT:
                switchDefaultMode();
                break;

            case PURCHASE:
                switchPurchaseMode();
                break;

            case COMMENT:
                switchCommentMode();
                break;
        }
    }

    private void switchDefaultMode() {
//        SCREEN_MODES screenMode = mScreenMode;
        mScreenMode = SCREEN_MODES.DEFAULT;
//        mRlTopView.startAnimation(buildAnimation(screenMode));
        mRlTopView.setVisibility(View.VISIBLE);
        mFabMenu.setVisibility(View.VISIBLE);
        mCivImage.setVisibility(View.GONE);
        mLlPurchase.setVisibility(View.GONE);
        mLlComments.setVisibility(View.GONE);
    }

    private void switchPurchaseMode() {
//        SCREEN_MODES screenMode = mScreenMode;
        mScreenMode = SCREEN_MODES.PURCHASE;
//        mRlTopView.startAnimation(buildAnimation(screenMode));
        mRlTopView.setVisibility(View.GONE);
        mFabMenu.setVisibility(View.GONE);
        mCivImage.setVisibility(View.VISIBLE);
//        mCivImage.startAnimation(buildAnimation());
        mLlPurchase.setVisibility(View.VISIBLE);

        mPayment = new Payment();

        resetPurchaseMode();
    }

    private void switchCommentMode() {
        SCREEN_MODES screenMode = mScreenMode;
        mScreenMode = SCREEN_MODES.COMMENT;
//        mLlPlayer.startAnimation(buildAnimation(screenMode));
        mRlTopView.setVisibility(View.GONE);
        mFabMenu.setVisibility(View.GONE);
        mCivImage.setVisibility(View.VISIBLE);
//        mCivImage.startAnimation(buildAnimation());
        mLlComments.setVisibility(View.VISIBLE);
    }

    private class MenuPopupDismissed implements PopupWindow.OnDismissListener {

        private boolean mWasPlaying = false;

        public MenuPopupDismissed(boolean wasPlaying) {
            mWasPlaying = wasPlaying;
        }

        @Override
        public void onDismiss() {
            if (mWasPlaying) startPlayer();
        }
    }

    private class SongDataDownloader extends AsyncTask<String, Void, ByteArrayOutputStream> {

        protected void onPreExecute() {
            mDialog = DialogHelper.loadingDialog(SongPlayerScreen.this, R.string.ss_song_loading).show();
        }

        protected ByteArrayOutputStream doInBackground(String... imgUrls) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try {
                URL songUrl = new URL(imgUrls[0]);
                InputStream input = new BufferedInputStream(songUrl.openStream());
                byte data[] = new byte[1024];
                int count = 0;

                while ((count = input.read(data)) != -1) {
                    out.write(data, 0, count);
                }

                input.close();
            } catch (Exception e) {
                Log.d("SONG_DATA", "EXCEPTION2: " + e.getMessage());
            }

            return out;
        }

        protected void onPostExecute(ByteArrayOutputStream songData) {
            mDialog.dismiss();

            mSongBytes = songData.toByteArray();
            mIsPlayerPlaying = true;

            setPlayerBtns();
        }
    }

    public void onBackPressed() {
        if (mScreenMode == SCREEN_MODES.DEFAULT) {
            Log.d("DIRECTORY_LIST", "HERE4");
            super.onBackPressed();
            return;
        }

        switchDefaultMode();
    }
}