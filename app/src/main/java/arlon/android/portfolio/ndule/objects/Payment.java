package arlon.android.portfolio.ndule.objects;

import android.os.Parcel;
import android.os.Parcelable;
import com.orm.SugarRecord;
import com.orm.dsl.Column;

/**
 * Created by Arlon Mukeba on 3/2/2018.
 */

public class Payment extends SugarRecord implements Parcelable {

    public final static String PAYMENT = "payment";

    public final static String COLUMN_ID = "COLUMN_ID";
    public final static String COLUMN_TIMESTAMP = "COLUMN_TIMESTAMP";
    public final static String COLUMN_METHOD = "COLUMN_METHOD";
    public final static String COLUMN_AMOUNT = "COLUMN_AMOUNT";
    public final static String COLUMN_STATUS = "COLUMN_STATUS";
    public final static String COLUMN_USER_ID = "COLUMN_USER_ID";
    public final static String COLUMN_SONG_ID = "COLUMN_SONG_ID";

    public final static String COLUMN_STATUS_PAID = "COLUMN_STATUS_PAID";
    public final static String COLUMN_STATUS_PENDING = "COLUMN_STATUS_PENDING";

    public final static String PAYMENT_METHOD_VOUCHER = "Voucher Payment";
    public final static String PAYMENT_METHOD_ACCOUNT = "Account Payment";
    public final static String PAYMENT_METHOD_AIRTEL_MONEY = "Airtel Money";
    public final static String PAYMENT_METHOD_MPESA = "Vodacom MPESA";
    public final static String PAYMENT_METHOD_ORANGE_MONEY = "Orange Money";

    @Column(name = COLUMN_ID)
    private String mPaymentId = "";

    @Column(name = COLUMN_TIMESTAMP)
    private String mTimeStamp = "";

    @Column(name = COLUMN_METHOD)
    private String mMethod = "";

    @Column(name = COLUMN_AMOUNT)
    private double mAmount = 0.0;

    @Column(name = COLUMN_STATUS)
    private String mStatus = COLUMN_STATUS_PENDING;

    @Column(name = COLUMN_USER_ID)
    private String mUserId = "";

    @Column(name = COLUMN_SONG_ID)
    private String mSongId = "";

    public Payment() {
    }

    public Payment(Parcel in) {
        setId(in.readLong());
        setPaymentId(in.readString());
        setTimeStamp(in.readString());
        setMethod(in.readString());
        setAmount(in.readDouble());
        setStatus(in.readString());
        setUserId(in.readString());
        setSongId(in.readString());
    }

    public void setPaymentId(String paymentId) {
        mPaymentId = paymentId;
    }

    public String getPaymentId() {
        return mPaymentId;
    }

    public void setTimeStamp(String timeStamp) {
        mTimeStamp = timeStamp;
    }

    public String getTimeStamp() {
        return mTimeStamp;
    }

    public void setMethod(String method) {
        mMethod = method;
    }

    public String getMethod() {
        return mMethod;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }

    public double getAmount() {
        return mAmount;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setSongId(String songId) {
        mSongId = songId;
    }

    public String getSongId() {
        return mSongId;
    }

    public static final Creator<Payment> CREATOR = new Creator<Payment>() {

        @Override
        public Payment createFromParcel(Parcel source) {
            return new Payment(source);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getPaymentId());
        dest.writeString(getTimeStamp());
        dest.writeString(getMethod());
        dest.writeDouble(getAmount());
        dest.writeString(getStatus());
        dest.writeString(getUserId());
        dest.writeString(getSongId());
    }

    public long save() {
        if (mTimeStamp.isEmpty()) {
            mTimeStamp = "" + System.currentTimeMillis();
        }

        if (mPaymentId.isEmpty()) {
            mPaymentId = "Payment_" + mTimeStamp;
        }

        return super.save();
    }
}