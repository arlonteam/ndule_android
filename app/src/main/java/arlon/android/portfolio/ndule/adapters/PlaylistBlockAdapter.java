package arlon.android.portfolio.ndule.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.objects.Song;

/**
 * Created by Arlon Mukeba on 1/26/2018.
 */

public class PlaylistBlockAdapter extends RecyclerView.Adapter<PlaylistBlockAdapter.BlockViewHolder> {

    private Context mContext = null;
    private ArrayList<Object> mBlocks;
    private int mMeasuredWidth = 75;
    private int mMeasuredHeight = 75;

    public PlaylistBlockAdapter(Context context, ArrayList<Object> blocks, int measuredWidth,
                                int measuredHeight) {

        mContext = context;
        mBlocks = blocks;
        mMeasuredWidth = measuredWidth;
        mMeasuredHeight = measuredHeight;
    }

    public BlockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(parent.getContext())
            .inflate(R.layout.layout_playlist_block, parent, false);

        if (!((mMeasuredWidth == (-1)) || (mMeasuredHeight == (-1)))) {
            relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(mMeasuredWidth,
                mMeasuredHeight));
        } else {
            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(100, 100);
            ll.setMargins(2, 2, 2, 2);
            relativeLayout.setLayoutParams(ll);
        }

        return new BlockViewHolder(relativeLayout);
    }

    public void onBindViewHolder(final BlockViewHolder blockViewHolder, int position) {
        Object block = mBlocks.get(position);

        blockViewHolder.mTvCounter.setVisibility(View.GONE);
        blockViewHolder.mIvImage.setVisibility(View.GONE);

        if (!(block instanceof Song)) {
            blockViewHolder.mTvCounter.setVisibility(View.VISIBLE);
            blockViewHolder.mTvCounter.setText(block.toString());
            return;
        }

        blockViewHolder.mIvImage.setVisibility(View.VISIBLE);

        Song song = (Song) block;

        String songImage = Ndule.SONG_IMAGE_URL + song.getThumbnail() + ".jpg";

        Log.d("SONG_IMAGE_URL", songImage);

        Glide.with(mContext).load(songImage).asBitmap().override(500, 500)
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                    .into(new SimpleTarget<Bitmap>(500, 500) {
                              @Override
                              public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                  blockViewHolder.mIvImage.setImageBitmap(bitmap);
                              }
                          }
                    );
    }

    public int getItemCount() {
        return ((mBlocks == null) || (mBlocks.isEmpty())) ? 0 : mBlocks.size();
    }

    public class BlockViewHolder extends RecyclerView.ViewHolder {

        public ImageView mIvImage;
        public TextView mTvCounter;

        public BlockViewHolder(View view) {
            super(view);

            mIvImage = (ImageView) view.findViewById(R.id.ivSongImage);
            mTvCounter = (TextView) view.findViewById(R.id.ivPlaylistCounter);
        }
    }
}