package arlon.android.portfolio.ndule.objects;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import arlon.android.portfolio.ndule.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Arlon Mukeba on 2/21/2018.
 */

public class CommentViewHolder extends RecyclerView.ViewHolder {

    public View vHolder;
    public TextView mTvAuthor;
    public TextView mTvContent;
    public TextView mTvDate;
    public ImageView mIvLike;
    public TextView mTvLike;
    public ImageView mIvReply;
    public TextView mTvReplies;
    public View mVLastDot;
    public CircleImageView mCivImage;
    public RecyclerView mRvReplies;

    public CommentViewHolder(View view) {
        super(view);

        vHolder = view;

        mTvAuthor = (TextView) view.findViewById(R.id.tvAuthor);
        mTvContent = (TextView) view.findViewById(R.id.tvContent);
//        etvCommentContent = (ExpandableTextView) view.findViewById(R.id.etvCommentContent);
        mTvDate = (TextView) view.findViewById(R.id.tvDate);
        mIvLike = (ImageView) view.findViewById(R.id.ivLike);
        mTvLike = (TextView) view.findViewById(R.id.tvLikes);
        mIvReply = (ImageView) view.findViewById(R.id.ivReply);
        mTvReplies = (TextView) view.findViewById(R.id.tvReplies);
        mCivImage = (CircleImageView) view.findViewById(R.id.ivImage);
        mRvReplies = (RecyclerView) view.findViewById(R.id.rvReplies);
        mVLastDot = (View) view.findViewById(R.id.vLastDot);
    }

    public void setColorTheme(int themeColor) {
        mTvAuthor.setTextColor(themeColor);
        mTvDate.setTextColor(themeColor);
        mTvLike.setTextColor(themeColor);
        mTvReplies.setTextColor(themeColor);
        mCivImage.setBorderColor(themeColor);
    }
}