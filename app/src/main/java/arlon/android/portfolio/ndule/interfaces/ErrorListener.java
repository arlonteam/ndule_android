package arlon.android.portfolio.ndule.interfaces;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

/**
 * Created by Arlon Mukeba on 2/6/2018.
 */

public class ErrorListener implements Response.ErrorListener {

    private Context mContext = null;
    private ResultHandler mResultHandler = null;

    public ErrorListener(Context context, ResultHandler resultHandler) {
        mContext = context;
        mResultHandler = resultHandler;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if ((error != null) && (error.getMessage() != null)) {
            Log.d("ErrorMessage", error.getMessage());
//            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_LONG).show();
            mResultHandler.error(error.getMessage());
        }
    }
}