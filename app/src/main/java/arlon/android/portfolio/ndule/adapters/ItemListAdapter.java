package arlon.android.portfolio.ndule.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Song;
import arlon.android.portfolio.ndule.screens.SongPlayerScreen;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Arlon Mukeba on 1/27/2018.
 */

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ItemViewHolder> {

    public enum TYPE { ALL, FAVS, PLAYLIST, ARTIST, SEARCH };
    public enum ACTION { PLAY, ADDTOPLAYLIST, ADDTOFAVS, REMOVE };

    protected TYPE mType = TYPE.ALL;
    private Context mContext = null;
    protected ArrayList<Object> mItems;
    protected ArrayList<Object> mClonedItems = new ArrayList<Object>();

    public ItemListAdapter(Context context, TYPE type) {
        mType = type;
        mContext = context;
    }

    public void setItems(ArrayList<Object> items) {
        mItems = items;
        mClonedItems = items;
    }

    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout rlSongItem = (RelativeLayout) LayoutInflater.from(parent.getContext())
            .inflate(R.layout.layout_song_item, parent, false);

        return new ItemViewHolder(rlSongItem);
    }

    public void onBindViewHolder(ItemViewHolder itemViewHolder, int position) {
        Object item = mItems.get(position);

        setItemDetails(itemViewHolder, item);
        setItemClickListener(itemViewHolder, item);

        itemViewHolder.mLlButtons.setVisibility((mType.equals(TYPE.SEARCH)) ? View.GONE :
            View.VISIBLE);

        itemViewHolder.mTvItemType.setVisibility((mType.equals(TYPE.SEARCH)) ? View.VISIBLE :
            View.GONE);

        if (mType.equals(TYPE.SEARCH)) {
            if (item instanceof Song) {
                itemViewHolder.mTvItemType.setText("SONG");
            } else {
                itemViewHolder.mTvItemType.setText("ARTIST");
            }
        } else {
            if (item instanceof Song) {
                setSongViews(itemViewHolder, (Song) item);
            } else {
                setArtistViews(itemViewHolder, (Artist) item);
            }
        }
    }

    private void setItemDetails(ItemViewHolder itemViewHolder, Object item) {
        String imageUrl = "";
        String title = "";
        String firstSub = "";
        String secondSub = "";

        if (item instanceof Song) {
            Song song = ((Song) item);
            imageUrl = Ndule.SONG_IMAGE_URL + song.getThumbnail() + ".jpg";
            title = song.getTitle();
            firstSub = song.getArtist().getName();
            secondSub = song.getReleaseDate();
        } else {
            Artist artist = ((Artist) item);
            imageUrl = Ndule.ARTIST_IMAGE_URL + artist.getImage() + ".jpg";
            title = artist.getName();

            Select select = Select.from(Song.class).where(Condition.prop(Song.COLUMN_ARTIST_ID)
                .eq(artist.getArtistId()));

            int count = (int) select.count();

            firstSub = count + " song" + ((count == 1) ? "" : "s");

            select = Select.from(Song.class).where(Condition.prop(Song.COLUMN_ARTIST_ID)
                .eq(artist.getArtistId())).and(Condition.prop(Song.COLUMN_LIKED).eq("YES"));

            secondSub = ((int) select.count()) + " liked";
        }

        populateItemDetails(itemViewHolder, imageUrl, title, firstSub, secondSub);
    }

    private void populateItemDetails(final ItemViewHolder itemViewHolder, String imageUrl,
                                     String title, String firstSub, String secondSub) {

        Glide.with(mContext).load(imageUrl).asBitmap().override(500, 500)
            /*.error(R.drawable.ic_treble_key)*/
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                .into(new SimpleTarget<Bitmap>(500, 500) {
                          @Override
                          public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                              itemViewHolder.mCivImage.setImageBitmap(bitmap);
                          }
                      }
                );

        itemViewHolder.mTvTitle.setText(title);
        itemViewHolder.mTvArtist.setText(firstSub);
        itemViewHolder.mTvDuration.setText(secondSub);
    }

    private void setArtistViews(ItemViewHolder artistViewHolder, Artist artist) {
        artistViewHolder.mIvPlay.setVisibility(View.VISIBLE);
        artistViewHolder.mIvAddToPlaylist.setVisibility(View.GONE);
        artistViewHolder.mIvAddToFav.setVisibility(View.GONE);
        artistViewHolder.mIvRemove.setVisibility(View.GONE);
    }

    private void setSongViews(final ItemViewHolder songViewHolder, Song song) {
        songViewHolder.mIvPlay.setVisibility((mType == TYPE.ALL) ? View.VISIBLE : View.GONE);

        songViewHolder.mIvAddToPlaylist.setVisibility(((mType == TYPE.ALL) &&
                (!song.belongsToPlaylist())) ? View.VISIBLE : View.GONE);

        songViewHolder.mIvAddToFav.setVisibility(((mType == TYPE.ALL) && (!song.isLiked())) ?
                View.VISIBLE : View.GONE);

        songViewHolder.mIvRemove.setVisibility(((mType == TYPE.FAVS) || (mType == TYPE.PLAYLIST)) ?
                View.VISIBLE : View.GONE);
    }

    protected void setItemClickListener(ItemViewHolder songViewHolder, Object item) {

    }

    protected void play(Activity activity, View view, Song song, Artist artist, boolean isPlayList) {
        Intent intent = new Intent(activity, SongPlayerScreen.class);
        intent.putExtra(SongPlayerScreen.SONG_PARAM, song);
        intent.putExtra(SongPlayerScreen.ARTIST_PARAM, artist);
        intent.putExtra(SongPlayerScreen.PLAYLIST_PARAM, isPlayList);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity,
                    view, "song_image");

            activity.startActivity(intent, options.toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    public int getItemCount() {
        return ((mItems == null) || (mItems.isEmpty())) ? 0 : mItems.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout mLlView;
        public CircleImageView mCivImage;
        public TextView mTvTitle;
        public TextView mTvArtist;
        public TextView mTvDuration;
        public LinearLayout mLlButtons;
        public TextView mTvItemType;
        public ImageView mIvAddToPlaylist;
        public ImageView mIvRemove;
        public ImageView mIvAddToFav;
        public ImageView mIvPlay;

        public ItemViewHolder(View view) {
            super(view);

            mLlView = (LinearLayout) view.findViewById(R.id.llSongItem);
            mCivImage = (CircleImageView) view.findViewById(R.id.civImage);
            mTvTitle = (TextView) view.findViewById(R.id.tvTitle);
            mTvArtist = (TextView) view.findViewById(R.id.tvArtist);
            mTvDuration = (TextView) view.findViewById(R.id.tvDuration);
            mLlButtons = (LinearLayout) view.findViewById(R.id.llItemBtns);
            mTvItemType = (TextView) view.findViewById(R.id.tvItemType);
            mIvAddToPlaylist = (ImageView) view.findViewById(R.id.ivAddPlaylist);
            mIvRemove = (ImageView) view.findViewById(R.id.ivRemovePlaylist);
            mIvAddToFav = (ImageView) view.findViewById(R.id.ivFav);
            mIvPlay = (ImageView) view.findViewById(R.id.ivPlay);
        }
    }
}