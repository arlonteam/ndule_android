package arlon.android.portfolio.ndule.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Hashtable;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.objects.User;
import arlon.android.portfolio.ndule.utils.StringUtils;

/**
 * Created by Arlon Mukeba on 1/16/2018.
 */

public class LoginFragment extends BaseFragment {

    public static final String TAG = "LoginFragment";

    private EditText mEdtUsername = null;
    private EditText mEdtPwd = null;
    private TextView mTvLogin = null;
    private TextView mTvSignUp = null;
    private TextView mTvPwdForgotten = null;

    public static LoginFragment newInstance(Hashtable<String, Object> params) {
        return new LoginFragment();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_login;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        populateViews();
    }

    public void setupViews() {
        mEdtUsername = (EditText) getView().findViewById(R.id.edtUsername);
        mEdtPwd = (EditText) getView().findViewById(R.id.edtPwd);
        mTvLogin = (TextView) getView().findViewById(R.id.tvLogin);
        mTvLogin.setOnClickListener(new LoginBtnClicked());
        mTvPwdForgotten = (TextView) getView().findViewById(R.id.tvPwdForgotten);
        mTvPwdForgotten.setOnClickListener(new ForgottenPwdBtnClicked());
        mTvSignUp = (TextView) getView().findViewById(R.id.tvSignUp);
        mTvSignUp.setOnClickListener(new SignUpBtnClicked());
    }

    public void populateViews() {

    }

    private class LoginBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            login();
        }
    }

    private class SignUpBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            mFragmentSwitcher.switchFragment(SignUpFragment.TAG, new Hashtable<String, Object>(), false);
        }
    }

    private class ForgottenPwdBtnClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            mFragmentSwitcher.switchFragment(LostPwdFragment.TAG, new Hashtable<String, Object>(), false);
        }
    }

    private void login() {
        Log.d("SAVED_USER", "HERE_1");

        if (!validateLogin()) {
            Toast.makeText(getContext(), R.string.ls_empty_fields_error, Toast.LENGTH_SHORT).show();
            return;
        }

        String email = mEdtUsername.getText().toString();
        String password = mEdtPwd.getText().toString();

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        Log.d("SAVED_USER", user.toString());

        if (localLogin(user)) {
            Log.d("SAVED_USER", "2. " + user.toString());
            updateLoginStatus(Ndule.getUser());
            return;
        }

        proceedWithLogin(user);
    }

    private void updateLoginStatus(User user) {
        user.setLogInStatus(User.LOGIN_STATUS_LOGGED_IN);
        user.save();

        Ndule.setUser(user);

        mFragmentSwitcher.navigateBack(Activity.RESULT_OK);
    }

    private boolean localLogin(User user) {
        User currentUser = Ndule.getUser();

        if (currentUser == null) return false;

        Log.d("SAVED_USER", "EXISTING " + currentUser.toString());

//        if (currentUser.getDeviceToken().isEmpty()) return false;

        return  ((currentUser.getEmail().equals(user.getEmail())) && (currentUser.getPassword()
            .equals(user.getPassword())));
    }

    private void proceedWithLogin(User user) {
        /*mDialog = DialogHelper.loadingDialog(this, getString(R.string.af_dialog_message)) .show();

        Log.d("USERTEST", "1: " + user.toString());

        User dummyUser = Ndule.getDummy();

        if (dummyUser != null) {
            Log.d("USERTEST", dummyUser.toString());
            user.setUsername(dummyUser.getUsername());
            user.setDeviceToken(dummyUser.getDeviceToken());
        } else {
            user.setUsername(Ndule.getDeviceId(this));
        }

        user.save();*/

//        UserHelper.login(this, user, new LoginHandler());
    }

    /*private class LoginHandler implements NetworkListener {

        private String message = getString(R.string.lf_account_not_found_error);

        @Override
        public void success(Object data) {
            mDialog.dismiss();

            ArrayList<User> users = (ArrayList<User>) data;

            Log.d("LOGINRETURNED", users.size() + "");

            if (!users.isEmpty()) {
                updateLoginStatus(users.get(0));
                return;
            }

            Toast.makeText(LoginScreen.this, this.message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void error(String message) {
            mDialog.dismiss();
//            loadUsers();
            Log.d("USERS_COUNT", "MESSAGE: " + message);
            Toast.makeText(LoginScreen.this, this.message, Toast.LENGTH_SHORT).show();
        }
    }*/

    private boolean validateLogin() {
        boolean valid = true;

        if (mEdtUsername.getText().toString().isEmpty()) {
            mEdtUsername.setError(getString(R.string.ls_empty_username_error));
            valid = false;
        } else {
            if (!StringUtils.validateEmail(mEdtUsername.getText().toString())) {
                mEdtUsername.setError(getString(R.string.ls_invalid_email_error));
                valid = false;
            }
        }

        if (mEdtPwd.getText().toString().isEmpty()) {
            mEdtPwd.setError(getString(R.string.ls_empty_password_error));
            valid = false;
        }

        return valid;
    }
}