package arlon.android.portfolio.ndule.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.adapters.ItemListAdapter;
import arlon.android.portfolio.ndule.objects.Artist;
import arlon.android.portfolio.ndule.objects.Song;

/**
 * Created by Arlon Mukeba on 3/3/2018.
 */

public class SongSearchFragment extends BaseFragment {

    public static final String TAG = "SONG_SEARCH_FRAGMENT";

    private RecyclerView mRvItemList = null;
    private RelativeLayout mRlSearch = null;
    private EditText mEdtSearch = null;
    private ImageView mIvCancelSearch = null;
    private ItemAdapter mItemListAdapter = null;
    protected ItemListAdapter.TYPE mItemListType = ItemListAdapter.TYPE.SEARCH;

    public SongSearchFragment() {

    }

    public static SongSearchFragment newInstance(Hashtable<String, Object> params) {
        SongSearchFragment songListFragment = new SongSearchFragment();
        Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_PARAMS, params);
        songListFragment.setArguments(args);
        return songListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onResume() {
        super.onResume();
    }

    protected int getFragmentLayout() {
        return R.layout.fragment_song_search;
    }

    public void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        super.initFragment(savedInstanceState);
    }

    private void setupViews() {
        mRvItemList = (RecyclerView) getView().findViewById(R.id.rvItemList);

        mRlSearch = (RelativeLayout) getView().findViewById(R.id.rlSearch);

        mEdtSearch = (EditText) getView().findViewById(R.id.edtSearch);
        mEdtSearch.addTextChangedListener(new EditTextWatcher());

        mIvCancelSearch = (ImageView) getView().findViewById(R.id.ivSearchCancel);
        mIvCancelSearch.setOnClickListener(new SearchCancelClicked());
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        loadSongs((ArrayList<Object>) data);
    }

    protected void populateViews() {
    }

    private void loadSongs(ArrayList<Object> songs) {
        mItemListAdapter = getItemListAdapter();

        Collections.sort(songs, new ItemComparator());
        mItemListAdapter.setItems(songs);

        Log.d("SONG_LIST_TYPE", mItemListType + " : " + songs.size());

        mRvItemList.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvItemList.setItemAnimator(new DefaultItemAnimator());
        mRvItemList.setAdapter(mItemListAdapter);

        filterList("");
    }

    private class EditTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            filterList(s.toString());
        }
    }

    private class SearchCancelClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            mEdtSearch.setText("");
        }
    }

    protected ItemAdapter getItemListAdapter() {
        return new ItemAdapter(mItemListType);
    }

    public int getFabIconResId() {
        return -1;
    }

    protected DataFetcher getLoader() {
        return new HomeDataFetcher(getContext());
    }

    private static class HomeDataFetcher extends DataFetcher {

        public HomeDataFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            ArrayList<Object> objects = new ArrayList<Object>();
            objects.addAll(Select.from(Song.class).list());
            objects.addAll(Select.from(Artist.class).list());
            return objects;
        }
    }

    private class ItemAdapter extends ItemListAdapter implements Filterable {

        private LayoutInflater mLayoutInflater;
        private ArrayList<Song> mSongs;

        public ItemAdapter(TYPE type) {
            super(getContext(), type);
        }

        protected void setItemClickListener(ItemViewHolder songViewHolder, Object item) {
            Song song = (item instanceof Song) ? (Song) item : null;
            Artist artist = (item instanceof Artist) ? (Artist) item : null;

            songViewHolder.mLlView.setOnClickListener(new ItemClickListener(songViewHolder.mCivImage,
                artist, song));
        }

        private class ItemClickListener implements View.OnClickListener {

            private View mView = null;
            private Artist mArtist = null;
            private Song mSong = null;

            public ItemClickListener(View view, Artist artist, Song song) {
                mView = view;
                mArtist = artist;
                mSong = song;
            }

            @Override
            public void onClick(View view) {
                play(getActivity(), mView, mSong, mArtist, (mArtist != null));
            }
        }

        @Override
        public Filter getFilter() {
            return new ItemFilter();
        }

        private class ItemFilter extends Filter {

            private FilterResults mResults = new FilterResults();

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<Object> items = mClonedItems;
                String filter = constraint.toString();

                Log.d("FILTERING", filter);

                if (constraint.length() != 0) {
                    items = new ArrayList<Object>();

                    for (Object item : mClonedItems) {
                        if (getItemTitle(item).toLowerCase().contains(filter.toLowerCase())) {
                            items.add(item);
                        }
                    }
                }

                mResults.values = items;
                mResults.count = items.size();

                return mResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mItems = (ArrayList<Object>) mResults.values;

                notifyDataSetChanged();
            }
        }
    }

    private class ItemComparator implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            return getItemTitle(o1).compareTo(getItemTitle(o2));
        }
    }

    private String getItemTitle(Object object) {
        String itemTitle = "";

        if (object instanceof Song) {
            itemTitle = ((Song) object).getTitle();
        } else if (object instanceof Artist) {
            itemTitle = ((Artist) object).getName();
        }

        return itemTitle;
    }

    private void filterList(String constraint) {
        mItemListAdapter.getFilter().filter(constraint);
        mItemListAdapter.notifyDataSetChanged();

        mRvItemList.scrollTo(0, 0);
    }
}