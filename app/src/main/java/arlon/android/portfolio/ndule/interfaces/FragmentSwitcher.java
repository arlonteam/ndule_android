package arlon.android.portfolio.ndule.interfaces;

import java.util.Hashtable;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public interface FragmentSwitcher {

    void switchFragment(String fragmentTag, Hashtable<String, Object> parameters, boolean shouldAdd);
    void navigateBack(int code);
    void toggleLoginStatus();
    void changeProfileDetails();
    void toggleFabIcon(boolean visible);
    void showNotification(boolean visible);
    void login();
}