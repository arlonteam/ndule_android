package arlon.android.portfolio.ndule.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

/**
 * Created by Arlon Mukeba on 1/13/2018.
 */

public class Artist extends SugarRecord implements Parcelable {

    public final static String ARTIST = "artist";

    public final static String COLUMN_ID = "artist_id";
    public final static String COLUMN_TIMESTAMP = "timestamp";
    public final static String COLUMN_NAME = "name";
    public final static String COLUMN_IMAGE = "image";

    @Column(name = COLUMN_ID)
    private String mArtistId = "";

    @Column(name = COLUMN_TIMESTAMP)
    private String mTimeStamp = "";

    @Column(name = COLUMN_NAME)
    private String mName = "";

    @Column(name = COLUMN_IMAGE)
    private String mImage = "";

    public Artist() {
    }

    public Artist(Parcel in) {
        setId(in.readLong());
        setArtistId(in.readString());
        setName(in.readString());
        setImage(in.readString());
    }

    public void setArtistId(String artistId) {
        mArtistId = artistId;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setImage(String thumbnail) {
        mImage = thumbnail;
    }

    public String getArtistId() {
        return mArtistId;
    }

    public String getName() {
        return mName;
    }

    public String getImage() {
        return mImage;
    }

    public static final Creator<Artist> CREATOR = new Creator<Artist>() {

        @Override
        public Artist createFromParcel(Parcel source) {
            return new Artist(source);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getArtistId());
        dest.writeString(getName());
        dest.writeString(getImage());
    }

    public long save() {
        if (mTimeStamp.isEmpty()) {
            mTimeStamp = "" + System.currentTimeMillis();
        }

        if (mArtistId.isEmpty()) {
            mArtistId = "Artist_" + mTimeStamp;
        }

        return super.save();
    }

    public boolean equals(Object object) {
        boolean equals = (object != null);
        equals &= (object instanceof Artist);

        if (equals) {
            equals = mName.equalsIgnoreCase(((Artist) object).getName());
            equals |= mArtistId.equalsIgnoreCase(((Artist) object).getArtistId());
        }

        return equals;
    }

    public String toString () {
        return new StringBuffer("ARTIST: ").append("\n\n")
            .append("Id: ").append(mArtistId)
                .append("Title: ").append(mName)
                    .append("Thumbnail: ").append(mImage).toString();
    }

    /*public static JSONObject toLoginApi(User user) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CustomRequest.OBJECT_PARAM, CustomRequest.USER);
            jsonObject.put(CustomRequest.ACTION_PARAM, UserHelper.REQUEST_TAG_LOGIN);
            jsonObject.put(COLUMN_EMAIL, user.getEmail());
            jsonObject.put(COLUMN_PASSWORD, user.getPassword());
        } catch (Exception e) {
            Log.d("REQUESTBODY", "EXCEPTION: " + e.getMessage());
        }

        return jsonObject;
    }

    public static JSONObject toRegisterApi(User user, String action) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CustomRequest.OBJECT_PARAM, CustomRequest.USER);
            jsonObject.put(CustomRequest.ACTION_PARAM, action*//*isCreating ? "create" : "update"*//*);
            jsonObject.put(COLUMN_ID, user.getArtist());
            jsonObject.put(COLUMN_DEVICE_TOKEN, user.getDeviceToken());
            jsonObject.put(COLUMN_NAME, user.getName());
            jsonObject.put(COLUMN_ALBUM_TITLE, user.getAlbumTitle());
            jsonObject.put(COLUMN_AUTHOR_PSEUDO, user.getArtist());
            jsonObject.put(COLUMN_GENDER, user.getGender());
            jsonObject.put(COLUMN_BIRTH_DATE, user.getBirthDate());
            jsonObject.put(COLUMN_EMAIL, user.getEmail());
            jsonObject.put(COLUMN_PASSWORD, user.getPassword());
            jsonObject.put(COLUMN_PHYSICAL_ADDRESS, user.getPhysicalAddress());
            jsonObject.put(COLUMN_IMAGE, user.getImage());
        } catch (JSONException e) {

        }

        return jsonObject;
    }

    public static User fromRegisterApi(JSONObject jsonObject) {
        User user = new User();

        user.setArtist(JsonHelper.getString(jsonObject, COLUMN_ID));
        user.setDeviceToken(JsonHelper.getString(jsonObject, COLUMN_DEVICE_TOKEN));
        user.setName(JsonHelper.getString(jsonObject, COLUMN_NAME));
        user.setAlbumTitle(JsonHelper.getString(jsonObject, COLUMN_ALBUM_TITLE));
        user.setArtist(JsonHelper.getString(jsonObject, COLUMN_AUTHOR_PSEUDO));
        user.setGender(JsonHelper.getString(jsonObject, COLUMN_GENDER));
        user.setBirthDate(JsonHelper.getString(jsonObject, COLUMN_BIRTH_DATE));
        user.setEmail(JsonHelper.getString(jsonObject, COLUMN_EMAIL));
        user.setPassword(JsonHelper.getString(jsonObject, COLUMN_PASSWORD));
        user.setPhysicalAddress(JsonHelper.getString(jsonObject, COLUMN_PHYSICAL_ADDRESS));
        user.setImage(JsonHelper.getString(jsonObject, COLUMN_IMAGE));

        return user;
    }

    public static JSONObject userIdToJSON(User user) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(COLUMN_NAME, user.getName());
        return jsonObject;
    }

    public static JSONObject userIdsToJSONArray(ArrayList<User> users) {
        JSONArray userArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        try {
            for (User user : users) {
                userArray.put(userIdToJSON(user));
            }

            jsonObject.put("", userArray);
        } catch (Exception e) {

        }

        return jsonObject;
    }*/
}