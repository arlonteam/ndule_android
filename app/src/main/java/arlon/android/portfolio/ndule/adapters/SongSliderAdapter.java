package arlon.android.portfolio.ndule.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;

import arlon.android.portfolio.ndule.Ndule;
import arlon.android.portfolio.ndule.R;
import arlon.android.portfolio.ndule.objects.Song;

/**
 * Created by Arlon Mukeba on 1/26/2018.
 */

public class SongSliderAdapter extends PagerAdapter {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<Song> mSongs;

    public SongSliderAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setSongs(ArrayList<Song> songs) {
        mSongs = songs;
    }

    public Song getSongs(int index) {
        return mSongs.get(index);
    }

    @Override
    public int getCount() {
        return mSongs.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RelativeLayout relativeLayout = (RelativeLayout) mLayoutInflater.inflate(
            R.layout.layout_song_image, container, false);

        final ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.ivSongImage);

        Song song = mSongs.get(position);

        String songImage = Ndule.SONG_IMAGE_URL + song.getThumbnail() + ".jpg";

        Log.d("SONG_IMAGE", songImage);

        Glide.with(mContext).load(songImage).asBitmap().override(500, 500)
            .error(R.drawable.ic_treble_key)
                .diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().dontTransform()
                    .into(new SimpleTarget<Bitmap>(500, 500) {
                              @Override
                              public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                  imageView.setImageBitmap(bitmap);
                              }
                          }
                    );

        setSongClickListener(relativeLayout, song);

        container.addView(relativeLayout);

        return relativeLayout;
    }

    protected void setSongClickListener(RelativeLayout relativeLayout, Object object) {

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}